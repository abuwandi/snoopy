Snoopy
======

See the [project website][snoopy] for documentation and APIs.

Snoopy is an asynchronous client that’s efficient by default and easy to use. Its request/response API is designed with fluent builders and immutability. It
supports both synchronous blocking calls and async calls.

Snoopy is a high-performance networking async APIs, carefully designed to serve high performance, scalable 
and reliable network applications.

We highly recommend you keep Snoopy up-to-date. As with auto-updating web browsers, staying current
with HTTPS clients is an important defense against potential security problems.

Snoopy uses your platform's native TLS implementation for maximum security and performance. On Java platforms Snoopy also supports
OpenSsl.

As of now, version 0.9.1 supports only Http, more protocols is coming soon like for e.g. (FTP, SMTP, SSH, Telnet ... etc), so we recommend to keep checking
our progress because our goal is to develop the most advanced and efficient network client ever.

Get a URL
---------

This program downloads a URL and prints its contents as a string.

```java
URI uri = ...;
SnoopyConfig config = ...;
Snoopy.builder().config(config)
      .build()
      .get(uri)
      .followRedirects(true)
      .failIfNotSuccessfulResponse(true)
      .consumeAsString();
```


Post JSON to a Server
----------------

This program posts json data to a service.

```java
URI uri = ...;
SnoopyConfig config = ...;
T model = ...;
Class<T> clazz = ...;
Snoopy.builder().config(config)
      .build()
      .post(uri)
      .followRedirects(true)
      .failIfNotSuccessfulResponse(true)
      .body(model)
      .consumeAs(clazz);
```

Upload a file to a Server
----------------

This program uploads a file to a service.

```java
URI uri = ...;
SnoopyConfig config = ...;
Path fileToUpload = ...;
Snoopy.builder().config(config)
      .build()
      .post(uri)
      .followRedirects(true)
      .failIfNotSuccessfulResponse(true)
      .body(fileToUpload)
      .consumeAsString();
```

Upload an input stream to a Server
----------------

This program uploads an input stream to a service.

```java
URI uri = ...;
SnoopyConfig config = ...;
InputStream inputStreamToUpload = ...;
Snoopy.builder().config(config)
      .build()
      .post(uri)
      .followRedirects(true)
      .failIfNotSuccessfulResponse(true)
      .body(inputStreamToUpload)
      .consumeAsString();
```

Download a file from a Server
----------------------------------------

This program downloads a URL and stores it a file

```java
URI uri = ...;
SnoopyConfig config = ...;
Path path = ...;
Snoopy.builder().config(config)
      .build()
      .get(uri)
      .followRedirects(true)
      .failIfNotSuccessfulResponse(true)
      .consumeAsFile(path);
```

Requirements
------------

Snoopy works on Java 8+

Snoopy depends on [Netty][netty] and [RxJava][rxjava]. Both are libraries built for high performance, scalable and reliable network applications with strong backward-compatibility.

We highly recommend you keep Snoopy up-to-date. As with auto-updating web browsers, staying current
with HTTPS clients is an important defense against potential security problems. [We
track][tls_history] the dynamic TLS ecosystem and adjust Snoopy to improve connectivity and
security.

Snoopy uses your platform's native TLS implementation. On Java platforms Snoopy also supports
[Forked Tomcat Native][tcnative], which integrates OpenSsl with Java. Snoopy will use OpenSsl if it is available 
in its classpath.

Snoopy uses your platform's [Native I/O and Transports][native-transport] implementation. On Java platforms Snoopy also supports
[Linux Epoll][epoll], [FreeBSD Kqueue][kqueue], which integrates OpenSsl with Java. Snoopy will use OpenSsl if it is available 
in its classpath.

Releases
--------

Our [change log][changelog] has release history.

The latest release is available on [Maven Central](https://search.maven.org/artifact/org.bitbucket.abuwandi/snoopy-client/0.9.1/jar).

License
-------

```
Copyright 2020 ezHire, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
 [snoopy]: https://bitbucket.org/abuwandi/snoopy/
 [netty]: https://netty.io/
 [rxjava]: http://reactivex.io/
 [tcnative]: https://netty.io/wiki/forked-tomcat-native.html
 [epoll]: https://en.wikipedia.org/wiki/Epoll
 [kqueue]:https://en.wikipedia.org/wiki/Kqueue
 [native-transport]: https://netty.io/wiki/native-transports.html
 [changelog]: https://bitbucket.org/abuwandi/snoopy/