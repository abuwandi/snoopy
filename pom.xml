<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <groupId>org.bitbucket.abuwandi</groupId>
  <artifactId>snoopy</artifactId>
  <version>0.9.21</version>
  <name>Snoopy Network Client</name>
  <url>https://bitbucket.org/abuwandi/snoopy</url>
  <description>
    Asynchronous network client that’s efficient by default and easy to use.
    Its request/response API is designed with fluent builders and immutability.
    It supports both synchronous blocking calls and async calls
  </description>

  <properties>
    <netty.version>4.1.70.Final</netty.version>
    <netty.ssl.version>2.0.46.Final</netty.ssl.version>
    <log4j2.version>2.13.3</log4j2.version>
    <jackson.version>2.13.0</jackson.version>
    <lombok.version>1.18.22</lombok.version>
    <rxjava3.version>3.1.2</rxjava3.version>
    <apache.tika.version>2.1.0</apache.tika.version>
    <apache.commons-lang.version>3.12.0</apache.commons-lang.version>
  </properties>

  <build>
    <plugins>

      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-compiler-plugin</artifactId>
        <version>3.8.1</version>
        <configuration>
          <source>1.8</source>
          <target>1.8</target>
        </configuration>
      </plugin>

      <plugin>
        <groupId>org.codehaus.mojo</groupId>
        <artifactId>versions-maven-plugin</artifactId>
        <version>2.8.1</version>
        <configuration>
          <rulesUri>file:///${project.basedir}/maven-versions-rules.xml</rulesUri>
        </configuration>
      </plugin>

      <plugin>
        <groupId>com.coveo</groupId>
        <artifactId>fmt-maven-plugin</artifactId>
        <version>2.9.1</version>
        <executions>
          <execution>
            <goals>
              <goal>format</goal>
            </goals>
          </execution>
        </executions>
      </plugin>

      <plugin>
        <groupId>org.sonatype.plugins</groupId>
        <artifactId>nexus-staging-maven-plugin</artifactId>
        <version>1.6.8</version>
        <extensions>true</extensions>
        <configuration>
          <serverId>ossrh</serverId>
          <nexusUrl>https://oss.sonatype.org/</nexusUrl>
          <autoReleaseAfterClose>true</autoReleaseAfterClose>
        </configuration>
      </plugin>

      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-gpg-plugin</artifactId>
        <version>3.0.1</version>
        <executions>
          <execution>
            <id>sign-artifacts</id>
            <phase>verify</phase>
            <goals>
              <goal>sign</goal>
            </goals>
          </execution>
        </executions>
      </plugin>

      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-source-plugin</artifactId>
        <version>3.2.1</version>
        <executions>
          <execution>
            <id>attach-sources</id>
            <goals>
              <goal>jar-no-fork</goal>
            </goals>
          </execution>
        </executions>
      </plugin>

      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-javadoc-plugin</artifactId>
        <version>3.3.1</version>
        <executions>
          <execution>
            <id>attach-javadocs</id>
            <goals>
              <goal>jar</goal>
            </goals>
          </execution>
        </executions>
      </plugin>

    </plugins>

    <extensions>
      <extension>
        <groupId>kr.motd.maven</groupId>
        <artifactId>os-maven-plugin</artifactId>
        <version>1.7.0</version>
      </extension>
    </extensions>

  </build>

  <profiles>
    <profile>
      <id>linux</id>
      <activation>
        <os>
          <name>linux</name>
          <family>unix</family>
        </os>
      </activation>
      <dependencies>
        <dependency>
          <groupId>io.netty</groupId>
          <artifactId>netty-transport-native-epoll</artifactId>
          <version>${netty.version}</version>
          <classifier>${os.detected.classifier}</classifier>
        </dependency>
        <dependency>
          <groupId>io.netty</groupId>
          <artifactId>netty-tcnative-boringssl-static</artifactId>
          <version>${netty.ssl.version}</version>
          <classifier>${os.detected.classifier}</classifier>
        </dependency>
      </dependencies>
    </profile>
    <profile>
      <id>osx</id>
      <activation>
        <os>
          <family>mac</family>
        </os>
      </activation>
      <dependencies>
        <dependency>
          <groupId>io.netty</groupId>
          <artifactId>netty-transport-native-kqueue</artifactId>
          <version>${netty.version}</version>
          <classifier>${os.detected.classifier}</classifier>
        </dependency>
        <dependency>
          <groupId>io.netty</groupId>
          <artifactId>netty-tcnative-boringssl-static</artifactId>
          <version>${netty.ssl.version}</version>
          <classifier>${os.detected.classifier}</classifier>
        </dependency>
      </dependencies>
    </profile>
    <profile>
      <id>windows</id>
      <activation>
        <os>
          <family>windows</family>
        </os>
      </activation>
      <dependencies>
      </dependencies>
    </profile>
    <!--    <profile>-->
    <!--      <id>all</id>-->
    <!--      <activation>-->
    <!--        <activeByDefault>true</activeByDefault>-->
    <!--      </activation>-->
    <!--      <dependencies>-->
    <!--        <dependency>-->
    <!--          <groupId>io.netty</groupId>-->
    <!--          <artifactId>netty-transport-native-kqueue</artifactId>-->
    <!--          <version>${netty.version}</version>-->
    <!--        </dependency>-->
    <!--        <dependency>-->
    <!--          <groupId>io.netty</groupId>-->
    <!--          <artifactId>netty-tcnative-boringssl-static</artifactId>-->
    <!--          <version>${netty.ssl.version}</version>-->
    <!--        </dependency>-->
    <!--        <dependency>-->
    <!--          <groupId>io.netty</groupId>-->
    <!--          <artifactId>netty-transport-native-epoll</artifactId>-->
    <!--          <version>${netty.version}</version>-->
    <!--        </dependency>-->
    <!--      </dependencies>-->
    <!--    </profile>-->
  </profiles>

  <dependencies>

    <dependency>
      <groupId>com.fasterxml.jackson.core</groupId>
      <artifactId>jackson-databind</artifactId>
      <version>${jackson.version}</version>
    </dependency>

    <dependency>
      <groupId>com.fasterxml.jackson.dataformat</groupId>
      <artifactId>jackson-dataformat-yaml</artifactId>
      <version>${jackson.version}</version>
    </dependency>

    <dependency>
      <groupId>org.projectlombok</groupId>
      <artifactId>lombok</artifactId>
      <version>${lombok.version}</version>
      <scope>provided</scope>
    </dependency>

    <dependency>
      <groupId>io.reactivex.rxjava3</groupId>
      <artifactId>rxjava</artifactId>
      <version>${rxjava3.version}</version>
    </dependency>

    <dependency>
      <groupId>org.apache.commons</groupId>
      <artifactId>commons-lang3</artifactId>
      <version>${apache.commons-lang.version}</version>
    </dependency>

    <dependency>
      <groupId>org.apache.tika</groupId>
      <artifactId>tika-core</artifactId>
      <version>${apache.tika.version}</version>
    </dependency>

    <dependency>
      <groupId>io.netty</groupId>
      <artifactId>netty-codec-http</artifactId>
      <version>${netty.version}</version>
    </dependency>

    <dependency>
      <groupId>io.netty</groupId>
      <artifactId>netty-handler</artifactId>
      <version>${netty.version}</version>
    </dependency>

    <!--    <dependency>-->
    <!--      <groupId>org.apache.logging.log4j</groupId>-->
    <!--      <artifactId>log4j-api</artifactId>-->
    <!--      <version>${log4j2.version}</version>-->
    <!--    </dependency>-->

    <!--    <dependency>-->
    <!--      <groupId>org.apache.logging.log4j</groupId>-->
    <!--      <artifactId>log4j-core</artifactId>-->
    <!--      <version>${log4j2.version}</version>-->
    <!--    </dependency>-->

    <!--    <dependency>-->
    <!--      <groupId>org.apache.logging.log4j</groupId>-->
    <!--      <artifactId>log4j-slf4j-impl</artifactId>-->
    <!--      <version>${log4j2.version}</version>-->
    <!--    </dependency>-->

    <!--    <dependency>-->
    <!--      <groupId>com.lmax</groupId>-->
    <!--      <artifactId>disruptor</artifactId>-->
    <!--      <version>3.4.2</version>-->
    <!--    </dependency>-->

    <!--    <dependency>-->
    <!--      <groupId>pl.pojo</groupId>-->
    <!--      <artifactId>pojo-tester</artifactId>-->
    <!--      <version>0.7.6</version>-->
    <!--      <scope>test</scope>-->
    <!--    </dependency>-->

    <!--    <dependency>-->
    <!--      <groupId>org.powermock</groupId>-->
    <!--      <artifactId>powermock-core</artifactId>-->
    <!--      <version>2.0.7</version>-->
    <!--      <scope>test</scope>-->
    <!--    </dependency>-->

    <!--    <dependency>-->
    <!--      <groupId>org.powermock</groupId>-->
    <!--      <artifactId>powermock-api-mockito2</artifactId>-->
    <!--      <version>2.0.7</version>-->
    <!--      <scope>test</scope>-->
    <!--    </dependency>-->

  </dependencies>

  <licenses>
    <license>
      <name>Apache License</name>
      <url>http://www.apache.org/licenses/LICENSE-2.0</url>
    </license>
  </licenses>

  <scm>
    <connection>scm:git:git://bitbucket.org/abuwandi/snoopy.git</connection>
    <developerConnection>scm:git:ssh://bitbucket.org:abuwandi/snoopy.git</developerConnection>
    <url>https://bitbucket.org/abuwandi/snoopy/src</url>
  </scm>

  <distributionManagement>
    <snapshotRepository>
      <id>ossrh</id>
      <url>https://oss.sonatype.org/content/repositories/snapshots</url>
    </snapshotRepository>
  </distributionManagement>

  <developers>
    <developer>
      <name>Zaid Abuwandi</name>
      <email>zaidabuwandi@gmail.com</email>
      <organization>org.bitbucket.abuwandi</organization>
      <organizationUrl>https://bitbucket.org/abuwandi</organizationUrl>
    </developer>
  </developers>

</project>