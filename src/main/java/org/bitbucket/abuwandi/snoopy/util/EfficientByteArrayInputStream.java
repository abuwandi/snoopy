package org.bitbucket.abuwandi.snoopy.util;

import java.io.InputStream;
import java.util.LinkedList;

/** The type Efficient byte array input stream. */
public class EfficientByteArrayInputStream extends InputStream {

  private final byte[] singleByteArray;
  private int readByteIndex;
  private final LinkedList<byte[]> buffers;
  private final int size;

  /**
   * Instantiates a new Efficient byte array input stream.
   *
   * @param byteArrayOutputStream the byte array output stream
   */
  public EfficientByteArrayInputStream(final EfficientByteArrayOutputStream byteArrayOutputStream) {
    this.singleByteArray = new byte[1];
    this.buffers = byteArrayOutputStream.buffers;
    this.size = byteArrayOutputStream.size;
  }

  @Override
  public int read() {
    final int readBytes = read(singleByteArray);
    if (readBytes < 1) {
      return -1;
    }
    return singleByteArray[0];
  }

  @Override
  public int read(byte[] destination) {
    return read(destination, 0, destination.length);
  }

  @Override
  public int read(final byte[] destination, final int offset, final int length) {
    if (destination == null || offset < 0 || length < 0 || length > (destination.length - offset)) {
      throw new IllegalArgumentException("Invalid read arguments");
    }
    if (length < 1) {
      return 0;
    }
    if (buffers.isEmpty()) {
      return -1;
    }
    int bytesReadTotal = 0;
    int destinationOffset = offset;
    while (bytesReadTotal < length && !buffers.isEmpty()) {
      final byte[] buffer = buffers.peek();
      final int bytesRead = Math.min(length - bytesReadTotal, buffer.length - readByteIndex);
      System.arraycopy(buffer, readByteIndex, destination, destinationOffset, bytesRead);
      bytesReadTotal += bytesRead;
      destinationOffset += bytesRead;
      readByteIndex += bytesRead;
      if (readByteIndex >= buffer.length) {
        readByteIndex = 0;
        buffers.remove();
      }
    }
    return bytesReadTotal;
  }

  @Override
  public int available() {
    return size;
  }
}
