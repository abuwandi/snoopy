package org.bitbucket.abuwandi.snoopy.util;

/**
 * The interface Disposal.
 *
 * @param <I> the type parameter
 */
public interface Disposal<I> extends Collector<I, Long> {

  /**
   * Dispose.
   *
   * @param output the output
   */
  void dispose(I output);

  @Override
  default void collect(I input) {
    dispose(input);
  }

  @Override
  default Long finish() {
    return 0L;
  }
}
