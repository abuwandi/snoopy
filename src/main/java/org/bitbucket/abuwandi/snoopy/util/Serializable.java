package org.bitbucket.abuwandi.snoopy.util;

/** The interface Serializable. */
public interface Serializable {

  /**
   * Serialize byte [ ].
   *
   * @return the byte [ ]
   */
  byte[] serialize();
}
