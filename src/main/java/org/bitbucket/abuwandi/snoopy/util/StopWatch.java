package org.bitbucket.abuwandi.snoopy.util;

import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import lombok.Builder;

/** The type Stop watch. */
public class StopWatch {

  private final AtomicLong startTime;
  private final AtomicReference<Status> status;
  private final AtomicLong total;
  private final AtomicLong last;

  /** Instantiates a new Stop watch. */
  @Builder
  public StopWatch() {
    startTime = new AtomicLong(0);
    status = new AtomicReference<>(Status.READY);
    total = new AtomicLong(0);
    last = new AtomicLong(0);
  }

  /**
   * Start long.
   *
   * @return the long
   */
  public long start() {
    if (status.compareAndSet(Status.READY, Status.STARTED)) {
      final long now = System.nanoTime();
      return startTime.updateAndGet(time -> now);
    } else {
      throw new IllegalStateException("Stop watch is not in ready state");
    }
  }

  /**
   * Time long.
   *
   * @return the long
   */
  public long time() {
    if (isStarted()) {
      final long now = System.nanoTime();
      final long time = now - startTime.getAndSet(now);
      total.addAndGet(time);
      return last.updateAndGet(current -> time);
    }
    throw new IllegalStateException("Stop watch is not in started state");
  }

  /**
   * Last long.
   *
   * @return the long
   */
  public long last() {
    return last.get();
  }

  /** Reset. */
  public void reset() {
    if (status.compareAndSet(Status.STARTED, Status.STARTED)) {
      startTime.set(0);
      total.set(0);
    } else {
      throw new IllegalStateException("Stop watch is not in started state");
    }
  }

  /**
   * Is started boolean.
   *
   * @return the boolean
   */
  public boolean isStarted() {
    return status.compareAndSet(Status.STARTED, Status.STARTED);
  }

  /**
   * Total long.
   *
   * @return the long
   */
  public long total() {
    return this.total.get();
  }

  /** The enum Status. */
  public enum Status {
    /** Ready status. */
    READY,
    /** Started status. */
    STARTED,
    /** Stopped status. */
    STOPPED;
  }
}
