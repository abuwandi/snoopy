package org.bitbucket.abuwandi.snoopy.util;

/** The enum Bit unit. */
public enum BitUnit {
  /** The Bit. */
  BIT {
    @Override
    public double toBits(double d) {
      return d;
    }

    @Override
    public double convert(double d, BitUnit u) {
      return u.toBits(d);
    }
  },

  /** The Kibit. */
  KIBIT {
    @Override
    public double toBits(double d) {
      return safeMulti(d, C_KIBIT);
    }

    @Override
    public double convert(double d, BitUnit u) {
      return u.toKibit(d);
    }
  },

  /** The Mibit. */
  MIBIT {
    @Override
    public double toBits(double d) {
      return safeMulti(d, C_MIBIT);
    }

    @Override
    public double convert(double d, BitUnit u) {
      return u.toMibit(d);
    }
  },

  /** The Gibit. */
  GIBIT {
    @Override
    public double toBits(double d) {
      return safeMulti(d, C_GIBIT);
    }

    @Override
    public double convert(double d, BitUnit u) {
      return u.toGibit(d);
    }
  },

  /** The Tibit. */
  TIBIT {
    @Override
    public double toBits(double d) {
      return safeMulti(d, C_TIBIT);
    }

    @Override
    public double convert(double d, BitUnit u) {
      return u.toTibit(d);
    }
  },

  /** The Pibit. */
  PIBIT {
    @Override
    public double toBits(double d) {
      return safeMulti(d, C_PIBIT);
    }

    @Override
    public double convert(double d, BitUnit u) {
      return u.toPibit(d);
    }
  },

  /** The Kbit. */
  KBIT {
    @Override
    public double toBits(double d) {
      return safeMulti(d, C_KBIT);
    }

    @Override
    public double convert(double d, BitUnit u) {
      return u.toKbit(d);
    }
  },

  /** The Mbit. */
  MBIT {
    @Override
    public double toBits(double d) {
      return safeMulti(d, C_MBIT);
    }

    @Override
    public double convert(double d, BitUnit u) {
      return u.toMbit(d);
    }
  },

  /** The Gbit. */
  GBIT {
    @Override
    public double toBits(double d) {
      return safeMulti(d, C_GBIT);
    }

    @Override
    public double convert(double d, BitUnit u) {
      return u.toGbit(d);
    }
  },

  /** The Tbit. */
  TBIT {
    @Override
    public double toBits(double d) {
      return safeMulti(d, C_TBIT);
    }

    @Override
    public double convert(double d, BitUnit u) {
      return u.toTbit(d);
    }
  },

  /** The Pbit. */
  PBIT {
    @Override
    public double toBits(double d) {
      return safeMulti(d, C_PBIT);
    }

    @Override
    public double convert(double d, BitUnit u) {
      return u.toPbit(d);
    }
  };

  /** The C kibit. */
  static final double C_KIBIT = Math.pow(2d, 10d);
  /** The C mibit. */
  static final double C_MIBIT = Math.pow(2d, 20d);
  /** The C gibit. */
  static final double C_GIBIT = Math.pow(2d, 30d);
  /** The C tibit. */
  static final double C_TIBIT = Math.pow(2d, 40d);
  /** The C pibit. */
  static final double C_PIBIT = Math.pow(2d, 50d);

  /** The C kbit. */
  static final double C_KBIT = Math.pow(10d, 3d);
  /** The C mbit. */
  static final double C_MBIT = Math.pow(10d, 6d);
  /** The C gbit. */
  static final double C_GBIT = Math.pow(10d, 9d);
  /** The C tbit. */
  static final double C_TBIT = Math.pow(10d, 12d);
  /** The C pbit. */
  static final double C_PBIT = Math.pow(10d, 15d);

  private static final double MAX = Double.MAX_VALUE;

  /**
   * Safe multi double.
   *
   * @param d the d
   * @param multi the multi
   * @return the double
   */
  static final double safeMulti(double d, double multi) {
    double limit = MAX / multi;

    if (d > limit) {
      return Double.MAX_VALUE;
    }

    if (d < -limit) {
      return Double.MIN_VALUE;
    }

    return d * multi;
  }

  /**
   * To bits double.
   *
   * @param d the d
   * @return the double
   */
  public abstract double toBits(double d);

  /**
   * To kibit double.
   *
   * @param d the d
   * @return the double
   */
  public final double toKibit(double d) {
    return toBits(d) / C_KIBIT;
  }

  /**
   * To mibit double.
   *
   * @param d the d
   * @return the double
   */
  public final double toMibit(double d) {
    return toBits(d) / C_MIBIT;
  }

  /**
   * To gibit double.
   *
   * @param d the d
   * @return the double
   */
  public final double toGibit(double d) {
    return toBits(d) / C_GIBIT;
  }

  /**
   * To tibit double.
   *
   * @param d the d
   * @return the double
   */
  public final double toTibit(double d) {
    return toBits(d) / C_TIBIT;
  }

  /**
   * To pibit double.
   *
   * @param d the d
   * @return the double
   */
  public final double toPibit(double d) {
    return toBits(d) / C_PIBIT;
  }

  /**
   * To kbit double.
   *
   * @param d the d
   * @return the double
   */
  public final double toKbit(double d) {
    return toBits(d) / C_KBIT;
  }

  /**
   * To mbit double.
   *
   * @param d the d
   * @return the double
   */
  public final double toMbit(double d) {
    return toBits(d) / C_MBIT;
  }

  /**
   * To gbit double.
   *
   * @param d the d
   * @return the double
   */
  public final double toGbit(double d) {
    return toBits(d) / C_GBIT;
  }

  /**
   * To tbit double.
   *
   * @param d the d
   * @return the double
   */
  public final double toTbit(double d) {
    return toBits(d) / C_TBIT;
  }

  /**
   * To pbit double.
   *
   * @param d the d
   * @return the double
   */
  public final double toPbit(double d) {
    return toBits(d) / C_PBIT;
  }

  /**
   * Convert double.
   *
   * @param d the d
   * @param u the u
   * @return the double
   */
  public abstract double convert(double d, BitUnit u);

  /**
   * Convert double.
   *
   * @param d the d
   * @param u the u
   * @return the double
   */
  public final double convert(double d, ByteUnit u) {
    return convert(d, u, Byte.SIZE);
  }

  /**
   * Convert double.
   *
   * @param d the d
   * @param u the u
   * @param wordSize the word size
   * @return the double
   */
  public final double convert(double d, ByteUnit u, int wordSize) {
    double bits = safeMulti(u.toBytes(d), wordSize);
    return convert(bits, BIT);
  }

  /**
   * To bytes double.
   *
   * @param d the d
   * @return the double
   */
  public final double toBytes(double d) {
    return ByteUnit.BYTE.convert(d, this);
  }

  /**
   * To bytes double.
   *
   * @param d the d
   * @param wordSize the word size
   * @return the double
   */
  public final double toBytes(double d, int wordSize) {
    return ByteUnit.BYTE.convert(d, this, wordSize);
  }

  /**
   * To ki b double.
   *
   * @param d the d
   * @return the double
   */
  public final double toKiB(double d) {
    return ByteUnit.KIB.convert(d, this);
  }

  /**
   * To mi b double.
   *
   * @param d the d
   * @return the double
   */
  public final double toMiB(double d) {
    return ByteUnit.MIB.convert(d, this);
  }

  /**
   * To gi b double.
   *
   * @param d the d
   * @return the double
   */
  public final double toGiB(double d) {
    return ByteUnit.GIB.convert(d, this);
  }

  /**
   * To ti b double.
   *
   * @param d the d
   * @return the double
   */
  public final double toTiB(double d) {
    return ByteUnit.TIB.convert(d, this);
  }

  /**
   * To pi b double.
   *
   * @param d the d
   * @return the double
   */
  public final double toPiB(double d) {
    return ByteUnit.PIB.convert(d, this);
  }

  /**
   * To ki b double.
   *
   * @param d the d
   * @param wordSize the word size
   * @return the double
   */
  public final double toKiB(double d, int wordSize) {
    return ByteUnit.KIB.convert(d, this, wordSize);
  }

  /**
   * To mi b double.
   *
   * @param d the d
   * @param wordSize the word size
   * @return the double
   */
  public final double toMiB(double d, int wordSize) {
    return ByteUnit.MIB.convert(d, this, wordSize);
  }

  /**
   * To gi b double.
   *
   * @param d the d
   * @param wordSize the word size
   * @return the double
   */
  public final double toGiB(double d, int wordSize) {
    return ByteUnit.GIB.convert(d, this, wordSize);
  }

  /**
   * To ti b double.
   *
   * @param d the d
   * @param wordSize the word size
   * @return the double
   */
  public final double toTiB(double d, int wordSize) {
    return ByteUnit.TIB.convert(d, this, wordSize);
  }

  /**
   * To pi b double.
   *
   * @param d the d
   * @param wordSize the word size
   * @return the double
   */
  public final double toPiB(double d, int wordSize) {
    return ByteUnit.PIB.convert(d, this, wordSize);
  }

  /**
   * To kb double.
   *
   * @param d the d
   * @return the double
   */
  public final double toKB(double d) {
    return ByteUnit.KB.convert(d, this);
  }

  /**
   * To mb double.
   *
   * @param d the d
   * @return the double
   */
  public final double toMB(double d) {
    return ByteUnit.MB.convert(d, this);
  }

  /**
   * To gb double.
   *
   * @param d the d
   * @return the double
   */
  public final double toGB(double d) {
    return ByteUnit.GB.convert(d, this);
  }

  /**
   * To tb double.
   *
   * @param d the d
   * @return the double
   */
  public final double toTB(double d) {
    return ByteUnit.TB.convert(d, this);
  }

  /**
   * To pb double.
   *
   * @param d the d
   * @return the double
   */
  public final double toPB(double d) {
    return ByteUnit.PB.convert(d, this);
  }

  /**
   * To kb double.
   *
   * @param d the d
   * @param wordSize the word size
   * @return the double
   */
  public final double toKB(double d, int wordSize) {
    return ByteUnit.KB.convert(d, this, wordSize);
  }

  /**
   * To mb double.
   *
   * @param d the d
   * @param wordSize the word size
   * @return the double
   */
  public final double toMB(double d, int wordSize) {
    return ByteUnit.MB.convert(d, this, wordSize);
  }

  /**
   * To gb double.
   *
   * @param d the d
   * @param wordSize the word size
   * @return the double
   */
  public final double toGB(double d, int wordSize) {
    return ByteUnit.GB.convert(d, this, wordSize);
  }

  /**
   * To tb double.
   *
   * @param d the d
   * @param wordSize the word size
   * @return the double
   */
  public final double toTB(double d, int wordSize) {
    return ByteUnit.TB.convert(d, this, wordSize);
  }

  /**
   * To pb double.
   *
   * @param d the d
   * @param wordSize the word size
   * @return the double
   */
  public final double toPB(double d, int wordSize) {
    return ByteUnit.PB.convert(d, this, wordSize);
  }
}
