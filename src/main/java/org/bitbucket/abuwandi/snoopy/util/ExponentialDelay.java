package org.bitbucket.abuwandi.snoopy.util;

import java.util.Objects;
import java.util.concurrent.TimeUnit;
import lombok.Builder;
import lombok.Getter;
import org.apache.commons.lang3.builder.ToStringBuilder;

/** The type Exponential delay. */
@Getter
public class ExponentialDelay {
  private final long lower;
  private final long upper;
  private final double growBy;
  private final TimeUnit unit;

  /**
   * Instantiates a new Exponential delay.
   *
   * @param unit the unit
   * @param upper the upper
   * @param lower the lower
   * @param growBy the grow by
   */
  @Builder
  public ExponentialDelay(TimeUnit unit, long upper, long lower, double growBy) {
    assert Objects.nonNull(unit) && lower <= upper && growBy > 0 : "Invalid arguments";
    this.lower = lower;
    this.upper = upper;
    this.growBy = growBy;
    this.unit = unit;
  }

  /**
   * Calculate long.
   *
   * @param attempt the attempt
   * @return the long
   */
  public long calculate(long attempt) {
    if (attempt <= 0) { // safeguard against underflow
      return lower;
    } else {
      final long value =
          Math.round((attempt >= 64 ? Long.MAX_VALUE : (1L << (attempt - 1))) * growBy);
      return value < lower ? lower : Math.min(value, upper);
    }
  }

  @Override
  public String toString() {
    return new ToStringBuilder(this)
        .append("unit", unit)
        .append("lower", lower)
        .append("upper", upper)
        .append("growBy", growBy)
        .build();
  }
}
