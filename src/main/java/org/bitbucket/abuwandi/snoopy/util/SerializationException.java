package org.bitbucket.abuwandi.snoopy.util;

import org.bitbucket.abuwandi.snoopy.exceptions.SnoopyException;

/** The type Serialization exception. */
public class SerializationException extends SnoopyException {

  /**
   * Instantiates a new Serialization exception.
   *
   * @param message the message
   */
  public SerializationException(String message) {
    super(message);
  }

  /**
   * Instantiates a new Serialization exception.
   *
   * @param cause the cause
   */
  public SerializationException(Throwable cause) {
    super(cause);
  }

  /**
   * Instantiates a new Serialization exception.
   *
   * @param message the message
   * @param cause the cause
   */
  public SerializationException(String message, Throwable cause) {
    super(message, cause);
  }
}
