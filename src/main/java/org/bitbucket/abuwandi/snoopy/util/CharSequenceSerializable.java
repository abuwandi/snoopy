package org.bitbucket.abuwandi.snoopy.util;

import java.nio.charset.Charset;
import java.util.Objects;
import java.util.Optional;
import lombok.Builder;
import lombok.NonNull;

/** The type Char sequence serializable. */
public class CharSequenceSerializable implements Serializable {

  private final CharSequence charSequence;
  private final Charset charset;
  private byte[] bytes;

  /**
   * Instantiates a new Char sequence serializable.
   *
   * @param charSequence the char sequence
   * @param charset the charset
   */
  @Builder
  public CharSequenceSerializable(@NonNull final CharSequence charSequence, final Charset charset) {
    this.charSequence = charSequence;
    this.charset = Optional.ofNullable(charset).orElse(Charset.defaultCharset());
  }

  @Override
  public byte[] serialize() {
    if (Objects.isNull(bytes)) {
      bytes = charSequence.toString().getBytes(charset);
    }
    return bytes;
  }
}
