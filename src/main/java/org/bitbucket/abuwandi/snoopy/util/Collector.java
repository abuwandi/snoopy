package org.bitbucket.abuwandi.snoopy.util;

import java.io.Closeable;

/**
 * The interface Collector.
 *
 * @param <I> the type parameter
 * @param <O> the type parameter
 */
public interface Collector<I, O> extends Closeable {

  /**
   * Collect.
   *
   * @param input the input
   */
  void collect(I input);

  /**
   * Finish o.
   *
   * @return the o
   */
  O finish();
}
