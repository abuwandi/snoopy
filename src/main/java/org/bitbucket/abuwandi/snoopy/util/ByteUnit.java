package org.bitbucket.abuwandi.snoopy.util;

/** The enum Byte unit. */
public enum ByteUnit {
  /** The Byte. */
  BYTE {
    @Override
    public double toBytes(double d) {
      return d;
    }

    @Override
    public double convert(double d, ByteUnit u) {
      return u.toBytes(d);
    }
  },

  /** The Kib. */
  KIB {
    @Override
    public double toBytes(double d) {
      return safeMulti(d, C_KIB);
    }

    @Override
    public double convert(double d, ByteUnit u) {
      return u.toKiB(d);
    }
  },

  /** The Mib. */
  MIB {
    @Override
    public double toBytes(double d) {
      return safeMulti(d, C_MIB);
    }

    @Override
    public double convert(double d, ByteUnit u) {
      return u.toMiB(d);
    }
  },

  /** The Gib. */
  GIB {
    @Override
    public double toBytes(double d) {
      return safeMulti(d, C_GIB);
    }

    @Override
    public double convert(double d, ByteUnit u) {
      return u.toGiB(d);
    }
  },

  /** The Tib. */
  TIB {
    @Override
    public double toBytes(double d) {
      return safeMulti(d, C_TIB);
    }

    @Override
    public double convert(double d, ByteUnit u) {
      return u.toTiB(d);
    }
  },

  /** The Pib. */
  PIB {
    @Override
    public double toBytes(double d) {
      return safeMulti(d, C_PIB);
    }

    @Override
    public double convert(double d, ByteUnit u) {
      return u.toPiB(d);
    }
  },

  /** The Kb. */
  KB {
    @Override
    public double toBytes(double d) {
      return safeMulti(d, C_KB);
    }

    @Override
    public double convert(double d, ByteUnit u) {
      return u.toKB(d);
    }
  },

  /** The Mb. */
  MB {
    @Override
    public double toBytes(double d) {
      return safeMulti(d, C_MB);
    }

    @Override
    public double convert(double d, ByteUnit u) {
      return u.toMB(d);
    }
  },

  /** The Gb. */
  GB {
    @Override
    public double toBytes(double d) {
      return safeMulti(d, C_GB);
    }

    @Override
    public double convert(double d, ByteUnit u) {
      return u.toGB(d);
    }
  },

  /** The Tb. */
  TB {
    @Override
    public double toBytes(double d) {
      return safeMulti(d, C_TB);
    }

    @Override
    public double convert(double d, ByteUnit u) {
      return u.toTB(d);
    }
  },

  /** The Pb. */
  PB {
    @Override
    public double toBytes(double d) {
      return safeMulti(d, C_PB);
    }

    @Override
    public double convert(double d, ByteUnit u) {
      return u.toPB(d);
    }
  };

  /** The C kib. */
  static final double C_KIB = Math.pow(2d, 10d);
  /** The C mib. */
  static final double C_MIB = Math.pow(2d, 20d);
  /** The C gib. */
  static final double C_GIB = Math.pow(2d, 30d);
  /** The C tib. */
  static final double C_TIB = Math.pow(2d, 40d);
  /** The C pib. */
  static final double C_PIB = Math.pow(2d, 50d);

  /** The C kb. */
  static final double C_KB = Math.pow(10d, 3d);
  /** The C mb. */
  static final double C_MB = Math.pow(10d, 6d);
  /** The C gb. */
  static final double C_GB = Math.pow(10d, 9d);
  /** The C tb. */
  static final double C_TB = Math.pow(10d, 12d);
  /** The C pb. */
  static final double C_PB = Math.pow(10d, 15d);

  private static final double MAX = Double.MAX_VALUE;

  /**
   * Safe multi double.
   *
   * @param d the d
   * @param multi the multi
   * @return the double
   */
  static final double safeMulti(double d, double multi) {
    double limit = MAX / multi;

    if (d > limit) {
      return Double.MAX_VALUE;
    }
    if (d < -limit) {
      return Double.MIN_VALUE;
    }

    return d * multi;
  }

  /**
   * To bytes double.
   *
   * @param d the d
   * @return the double
   */
  public abstract double toBytes(double d);

  /**
   * To ki b double.
   *
   * @param d the d
   * @return the double
   */
  public final double toKiB(double d) {
    return toBytes(d) / C_KIB;
  }

  /**
   * To mi b double.
   *
   * @param d the d
   * @return the double
   */
  public final double toMiB(double d) {
    return toBytes(d) / C_MIB;
  }

  /**
   * To gi b double.
   *
   * @param d the d
   * @return the double
   */
  public final double toGiB(double d) {
    return toBytes(d) / C_GIB;
  }

  /**
   * To ti b double.
   *
   * @param d the d
   * @return the double
   */
  public final double toTiB(double d) {
    return toBytes(d) / C_TIB;
  }

  /**
   * To pi b double.
   *
   * @param d the d
   * @return the double
   */
  public final double toPiB(double d) {
    return toBytes(d) / C_PIB;
  }

  /**
   * To kb double.
   *
   * @param d the d
   * @return the double
   */
  public final double toKB(double d) {
    return toBytes(d) / C_KB;
  }

  /**
   * To mb double.
   *
   * @param d the d
   * @return the double
   */
  public final double toMB(double d) {
    return toBytes(d) / C_MB;
  }

  /**
   * To gb double.
   *
   * @param d the d
   * @return the double
   */
  public final double toGB(double d) {
    return toBytes(d) / C_GB;
  }

  /**
   * To tb double.
   *
   * @param d the d
   * @return the double
   */
  public final double toTB(double d) {
    return toBytes(d) / C_TB;
  }

  /**
   * To pb double.
   *
   * @param d the d
   * @return the double
   */
  public final double toPB(double d) {
    return toBytes(d) / C_PB;
  }

  /**
   * Convert double.
   *
   * @param d the d
   * @param u the u
   * @return the double
   */
  public abstract double convert(double d, ByteUnit u);

  /**
   * Convert double.
   *
   * @param d the d
   * @param u the u
   * @return the double
   */
  public final double convert(double d, BitUnit u) {
    return convert(d, u, Byte.SIZE);
  }

  /**
   * Convert double.
   *
   * @param d the d
   * @param u the u
   * @param wordSize the word size
   * @return the double
   */
  public final double convert(double d, BitUnit u, int wordSize) {
    double bytes = u.toBits(d) / wordSize;
    return convert(bytes, BYTE);
  }

  /**
   * To bits double.
   *
   * @param d the d
   * @return the double
   */
  /*
   * Komfort-Methoden für Cross-Konvertierung
   */
  public final double toBits(double d) {
    return BitUnit.BIT.convert(d, this);
  }

  /**
   * To bits double.
   *
   * @param d the d
   * @param wordSize the word size
   * @return the double
   */
  public final double toBits(double d, int wordSize) {
    return BitUnit.BIT.convert(d, this, wordSize);
  }

  /**
   * To kibit double.
   *
   * @param d the d
   * @return the double
   */
  public final double toKibit(double d) {
    return BitUnit.KIBIT.convert(d, this);
  }

  /**
   * To mibit double.
   *
   * @param d the d
   * @return the double
   */
  public final double toMibit(double d) {
    return BitUnit.MIBIT.convert(d, this);
  }

  /**
   * To gibit double.
   *
   * @param d the d
   * @return the double
   */
  public final double toGibit(double d) {
    return BitUnit.GIBIT.convert(d, this);
  }

  /**
   * To tibit double.
   *
   * @param d the d
   * @return the double
   */
  public final double toTibit(double d) {
    return BitUnit.TIBIT.convert(d, this);
  }

  /**
   * To pibit double.
   *
   * @param d the d
   * @return the double
   */
  public final double toPibit(double d) {
    return BitUnit.PIBIT.convert(d, this);
  }

  /**
   * To kibit double.
   *
   * @param d the d
   * @param wordSize the word size
   * @return the double
   */
  public final double toKibit(double d, int wordSize) {
    return BitUnit.KIBIT.convert(d, this, wordSize);
  }

  /**
   * To mibit double.
   *
   * @param d the d
   * @param wordSize the word size
   * @return the double
   */
  public final double toMibit(double d, int wordSize) {
    return BitUnit.MIBIT.convert(d, this, wordSize);
  }

  /**
   * To gibit double.
   *
   * @param d the d
   * @param wordSize the word size
   * @return the double
   */
  public final double toGibit(double d, int wordSize) {
    return BitUnit.GIBIT.convert(d, this, wordSize);
  }

  /**
   * To tibit double.
   *
   * @param d the d
   * @param wordSize the word size
   * @return the double
   */
  public final double toTibit(double d, int wordSize) {
    return BitUnit.TIBIT.convert(d, this, wordSize);
  }

  /**
   * To pibit double.
   *
   * @param d the d
   * @param wordSize the word size
   * @return the double
   */
  public final double toPibit(double d, int wordSize) {
    return BitUnit.PIBIT.convert(d, this, wordSize);
  }

  /**
   * To kbit double.
   *
   * @param d the d
   * @return the double
   */
  public final double toKbit(double d) {
    return BitUnit.KBIT.convert(d, this);
  }

  /**
   * To mbit double.
   *
   * @param d the d
   * @return the double
   */
  public final double toMbit(double d) {
    return BitUnit.MBIT.convert(d, this);
  }

  /**
   * To gbit double.
   *
   * @param d the d
   * @return the double
   */
  public final double toGbit(double d) {
    return BitUnit.GBIT.convert(d, this);
  }

  /**
   * To tbit double.
   *
   * @param d the d
   * @return the double
   */
  public final double toTbit(double d) {
    return BitUnit.TBIT.convert(d, this);
  }

  /**
   * To pbit double.
   *
   * @param d the d
   * @return the double
   */
  public final double toPbit(double d) {
    return BitUnit.PBIT.convert(d, this);
  }

  /**
   * To kbit double.
   *
   * @param d the d
   * @param wordSize the word size
   * @return the double
   */
  public final double toKbit(double d, int wordSize) {
    return BitUnit.KBIT.convert(d, this, wordSize);
  }

  /**
   * To mbit double.
   *
   * @param d the d
   * @param wordSize the word size
   * @return the double
   */
  public final double toMbit(double d, int wordSize) {
    return BitUnit.MBIT.convert(d, this, wordSize);
  }

  /**
   * To gbit double.
   *
   * @param d the d
   * @param wordSize the word size
   * @return the double
   */
  public final double toGbit(double d, int wordSize) {
    return BitUnit.GBIT.convert(d, this, wordSize);
  }

  /**
   * To tbit double.
   *
   * @param d the d
   * @param wordSize the word size
   * @return the double
   */
  public final double toTbit(double d, int wordSize) {
    return BitUnit.TBIT.convert(d, this, wordSize);
  }

  /**
   * To pbit double.
   *
   * @param d the d
   * @param wordSize the word size
   * @return the double
   */
  public final double toPbit(double d, int wordSize) {
    return BitUnit.PBIT.convert(d, this, wordSize);
  }
}
