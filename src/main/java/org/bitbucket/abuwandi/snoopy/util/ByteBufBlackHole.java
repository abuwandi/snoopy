package org.bitbucket.abuwandi.snoopy.util;

import io.netty.buffer.ByteBuf;
import java.util.Objects;
import lombok.Builder;

/** The type Byte buf black hole. */
public class ByteBufBlackHole implements Disposal<ByteBuf> {

  private final Progress progress;

  /**
   * Instantiates a new Byte buf black hole.
   *
   * @param progress the progress
   */
  @Builder
  ByteBufBlackHole(final Progress progress) {
    this.progress = progress;
  }

  @Override
  public void dispose(final ByteBuf output) {
    final long readableBytes = output.readableBytes();
    IOUtils.release(output);
    if (Objects.nonNull(progress)) {
      progress.updateValue(readableBytes);
    }
  }

  @Override
  public void close() {
    // empty method
  }
}
