package org.bitbucket.abuwandi.snoopy.util;

import io.netty.buffer.ByteBuf;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;
import lombok.Builder;
import org.bitbucket.abuwandi.snoopy.exceptions.SnoopyException;

/** The type Byte buf file collector. */
public class ByteBufFileCollector implements Collector<ByteBuf, Path> {

  private final OutputStream os;
  private final Path path;
  private final Progress progress;

  /**
   * Instantiates a new Byte buf file collector.
   *
   * @param path the path
   * @param progress the progress
   */
  @Builder
  ByteBufFileCollector(final Path path, final Progress progress) {
    try {
      this.os = Files.newOutputStream(path);
    } catch (IOException exc) {
      throw new UncheckedIOException(exc);
    }
    this.path = path;
    this.progress = progress;
  }

  @Override
  public void collect(ByteBuf buffer) {

    final int readableBytes = buffer.readableBytes();

    try {
      buffer.getBytes(0, os, readableBytes);
    } catch (Exception exc) {
      close();
      throw new SnoopyException(exc);
    } catch (Throwable throwable) {
      close();
      throw throwable;
    } finally {
      IOUtils.release(buffer);
    }

    if (Objects.nonNull(progress)) {
      progress.updateValue(readableBytes);
    }
  }

  @Override
  public Path finish() {
    close();
    return path;
  }

  @Override
  public void close() {
    IOUtils.close(os);
  }
}
