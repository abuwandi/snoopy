package org.bitbucket.abuwandi.snoopy.util;

import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.core.Scheduler;
import io.reactivex.rxjava3.functions.Function;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiConsumer;
import java.util.function.Predicate;
import lombok.Builder;
import org.bitbucket.abuwandi.snoopy.client.SnoopySchedulers;
import org.reactivestreams.Publisher;

/** The type Retry strategy. */
public class RetryStrategy {

  private final ExponentialDelay delay;
  private final int maxAttempts;
  private final Predicate<Throwable> errorPredicate;
  private final BiConsumer<Integer, Throwable> onRetryConsumer;
  private final Scheduler scheduler;

  /**
   * Instantiates a new Retry strategy.
   *
   * @param maxAttempts the max attempts
   * @param unit the unit
   * @param lowestDelay the lowest delay
   * @param highestDelay the highest delay
   * @param growBy the grow by
   * @param errorPredicate the error predicate
   * @param onRetryConsumer the on retry consumer
   * @param scheduler the scheduler
   */
  @Builder
  public RetryStrategy(
      final int maxAttempts,
      final TimeUnit unit,
      final int lowestDelay,
      final int highestDelay,
      final int growBy,
      final Predicate<Throwable> errorPredicate,
      final BiConsumer<Integer, Throwable> onRetryConsumer,
      final Scheduler scheduler) {
    this.maxAttempts = maxAttempts;
    this.errorPredicate = Optional.ofNullable(errorPredicate).orElse(error -> true);
    this.onRetryConsumer = Optional.ofNullable(onRetryConsumer).orElse((attempt, error) -> {});
    this.scheduler = Optional.ofNullable(scheduler).orElseGet(SnoopySchedulers::timer);
    this.delay =
        ExponentialDelay.builder()
            .unit(unit)
            .lower(lowestDelay)
            .upper(highestDelay)
            .growBy(growBy)
            .build();
  }

  /**
   * Retry function.
   *
   * @return the function
   */
  public Function<Flowable<Throwable>, Publisher<Long>> retry() {
    if (maxAttempts < 1) {
      return errors -> errors.flatMap(Flowable::error);
    }
    final AtomicInteger counter = new AtomicInteger(0);
    return errors ->
        errors.flatMap(
            throwable -> {
              if (!errorPredicate.test(throwable)) {
                return Flowable.error(throwable);
              }
              final int attempt = counter.incrementAndGet();
              if (attempt <= maxAttempts) {
                try {
                  onRetryConsumer.accept(attempt, throwable);
                } catch (Exception exc) {
                  // ignore
                }
                return Flowable.timer(delay.calculate(attempt), delay.getUnit(), scheduler);
              }
              final MaxRetriesExceededException retriesExceededException =
                  new MaxRetriesExceededException(
                      String.format("Reached %d attempts with no success", maxAttempts), throwable);
              return Flowable.error(retriesExceededException);
            });
  }
}
