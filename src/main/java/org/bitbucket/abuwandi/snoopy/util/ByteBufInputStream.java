package org.bitbucket.abuwandi.snoopy.util;

import io.netty.buffer.ByteBuf;
import io.netty.util.ReferenceCountUtil;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.Objects;

/** The type Byte buf input stream. */
public class ByteBufInputStream extends InputStream {

  private final byte[] singleByteArray;
  private final LinkedList<ByteBuf> buffers;
  private final long size;

  /**
   * Instantiates a new Byte buf input stream.
   *
   * @param buffers the buffers
   */
  public ByteBufInputStream(final LinkedList<ByteBuf> buffers, final long size) {
    this.singleByteArray = new byte[1];
    this.buffers = buffers;
    this.size = size;
  }

  @Override
  public int read() {
    final int readBytes = read(singleByteArray);
    if (readBytes < 1) {
      return -1;
    }
    return singleByteArray[0];
  }

  @Override
  public int read(byte[] destination) {
    return read(destination, 0, destination.length);
  }

  @Override
  public int read(final byte[] destination, final int offset, final int length) {
    if (destination == null || offset < 0 || length < 0 || length > (destination.length - offset)) {
      throw new IllegalArgumentException("Invalid read arguments");
    }
    if (length < 1) {
      return 0;
    }
    if (buffers.isEmpty()) {
      return -1;
    }
    int bytesReadTotal = 0;
    int destinationOffset = offset;
    while (bytesReadTotal < length && !buffers.isEmpty()) {
      final ByteBuf buffer = buffers.peek();
      final int availableBytes = buffer.readableBytes();
      final int bytesRead = Math.min(length - bytesReadTotal, availableBytes);
      buffer.readBytes(destination, destinationOffset, bytesRead);
      bytesReadTotal += bytesRead;
      destinationOffset += bytesRead;
      if (availableBytes == 0) {
        ReferenceCountUtil.safeRelease(buffers.remove());
      }
    }
    return bytesReadTotal;
  }

  @Override
  public void close() {
    if (Objects.nonNull(buffers) && !buffers.isEmpty()) {
      buffers.forEach(ReferenceCountUtil::safeRelease);
    }
  }

  public long size() {
    return size;
  }
}
