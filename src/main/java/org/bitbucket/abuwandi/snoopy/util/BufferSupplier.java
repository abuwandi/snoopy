package org.bitbucket.abuwandi.snoopy.util;

/**
 * The interface Buffer supplier.
 *
 * @param <T> the type parameter
 */
public interface BufferSupplier<T> {

  /**
   * Allocate t.
   *
   * @param capacity the capacity
   * @return the t
   */
  public T allocate(int capacity);
}
