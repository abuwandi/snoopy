package org.bitbucket.abuwandi.snoopy.util;

import io.netty.buffer.ByteBuf;
import java.nio.file.Path;

/** The type Collectors. */
public class Collectors {

  /**
   * Byte buffer file collector collector.
   *
   * @param path the path
   * @param progress the progress
   * @return the collector
   */
  public static Collector<ByteBuf, Path> byteBufferFileCollector(
      final Path path, final Progress progress) {
    return ByteBufFileCollector.builder().path(path).progress(progress).build();
  }

  /**
   * Byte array file collector collector.
   *
   * @param path the path
   * @param progress the progress
   * @return the collector
   */
  public static Collector<byte[], Path> byteArrayFileCollector(
      final Path path, final Progress progress) {
    return ByteArrayFileCollector.builder().path(path).progress(progress).build();
  }

  /**
   * Byte buffer black hole disposal disposal.
   *
   * @param progress the progress
   * @return the disposal
   */
  public static Disposal<ByteBuf> byteBufferBlackHoleDisposal(final Progress progress) {
    return ByteBufBlackHole.builder().progress(progress).build();
  }

  /**
   * Byte array black hole disposal disposal.
   *
   * @param progress the progress
   * @return the disposal
   */
  public static Disposal<byte[]> byteArrayBlackHoleDisposal(final Progress progress) {
    return ByteArrayBlackHole.builder().progress(progress).build();
  }

  private Collectors() {
    throw new AssertionError("Cannot be instantiated");
  }
}
