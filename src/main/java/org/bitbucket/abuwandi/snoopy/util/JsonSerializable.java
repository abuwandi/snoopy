package org.bitbucket.abuwandi.snoopy.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.nio.charset.Charset;
import java.util.Objects;
import java.util.Optional;
import lombok.Builder;
import lombok.NonNull;

/**
 * The type Json serializable.
 *
 * @param <T> the type parameter
 */
public class JsonSerializable<T> implements Serializable {

  private final T content;
  private final ObjectMapper jsonEncoderDecoder;
  private final Charset charset;
  private byte[] encoded;

  /**
   * Instantiates a new Json serializable.
   *
   * @param content the content
   * @param charset the charset
   */
  @Builder
  public JsonSerializable(@NonNull final T content, final Charset charset) {
    this.content = content;
    this.charset = Optional.ofNullable(charset).orElse(Charset.defaultCharset());
    this.jsonEncoderDecoder = new ObjectMapper();
  }

  @Override
  public byte[] serialize() {
    if (Objects.isNull(encoded)) {
      try {
        final String strEncoded = jsonEncoderDecoder.writeValueAsString(content);
        encoded = strEncoded.getBytes(charset);
      } catch (Exception exc) {
        throw new SerializationException(exc);
      }
    }
    return encoded;
  }
}
