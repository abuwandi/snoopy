package org.bitbucket.abuwandi.snoopy.util;

import java.util.Objects;
import lombok.Builder;

/** The type Byte buf black hole. */
public class ByteArrayBlackHole implements Disposal<byte[]> {

  private final Progress progress;

  /**
   * Instantiates a new Byte buf black hole.
   *
   * @param progress the progress
   */
  @Builder
  ByteArrayBlackHole(final Progress progress) {
    this.progress = progress;
  }

  @Override
  public void dispose(final byte[] buffer) {
    if (Objects.nonNull(progress)) {
      progress.updateValue(buffer.length);
    }
  }

  @Override
  public void close() {
    // empty method
  }
}
