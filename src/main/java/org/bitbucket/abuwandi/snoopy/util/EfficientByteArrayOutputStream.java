package org.bitbucket.abuwandi.snoopy.util;

import java.io.OutputStream;
import java.util.LinkedList;

/** The type Efficient byte array output stream. */
public class EfficientByteArrayOutputStream extends OutputStream {

  private final byte[] singleByteArray;
  /** The Buffers. */
  protected final LinkedList<byte[]> buffers;
  /** The Size. */
  protected int size;

  /** Instantiates a new Efficient byte array output stream. */
  public EfficientByteArrayOutputStream() {
    this.singleByteArray = new byte[1];
    this.buffers = new LinkedList<>();
  }

  @Override
  public void write(int singleByte) {
    singleByteArray[0] = (byte) singleByte;
    write(singleByteArray);
  }

  @Override
  public void write(byte[] source) {
    write(source, 0, source.length);
  }

  @Override
  public void write(byte[] source, int offset, int length) {
    if (source == null || offset < 0 || length < 0 || length > (source.length - offset)) {
      throw new IllegalArgumentException("Invalid read arguments");
    }
    if (length < 1) {
      return;
    }
    final byte[] destination = new byte[length];
    System.arraycopy(source, offset, destination, 0, length);
    buffers.offer(destination);
    size += length;
  }
}
