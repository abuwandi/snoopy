package org.bitbucket.abuwandi.snoopy.util;

/** The interface Progress. */
public interface Progress {

  /**
   * Update value.
   *
   * @param value the value
   */
  void updateValue(long value);

  /**
   * Progress float.
   *
   * @return the float
   */
  float progress();
}
