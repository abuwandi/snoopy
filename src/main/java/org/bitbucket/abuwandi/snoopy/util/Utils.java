package org.bitbucket.abuwandi.snoopy.util;

/** The type Utils. */
public class Utils {

  /**
   * Current thread details string.
   *
   * @return the string
   */
  public static String currentThreadDetails() {
    //    return String.format(
    //        "Current thread id: %d name: %s state: %s",
    //        Thread.currentThread().getId(),
    //        Thread.currentThread().getName(),
    //        Thread.currentThread().getState().name());
    return String.format("%s", Thread.currentThread().getId());
  }

  private Utils() {
    throw new AssertionError("Utils cannot be instantiated");
  }
}
