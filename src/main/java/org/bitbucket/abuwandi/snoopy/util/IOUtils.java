package org.bitbucket.abuwandi.snoopy.util;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelFuture;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;
import java.util.Optional;
import org.bitbucket.abuwandi.snoopy.client.ErrorCallback;
import org.bitbucket.abuwandi.snoopy.client.SuccessCallback;
import org.bitbucket.abuwandi.snoopy.client.bootstrap.Connection;
import org.bitbucket.abuwandi.snoopy.client.bootstrap.ConnectionException;

/** The type Io utils. */
public class IOUtils {

  /** The constant DEFAULT_BUFFER_SIZE. */
  public static final int DEFAULT_BUFFER_SIZE = 8192;

  /** The constant EMPTY_BUFFER. */
  public static final byte[] EMPTY_BUFFER = new byte[0];
  /** The constant ZERO. */
  public static final byte ZERO = 0;
  /** The constant EMPTY_STRING. */
  public static final String EMPTY_STRING = "";

  private IOUtils() {
    throw new AssertionError("IOUtils cannot be instantiated");
  }

  /**
   * Copy long.
   *
   * @param src the src
   * @param dst the dst
   * @return the long
   * @throws IOException the io exception
   */
  public static long copy(InputStream src, OutputStream dst) throws IOException {
    return copy(Channels.newChannel(src), Channels.newChannel(dst));
  }

  /**
   * Copy long.
   *
   * @param src the src
   * @param dst the dst
   * @return the long
   * @throws IOException the io exception
   */
  public static long copy(ReadableByteChannel src, WritableByteChannel dst) throws IOException {
    return copy(src, dst, ByteBuffer.allocateDirect(DEFAULT_BUFFER_SIZE));
  }

  /**
   * Copy long.
   *
   * @param src the src
   * @param dst the dst
   * @param buffer the buffer
   * @return the long
   * @throws IOException the io exception
   */
  public static long copy(ReadableByteChannel src, WritableByteChannel dst, final ByteBuffer buffer)
      throws IOException {

    long bytesRead;
    long totalBytesRead = 0;

    while ((bytesRead = src.read(buffer)) > -1) {
      totalBytesRead += bytesRead;
      buffer.flip();
      dst.write(buffer);
      buffer.compact();
    }

    buffer.flip();

    while (buffer.hasRemaining()) {
      dst.write(buffer);
    }

    return totalBytesRead;
  }

  /**
   * Close.
   *
   * @param closeables the closeables
   */
  public static void close(Closeable... closeables) {

    if (Objects.isNull(closeables) || closeables.length < 1) {
      return;
    }
    for (Closeable closeable : closeables) {
      try {
        if (Objects.nonNull(closeable)) {
          closeable.close();
        }
      } catch (Exception exc) {
        // ignore
      }
    }
  }

  /**
   * Release.
   *
   * @param buffers the buffers
   */
  public static void release(ByteBuf... buffers) {
    if (Objects.isNull(buffers) || buffers.length < 1) {
      return;
    }
    for (ByteBuf buffer : buffers) {
      try {
        if (Objects.nonNull(buffer)) {
          buffer.release();
        }
      } catch (Exception exc) {
        // ignore
      }
    }
  }

  /**
   * Release boolean.
   *
   * @param connection the connection
   * @return the boolean
   */
  public static boolean release(Connection connection) {
    if (Objects.nonNull(connection)) {
      connection.release();
      return true;
    }
    return false;
  }

  /**
   * Safe delete boolean.
   *
   * @param path the path
   * @return the boolean
   */
  public static boolean safeDelete(final Path path) {
    try {
      return Objects.nonNull(path) && Files.deleteIfExists(path);
    } catch (Throwable exc) {
      return false;
    }
  }

  /**
   * Handle channel future.
   *
   * @param channelFuture the channel future
   * @param successCallback the success callback
   * @param errorCallback the error callback
   */
  public static void handleChannelFuture(
      final ChannelFuture channelFuture,
      final SuccessCallback successCallback,
      final ErrorCallback errorCallback) {
    if (channelFuture.isDone()) {
      if (channelFuture.isSuccess()) {
        successCallback.onSuccess();
        return;
      }
      final ConnectionException connectionException =
          Optional.ofNullable(channelFuture.cause())
              .map(ConnectionException::new)
              .orElseGet(() -> new ConnectionException("Connection write failed"));
      errorCallback.onError(connectionException);
      return;
    }
    channelFuture.addListener(
        cf -> {
          if (channelFuture.isSuccess()) {
            successCallback.onSuccess();
            return;
          }
          final ConnectionException connectionException =
              Optional.ofNullable(channelFuture.cause())
                  .map(ConnectionException::new)
                  .orElseGet(() -> new ConnectionException("Connection write failed"));
          errorCallback.onError(connectionException);
        });
  }
}
