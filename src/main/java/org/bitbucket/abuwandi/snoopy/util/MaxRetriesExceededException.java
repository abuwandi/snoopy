package org.bitbucket.abuwandi.snoopy.util;

/** The type Max retries exceeded exception. */
public class MaxRetriesExceededException extends RuntimeException {

  /**
   * Instantiates a new Max retries exceeded exception.
   *
   * @param message the message
   */
  public MaxRetriesExceededException(String message) {
    super(message);
  }

  /**
   * Instantiates a new Max retries exceeded exception.
   *
   * @param message the message
   * @param cause the cause
   */
  public MaxRetriesExceededException(String message, Throwable cause) {
    super(message, cause);
  }

  /**
   * Instantiates a new Max retries exceeded exception.
   *
   * @param cause the cause
   */
  public MaxRetriesExceededException(Throwable cause) {
    super(cause);
  }
}
