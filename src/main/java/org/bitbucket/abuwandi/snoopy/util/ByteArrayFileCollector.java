package org.bitbucket.abuwandi.snoopy.util;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;
import lombok.Builder;
import org.bitbucket.abuwandi.snoopy.exceptions.SnoopyException;

/** The type Byte buf file collector. */
public class ByteArrayFileCollector implements Collector<byte[], Path> {

  private final OutputStream os;
  private final Path path;
  private final Progress progress;

  /**
   * Instantiates a new Byte buf file collector.
   *
   * @param path the path
   * @param progress the progress
   */
  @Builder
  ByteArrayFileCollector(final Path path, final Progress progress) {
    try {
      this.os = Files.newOutputStream(path);
    } catch (IOException exc) {
      throw new UncheckedIOException(exc);
    }
    this.path = path;
    this.progress = progress;
  }

  @Override
  public void collect(byte[] buffer) {
    if (buffer.length < 1) {
      return;
    }
    try {
      os.write(buffer);
    } catch (Exception exc) {
      close();
      throw new SnoopyException(exc);
    } catch (Throwable throwable) {
      close();
      throw throwable;
    }

    if (Objects.nonNull(progress)) {
      progress.updateValue(buffer.length);
    }
  }

  @Override
  public Path finish() {
    close();
    return path;
  }

  @Override
  public void close() {
    IOUtils.close(os);
  }
}
