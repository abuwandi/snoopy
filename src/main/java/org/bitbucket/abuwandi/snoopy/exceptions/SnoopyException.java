package org.bitbucket.abuwandi.snoopy.exceptions;

/** The type Snoopy exception. */
public class SnoopyException extends RuntimeException {

  /**
   * Instantiates a new Snoopy exception.
   *
   * @param message the message
   */
  public SnoopyException(String message) {
    super(message);
  }

  /**
   * Instantiates a new Snoopy exception.
   *
   * @param cause the cause
   */
  public SnoopyException(Throwable cause) {
    super(cause);
  }

  /**
   * Instantiates a new Snoopy exception.
   *
   * @param message the message
   * @param cause the cause
   */
  public SnoopyException(String message, Throwable cause) {
    super(message, cause);
  }
}
