package org.bitbucket.abuwandi.snoopy.client.http;

import io.netty.buffer.ByteBuf;
import io.netty.handler.codec.http.HttpContent;
import java.util.concurrent.atomic.AtomicBoolean;
import lombok.Builder;
import org.bitbucket.abuwandi.snoopy.util.BufferSupplier;

/** The type Empty request body. */
public class EmptyRequestBody implements RequestBody {

  private final AtomicBoolean completed;

  /** Instantiates a new Empty request body. */
  @Builder
  public EmptyRequestBody() {
    this.completed = new AtomicBoolean(false);
  }

  @Override
  public HttpContent next(BufferSupplier<ByteBuf> supplier) {
    return null;
  }

  @Override
  public CharSequence contentType() {
    return null;
  }

  @Override
  public long contentLength() {
    return 0;
  }

  @Override
  public boolean pause() {
    return false;
  }

  @Override
  public boolean resume() {
    return false;
  }

  @Override
  public boolean isPaused() {
    return false;
  }

  @Override
  public boolean canResume() {
    return false;
  }

  @Override
  public boolean complete() {
    return this.completed.compareAndSet(false, true);
  }

  @Override
  public boolean isCompleted() {
    return completed.get();
  }
}
