package org.bitbucket.abuwandi.snoopy.client.http;

import java.util.Arrays;
import java.util.Optional;
import org.apache.commons.lang3.StringUtils;

/** The enum Http method. */
public enum HttpMethod {
  /** The Head. */
  HEAD(
      "head",
      "Asks for a response of a resource but without the response body\nfor more info please visit https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/HEAD",
      false,
      false,
      true,
      true,
      true),
  /** The Get. */
  GET(
      "get",
      "Requests a representation of a resource\nfor more info please visit https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/GET",
      false,
      true,
      true,
      true,
      true),
  /** The Post. */
  POST(
      "post",
      "Creates a new resource\nfor more info please visit https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/POST",
      true,
      true,
      false,
      false,
      false),
  /** The Put. */
  PUT(
      "put",
      "Creates a new resource or replaces a representation of a resource\nfor more info please visit https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/POST",
      true,
      false,
      true,
      false,
      false),
  /** The Patch. */
  PATCH(
      "patch",
      "Applies partial modifications to a resource\nfor more info please visit https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/PATCH",
      true,
      true,
      false,
      false,
      false),
  /** The Delete. */
  DELETE(
      "delete",
      "Deletes a resource\nfor more info please visit https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/DELETE",
      true,
      true,
      true,
      false,
      false),
  /** The Options. */
  OPTIONS(
      "options",
      "Describes the communication options for a resource\nfor more info please visit https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/OPTIONS",
      false,
      true,
      true,
      false,
      true),
  /** The Trace. */
  TRACE(
      "trace",
      "Performs a message loop-back test along the path of a resource\nfor more info please visit https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/TRACE",
      false,
      false,
      true,
      false,
      false);

  private final String value;
  private final String description;
  private final boolean hasRequestBody;
  private final boolean hasResponseBody;
  private final boolean idempotent;
  private final boolean cacheable;
  private final boolean safe;

  HttpMethod(
      final String value,
      final String description,
      final boolean hasRequestBody,
      final boolean hasResponseBody,
      final boolean idempotent,
      final boolean cacheable,
      final boolean safe) {
    this.value = value;
    this.description = description;
    this.hasRequestBody = hasRequestBody;
    this.hasResponseBody = hasResponseBody;
    this.idempotent = idempotent;
    this.cacheable = cacheable;
    this.safe = safe;
  }

  /**
   * Value string.
   *
   * @return the string
   */
  public String value() {
    return value;
  }

  /**
   * Description string.
   *
   * @return the string
   */
  public String description() {
    return description;
  }

  /**
   * Is idempotent boolean.
   *
   * @return the boolean
   */
  public boolean isIdempotent() {
    return idempotent;
  }

  /**
   * Is safe boolean.
   *
   * @return the boolean
   */
  public boolean isSafe() {
    return safe;
  }

  /**
   * Is cacheable boolean.
   *
   * @return the boolean
   */
  public boolean isCacheable() {
    return cacheable;
  }

  /**
   * Has request body boolean.
   *
   * @return the boolean
   */
  public boolean hasRequestBody() {
    return hasRequestBody;
  }

  /**
   * Has response body boolean.
   *
   * @return the boolean
   */
  public boolean hasResponseBody() {
    return hasResponseBody;
  }

  /**
   * For value http method.
   *
   * @param name the name
   * @return the http method
   */
  public static HttpMethod forValue(final String name) {
    return Optional.ofNullable(name)
        .filter(StringUtils::isNoneBlank)
        .map(
            target ->
                Arrays.stream(HttpMethod.values())
                    .filter(scheme -> scheme.value().equalsIgnoreCase(name))
                    .findFirst()
                    .orElseThrow(() -> new IllegalArgumentException("Invalid argument value")))
        .orElseThrow(() -> new IllegalArgumentException("Invalid argument value"));
  }
}
