package org.bitbucket.abuwandi.snoopy.client.http;

import io.netty.handler.codec.http.HttpVersion;
import java.net.URI;
import java.util.List;
import java.util.Map;
import org.bitbucket.abuwandi.snoopy.client.ApplicationProtocol;
import org.bitbucket.abuwandi.snoopy.client.HandshakeCallback;
import org.bitbucket.abuwandi.snoopy.client.ProgressCallback;
import org.bitbucket.abuwandi.snoopy.client.StatusCallback;
import org.bitbucket.abuwandi.snoopy.client.ThroughputCallback;
import org.bitbucket.abuwandi.snoopy.client.authentication.Authentication;
import org.bitbucket.abuwandi.snoopy.client.authentication.TokenProvider;
import org.bitbucket.abuwandi.snoopy.model.Token;

/** The type Http post request builder. */
public class HttpPostRequestBuilder extends HttpRequestBodyBuilder {

  /** The Multipart. */
  protected Object[] multipart;

  /** Instantiates a new Http post request builder. */
  HttpPostRequestBuilder() {
    super(HttpMethod.POST);
  }

  /**
   * Multipart http post request builder.
   *
   * @param multipart the multipart
   * @return the http post request builder
   */
  public HttpPostRequestBuilder multipart(Object... multipart) {
    this.multipart = multipart;
    return this;
  }

  @Override
  public HttpPostRequestBuilder body(final Object body) {
    super.body(body);
    return this;
  }

  @Override
  public HttpPostRequestBuilder tokenProvider(TokenProvider<Authentication, Token> tokenProvider) {
    super.tokenProvider(tokenProvider);
    return this;
  }

  @Override
  public HttpPostRequestBuilder reference(String reference) {
    super.reference(reference);
    return this;
  }

  @Override
  public HttpPostRequestBuilder version(HttpVersion version) {
    this.version = version;
    return this;
  }

  @Override
  public HttpPostRequestBuilder host(CharSequence host) {
    super.host(host);
    return this;
  }

  @Override
  public HttpPostRequestBuilder closeConnection() {
    super.closeConnection();
    return this;
  }

  @Override
  public HttpPostRequestBuilder keepConnectionAlive() {
    super.keepConnectionAlive();
    return this;
  }

  @Override
  public HttpPostRequestBuilder uri(URI uri) {
    super.uri(uri);
    return this;
  }

  public HttpPostRequestBuilder responseStatusCallback(
      final StatusCallback responseStatusCallback) {
    this.responseStatusCallback = responseStatusCallback;
    return this;
  }

  public HttpPostRequestBuilder responseThroughputCallback(
      final ThroughputCallback responseThroughputCallback) {
    this.responseThroughputCallback = responseThroughputCallback;
    return this;
  }

  public HttpPostRequestBuilder responseProgressCallback(
      final ProgressCallback responseProgressCallback) {
    this.responseProgressCallback = responseProgressCallback;
    return this;
  }

  public HttpPostRequestBuilder statusCallback(final StatusCallback statusCallback) {
    this.statusCallback = statusCallback;
    return this;
  }

  public HttpPostRequestBuilder handshakeCallback(final HandshakeCallback handshakeCallback) {
    this.handshakeCallback = handshakeCallback;
    return this;
  }

  public HttpPostRequestBuilder progressCallback(final ProgressCallback progressCallback) {
    this.progressCallback = progressCallback;
    return this;
  }

  public HttpPostRequestBuilder throughputCallback(final ThroughputCallback throughputCallback) {
    this.throughputCallback = throughputCallback;
    return this;
  }

  @Override
  public HttpPostRequestBuilder header(CharSequence name, CharSequence... values) {
    super.header(name, values);
    return this;
  }

  @Override
  public HttpPostRequestBuilder headers(Map<CharSequence, List<CharSequence>> headerMap) {
    super.headers(headerMap);
    return this;
  }

  @Override
  public HttpPostRequestBuilder followRedirects(boolean followRedirects) {
    super.followRedirects(followRedirects);
    return this;
  }

  @Override
  public HttpPostRequestBuilder failIfNotSuccessfulResponse(boolean failIfNotSuccessfulResponse) {
    this.failIfNotSuccessfulResponse = failIfNotSuccessfulResponse;
    return this;
  }

  public HttpPostRequestBuilder connectionRetry(final int connectionRetry) {
    this.connectionRetry = connectionRetry;
    return this;
  }

  public HttpPostRequestBuilder ignoreResponseBody(final boolean ignoreResponseBody) {
    this.ignoreResponseBody = ignoreResponseBody;
    return this;
  }

  @Override
  protected HttpPostRequestBuilder redirectsDepth(int redirectsDepth) {
    super.redirectsDepth(redirectsDepth);
    return this;
  }

  protected HttpPostRequestBuilder applicationProtocol(
      final ApplicationProtocol applicationProtocol) {
    this.applicationProtocol = applicationProtocol;
    return this;
  }
}
