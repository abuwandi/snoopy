package org.bitbucket.abuwandi.snoopy.client.handlers;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.ssl.SslHandler;
import io.netty.handler.ssl.SslHandshakeCompletionEvent;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.SSLSession;
import org.bitbucket.abuwandi.snoopy.client.RequestRegistrar;
import org.bitbucket.abuwandi.snoopy.client.SnoopyRequest;

/**
 * The type Snoopy ssl handler.
 *
 * @param <T> the type parameter
 */
public class SnoopySslHandler<T> extends ChannelInboundHandlerAdapter
    implements RequestRegistrar<T, SnoopyRequest<T>> {

  private final AtomicReference<SnoopyRequest<T>> atomicRequest;

  /** Instantiates a new Snoopy ssl handler. */
  public SnoopySslHandler() {
    atomicRequest = new AtomicReference<>(null);
  }

  @Override
  public void userEventTriggered(ChannelHandlerContext ctx, Object evt) {
    if (evt == SslHandshakeCompletionEvent.SUCCESS) {
      final SnoopyRequest<T> request = atomicRequest.get();
      if (Objects.nonNull(request)) {
        final SslHandler handler = ctx.pipeline().get(SslHandler.class);
        final SSLEngine engine = handler.engine();
        final SSLSession sslSession = engine.getSession();
        request.handshake(sslSession);
      }
    }
    ctx.fireUserEventTriggered(evt);
  }

  @Override
  public void channelInactive(ChannelHandlerContext ctx) {
    deregisterAll();
    ctx.fireChannelInactive();
  }

  @Override
  public void channelUnregistered(ChannelHandlerContext ctx) {
    deregisterAll();
    ctx.fireChannelUnregistered();
  }

  @Override
  public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {
    deregisterAll();
    super.handlerRemoved(ctx);
  }

  @Override
  public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
    deregisterAll();
    super.exceptionCaught(ctx, cause);
  }

  @Override
  public boolean register(SnoopyRequest<T> registrant) {
    return atomicRequest.compareAndSet(null, registrant);
  }

  @Override
  public boolean deregister(SnoopyRequest<T> registrant) {
    return atomicRequest.compareAndSet(registrant, null);
  }

  @Override
  public void deregisterAll() {
    atomicRequest.set(null);
  }
}
