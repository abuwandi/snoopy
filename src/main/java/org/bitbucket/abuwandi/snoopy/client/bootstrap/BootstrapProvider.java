package org.bitbucket.abuwandi.snoopy.client.bootstrap;

import io.netty.bootstrap.Bootstrap;
import io.reactivex.rxjava3.core.Single;
import java.net.URI;

/** The interface Bootstrap provider. */
public interface BootstrapProvider {

  /**
   * Bootstrap single.
   *
   * @param uri the uri
   * @return the single
   */
  Single<Bootstrap> bootstrap(URI uri);

  /** Shutdown. */
  void shutdown();
}
