package org.bitbucket.abuwandi.snoopy.client;

/** The interface Status callback. */
public interface StatusCallback {

  /** The constant DO_NOTHING_STATUS_CALLBACK. */
  StatusCallback DO_NOTHING_STATUS_CALLBACK = newStatus -> {};

  /**
   * On status change.
   *
   * @param newStatus the new status
   */
  void onStatusChange(Status newStatus);
}
