package org.bitbucket.abuwandi.snoopy.client.http.authentication;

import org.bitbucket.abuwandi.snoopy.client.ClientException;

/** The type Http authentication exception. */
public class HttpAuthenticationException extends ClientException {

  /**
   * Instantiates a new Http authentication exception.
   *
   * @param message the message
   */
  public HttpAuthenticationException(String message) {
    super(message);
  }

  /**
   * Instantiates a new Http authentication exception.
   *
   * @param cause the cause
   */
  public HttpAuthenticationException(Throwable cause) {
    super(cause);
  }

  /**
   * Instantiates a new Http authentication exception.
   *
   * @param message the message
   * @param cause the cause
   */
  public HttpAuthenticationException(String message, Throwable cause) {
    super(message, cause);
  }
}
