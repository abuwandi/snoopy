package org.bitbucket.abuwandi.snoopy.client.http;

import io.netty.buffer.ByteBufAllocator;
import io.netty.buffer.Unpooled;
import io.netty.handler.codec.http.DefaultFullHttpRequest;
import io.netty.handler.codec.http.DefaultHttpRequest;
import io.netty.handler.codec.http.EmptyHttpHeaders;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpContent;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpObject;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.HttpVersion;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.core.Single;
import java.net.URI;
import java.net.URL;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import javax.net.ssl.SSLSession;
import lombok.Builder;
import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;
import org.bitbucket.abuwandi.snoopy.client.ApplicationProtocol;
import org.bitbucket.abuwandi.snoopy.client.Completable;
import org.bitbucket.abuwandi.snoopy.client.HandshakeCallback;
import org.bitbucket.abuwandi.snoopy.client.Pausable;
import org.bitbucket.abuwandi.snoopy.client.ProgressCallback;
import org.bitbucket.abuwandi.snoopy.client.SnoopyRequest;
import org.bitbucket.abuwandi.snoopy.client.SslSessionInfo;
import org.bitbucket.abuwandi.snoopy.client.Status;
import org.bitbucket.abuwandi.snoopy.client.StatusCallback;
import org.bitbucket.abuwandi.snoopy.client.ThroughputCallback;
import org.bitbucket.abuwandi.snoopy.client.authentication.Authenticator;
import org.bitbucket.abuwandi.snoopy.client.bootstrap.Connection;
import org.bitbucket.abuwandi.snoopy.client.http.util.Extractors;
import org.bitbucket.abuwandi.snoopy.util.IOUtils;
import org.bitbucket.abuwandi.snoopy.util.RetryStrategy;
import org.bitbucket.abuwandi.snoopy.util.StopWatch;

/** The type Snoopy http request. */
public class SnoopyHttpRequest implements SnoopyRequest<HttpObject>, Pausable, Completable {

  public String reference() {
    return reference;
  }

  public Connection connection() {
    return this.connection.get();
  }

  public Single<SnoopyHttpResponse> execute() {
    return Single.just(this)
        .flatMap(this::verify)
        .flatMap(authenticator::authenticate)
        .map(SnoopyRequest::uri)
        .flatMap(this::connect)
        .flatMap(this::submit)
        .flatMap(this::followRedirects)
        .flatMap(this::failIfNotSuccessfulResponse);
  }

  public void setStatus(final Status status) {
    final Status before = this.status.get();
    final Status after =
        this.status.updateAndGet(
            current -> {
              if (current == Status.COMPLETE) {
                return current;
              }
              if (current == Status.FAILED && status != Status.RETRY) {
                return current;
              }
              return status;
            });
    if (before != after) {
      if (after == Status.COMPLETE) {
        connection.get().startResponseTimeout();
      }
      statusChangeCallback.onStatusChange(status);
    }
  }

  @Override
  public void setThroughput(final long throughput) {
    final long before = this.throughput.get();
    final long after = this.throughput.updateAndGet(current -> throughput);
    if (before != after) {
      throughputChangeCallback.onThroughputChange(throughput);
    }
  }

  @Override
  public boolean complete() {
    final boolean success = Objects.isNull(body) || body.isCompleted();
    if (success) {
      setStatus(Status.COMPLETE);
      requestTime.set(stopWatch.time());
      response.get().startStopWatch();
    }
    return success;
  }

  @Override
  public boolean isCompleted() {
    return body.isCompleted();
  }

  @Override
  public boolean pause() {
    final boolean success = Objects.nonNull(body) && body.pause();
    if (success) {
      setStatus(Status.IDLE);
    }
    return success;
  }

  @Override
  public boolean resume() {
    final boolean success = Objects.nonNull(body) && body.resume();
    if (success) {
      setStatus(Status.ACTIVE);
    }
    if (Objects.nonNull(connection.get())) {
      connection.get().writeAndFlush(this, this::onSuccess, this::onError);
    }
    return success;
  }

  @Override
  public boolean isPaused() {
    return body.isPaused();
  }

  public Status getStatus() {
    return status.get();
  }

  public long time() {
    return requestTime.get();
  }

  @Override
  public URI uri() {
    return uri;
  }

  @Override
  public void handshake(SSLSession sslSession) {
    try {
      this.handshakeCallback.onHandshake(
          SslSessionInfo.builder()
              .certificates(sslSession.getPeerCertificateChain())
              .cipherSuite(sslSession.getCipherSuite())
              .host(sslSession.getPeerHost())
              .port(sslSession.getPeerPort())
              .protocol(sslSession.getProtocol())
              .build());
    } catch (Exception exc) {
      throw new HttpRequestException(exc);
    }
  }

  /**
   * Consume as string string.
   *
   * @return the string
   */
  public String consumeAsString() {
    return execute().flatMap(SnoopyHttpResponse::bodyAsString).blockingGet();
  }

  /**
   * Consume as t.
   *
   * @param <T> the type parameter
   * @param clazz the clazz
   * @return the t
   */
  public <T> T consumeAs(final Class<T> clazz) {
    return execute()
        .flatMap(
            response ->
                response
                    .bodyAs(clazz)
                    .collect(LinkedList<T>::new, LinkedList::add)
                    .map(LinkedList::getFirst))
        .blockingGet();
  }

  /**
   * Consume as file path.
   *
   * @param path the path
   * @return the path
   */
  public Path consumeAsFile(final Path path) {
    return execute().flatMap(response -> Extractors.saveBodyToFile(response, path)).blockingGet();
  }

  /**
   * Headers http headers.
   *
   * @return the http headers
   */
  public HttpHeaders headers() {
    return this.nativeRequest.headers();
  }

  /**
   * Protocol version http version.
   *
   * @return the http version
   */
  public HttpVersion protocolVersion() {
    return this.nativeRequest.protocolVersion();
  }

  /**
   * Method http method.
   *
   * @return the http method
   */
  public HttpMethod method() {
    return method;
  }

  private Single<Connection> connect(final URI uri) {
    return applicationProtocol.connect(uri).retryWhen(this.connectionRetryStrategy.retry());
  }

  private Single<SnoopyHttpRequest> verify(final SnoopyHttpRequest request) {
    if (request.executed.compareAndSet(false, true)) {
      return Single.just(request);
    }
    return Single.error(new HttpRequestException("Request is already executed"));
  }

  private Single<SnoopyHttpResponse> followRedirects(final SnoopyHttpResponse response) {
    try {
      if (!followRedirects || !response.isRedirect()) {
        return Single.just(response);
      }
      final CharSequence location = response.location();
      if (Objects.isNull(location)) {
        return Single.error(new HttpRequestException("No new location found."));
      }
      final URI uri = new URL(location.toString()).toURI();
      final int redirectsDepth = this.redirectsDepth.incrementAndGet();
      if (redirectsDepth > 5) {
        return Single.error(new HttpRequestException("Too many redirects..."));
      }
      response.release("Releasing redirect response");
      return builder.uri(uri).redirectsDepth(redirectsDepth).execute();
    } catch (Throwable throwable) {
      return Single.error(throwable);
    }
  }

  private Single<SnoopyHttpResponse> failIfNotSuccessfulResponse(
      final SnoopyHttpResponse response) {
    if (failIfNotSuccessfulResponse && !response.isSuccessful()) {
      return Single.error(
          new HttpRequestException(
              String.format(
                  "Response returned has http status code: %d phrase: %s",
                  response.status().code(), response.status().reasonPhrase())));
    }
    return Single.just(response);
  }

  private Single<SnoopyHttpResponse> submit(@NonNull final Connection connection) {
    return Single.just(connection)
        .flatMap(
            conn -> {
              SnoopyHttpResponse response = null;
              try {
                if (!conn.register(this)) {
                  return Single.error(new HttpRequestException("Request cannot be registered."));
                }
                this.connection.set(conn);
                response =
                    SnoopyHttpResponse.builder()
                        .request(SnoopyHttpRequest.this)
                        .statusCallback(responseStatusChangeCallback)
                        .throughputCallback(responseThroughputChangeCallback)
                        .progressCallback(responseProgressCallback)
                        .ignoreBody(ignoreResponseBody)
                        .build();
                if (!conn.register(response) || !this.response.compareAndSet(null, response)) {
                  response.release("Response cannot be registered.");
                  return Single.error(new HttpRequestException("Response cannot be registered."));
                }
                stopWatch.start();
                if (nativeRequest instanceof FullHttpRequest) {
                  conn.writeAndFlush(nativeRequest, this::onSuccess, this::onError);
                } else {
                  conn.writeAndFlush(this, this::onSuccess, this::onError);
                }
                return Single.just(response);
              } catch (Exception exc) {
                final HttpRequestException requestException = new HttpRequestException(exc);
                Optional.ofNullable(response)
                    .map(resp -> resp.onError(requestException))
                    .orElseGet(() -> IOUtils.release(conn));
                return Single.error(requestException);
              } catch (Throwable throwable) {
                Optional.ofNullable(response)
                    .map(resp -> resp.onError(throwable))
                    .orElseGet(() -> IOUtils.release(conn));
                return Single.error(throwable);
              }
            });
  }

  /** On success. */
  public void onSuccess() {
    complete();
  }

  /**
   * On progress.
   *
   * @param progress the progress
   */
  public void onProgress(final long progress) {
    setStatus(Status.ACTIVE);
    progressCallback.onProgress(progress);
  }

  /**
   * On error.
   *
   * @param throwable the throwable
   */
  public void onError(final Throwable throwable) {
    requestTime.set(stopWatch.time());
    setStatus(Status.FAILED);
    response
        .get()
        .onError(
            Optional.of(throwable)
                .filter(HttpRequestException.class::isInstance)
                .orElseGet(() -> new HttpRequestException(throwable)));
  }

  /**
   * Native request http request.
   *
   * @return the http request
   */
  public HttpRequest nativeRequest() {
    return nativeRequest;
  }

  /**
   * To flowable flowable.
   *
   * @param allocator the allocator
   * @return the flowable
   */
  public Flowable<? extends HttpObject> toFlowable(final ByteBufAllocator allocator) {
    if (interrupted.compareAndSet(false, true)) {
      return Flowable.<HttpObject>just(nativeRequest).concatWith(bodyFlowable(allocator));
    }
    return bodyFlowable(allocator);
  }

  private Flowable<HttpContent> bodyFlowable(final ByteBufAllocator allocator) {
    return Optional.ofNullable(body)
        .map(body -> body.toFlowable(capacity -> allocator.directBuffer(capacity, capacity)))
        .orElseGet(Flowable::empty);
  }

  private HttpRequest createNativeHttpRequest(
      final HttpVersion version, final HttpHeaders headers) {
    if (!method.hasRequestBody() || Objects.isNull(body)) {
      return new DefaultFullHttpRequest(
          version,
          io.netty.handler.codec.http.HttpMethod.valueOf(method.name()),
          uri.getRawPath()
              + Optional.ofNullable(uri.getRawQuery())
                  .filter(StringUtils::isNoneBlank)
                  .map(query -> "?" + query)
                  .orElse(""),
          Unpooled.EMPTY_BUFFER,
          headers,
          EmptyHttpHeaders.INSTANCE);
    }
    return new DefaultHttpRequest(
        version,
        io.netty.handler.codec.http.HttpMethod.valueOf(method.name()),
        uri.getRawPath()
            + Optional.ofNullable(uri.getRawQuery())
                .filter(StringUtils::isNoneBlank)
                .map(query -> "?" + query)
                .orElse(""),
        headers);
  }

  private final URI uri;
  private final String reference;
  private final HttpRequest nativeRequest;
  private final AtomicLong requestTime;
  private final AtomicReference<Connection> connection;
  private final HttpMethod method;
  private final boolean followRedirects;
  private final AtomicInteger redirectsDepth;
  private final HttpRequestBuilder builder;
  private final RequestBody body;
  private final ApplicationProtocol applicationProtocol;
  private final Authenticator<HttpObject, SnoopyHttpRequest> authenticator;
  private final boolean failIfNotSuccessfulResponse;
  private final StatusCallback responseStatusChangeCallback;
  private final ThroughputCallback responseThroughputChangeCallback;
  private final ProgressCallback responseProgressCallback;
  private final StatusCallback statusChangeCallback;
  private final ThroughputCallback throughputChangeCallback;
  private final HandshakeCallback handshakeCallback;
  private final AtomicReference<Status> status;
  private final AtomicLong throughput;
  private final AtomicBoolean executed;
  private final RetryStrategy connectionRetryStrategy;
  private final ProgressCallback progressCallback;
  private final AtomicBoolean interrupted;
  private final StopWatch stopWatch;
  private final AtomicReference<SnoopyHttpResponse> response;
  private final boolean ignoreResponseBody;

  /**
   * Instantiates a new Snoopy http request.
   *
   * @param uri the uri
   * @param version the version
   * @param headers the headers
   * @param method the method
   * @param reference the reference
   * @param followRedirects the follow redirects
   * @param redirectsDepth the redirects depth
   * @param builder the builder
   * @param applicationProtocol the application protocol
   * @param authenticator the authenticator
   * @param body the body
   * @param failIfNotSuccessfulResponse the fail if not successful response
   * @param responseStatusCallback the response status callback
   * @param responseThroughputCallback the response throughput callback
   * @param responseProgressCallback the response progress callback
   * @param statusChangeCallback the status change callback
   * @param throughputChangeCallback the throughput change callback
   * @param progressCallback the progress callback
   * @param handshakeCallback the handshake callback
   * @param connectionRetryStrategy the connection retry strategy
   * @param ignoreResponseBody the ignore response body
   */
  @Builder
  SnoopyHttpRequest(
      final URI uri,
      final HttpVersion version,
      final HttpHeaders headers,
      final HttpMethod method,
      final String reference,
      final boolean followRedirects,
      final int redirectsDepth,
      final HttpRequestBuilder builder,
      final ApplicationProtocol applicationProtocol,
      final Authenticator<HttpObject, SnoopyHttpRequest> authenticator,
      final RequestBody body,
      final boolean failIfNotSuccessfulResponse,
      final StatusCallback responseStatusCallback,
      final ThroughputCallback responseThroughputCallback,
      final ProgressCallback responseProgressCallback,
      final StatusCallback statusChangeCallback,
      final ThroughputCallback throughputChangeCallback,
      final ProgressCallback progressCallback,
      final HandshakeCallback handshakeCallback,
      final RetryStrategy connectionRetryStrategy,
      final boolean ignoreResponseBody) {
    this.uri = uri;
    this.method = method;
    this.requestTime = new AtomicLong(0);
    this.reference = reference;
    this.connection = new AtomicReference<>(null);
    this.followRedirects = followRedirects;
    this.redirectsDepth = new AtomicInteger(redirectsDepth);
    this.builder = builder;
    this.applicationProtocol = applicationProtocol;
    this.authenticator = authenticator;
    this.failIfNotSuccessfulResponse = failIfNotSuccessfulResponse;
    this.responseStatusChangeCallback = responseStatusCallback;
    this.responseThroughputChangeCallback = responseThroughputCallback;
    this.responseProgressCallback = responseProgressCallback;
    this.statusChangeCallback =
        Optional.ofNullable(statusChangeCallback).orElse(StatusCallback.DO_NOTHING_STATUS_CALLBACK);
    this.throughputChangeCallback =
        Optional.ofNullable(throughputChangeCallback)
            .orElse(ThroughputCallback.DO_NOTHING_THROUGHPUT_CALLBACK);
    this.progressCallback =
        Optional.ofNullable(progressCallback).orElse(ProgressCallback.DO_NOTHING_PROGRESS_CALLBACK);
    this.handshakeCallback =
        Optional.ofNullable(handshakeCallback)
            .orElse(HandshakeCallback.DO_NOTHING_HANDSHAKE_CALLBACK);
    this.body = body;
    this.throughput = new AtomicLong(0);
    this.status = new AtomicReference<>(null);
    this.executed = new AtomicBoolean(false);
    setStatus(Status.PENDING);
    this.connectionRetryStrategy =
        Optional.ofNullable(connectionRetryStrategy)
            .orElseGet(() -> RetryStrategy.builder().build());
    this.interrupted = new AtomicBoolean(false);
    this.stopWatch = StopWatch.builder().build();
    this.response = new AtomicReference<>(null);
    this.ignoreResponseBody = ignoreResponseBody;
    this.nativeRequest = createNativeHttpRequest(version, headers);
  }
}
