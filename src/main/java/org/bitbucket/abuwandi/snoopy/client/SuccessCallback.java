package org.bitbucket.abuwandi.snoopy.client;

/** The interface Success callback. */
public interface SuccessCallback {

  /** The constant DO_NOTHING_SUCCESS_CALLBACK. */
  SuccessCallback DO_NOTHING_SUCCESS_CALLBACK = () -> {};

  /** On success. */
  void onSuccess();
}
