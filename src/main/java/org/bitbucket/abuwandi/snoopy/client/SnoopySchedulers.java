package org.bitbucket.abuwandi.snoopy.client;

import io.reactivex.rxjava3.core.Scheduler;
import io.reactivex.rxjava3.internal.schedulers.RxThreadFactory;
import io.reactivex.rxjava3.schedulers.Schedulers;
import java.util.concurrent.Executors;

/** The type Snoopy schedulers. */
public final class SnoopySchedulers {

  private static final Scheduler IO;
  private static final Scheduler TIMER_SCHEDULER;

  static {
    IO =
        Schedulers.from(
            Executors.newFixedThreadPool(
                Runtime.getRuntime().availableProcessors(),
                new RxThreadFactory("snoopy-response-worker-")));

    TIMER_SCHEDULER =
        Schedulers.from(
            Executors.newFixedThreadPool(
                Runtime.getRuntime().availableProcessors(),
                new RxThreadFactory("snoopy-timer-worker-")));
  }

  private SnoopySchedulers() {
    throw new AssertionError("This class cannot be instantiated");
  }

  /**
   * Io scheduler.
   *
   * @return the scheduler
   */
  public static Scheduler io() {
    return IO;
  }

  /**
   * Timer scheduler.
   *
   * @return the scheduler
   */
  public static Scheduler timer() {
    return TIMER_SCHEDULER;
  }

  /** Shutdown. */
  public static void shutdown() {}
}
