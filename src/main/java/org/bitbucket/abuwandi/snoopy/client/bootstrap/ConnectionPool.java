package org.bitbucket.abuwandi.snoopy.client.bootstrap;

import io.reactivex.rxjava3.core.Single;

/** The interface Connection pool. */
public interface ConnectionPool {

  /**
   * Acquire single.
   *
   * @return the single
   */
  Single<Connection> acquire();

  /**
   * Release.
   *
   * @param connection the connection
   */
  void release(Connection connection);

  /** Shutdown. */
  void shutdown();

  void cleanUp();
}
