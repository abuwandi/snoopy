package org.bitbucket.abuwandi.snoopy.client.http.authentication;

import io.netty.handler.codec.http.HttpHeaderNames;
import io.netty.handler.codec.http.HttpObject;
import io.reactivex.rxjava3.core.Single;
import java.util.Objects;
import lombok.Builder;
import org.bitbucket.abuwandi.snoopy.client.authentication.Authentication;
import org.bitbucket.abuwandi.snoopy.client.authentication.Authenticator;
import org.bitbucket.abuwandi.snoopy.client.authentication.TokenProvider;
import org.bitbucket.abuwandi.snoopy.client.http.SnoopyHttpRequest;
import org.bitbucket.abuwandi.snoopy.model.Token;

/** The type Bearer token authenticator. */
public class BearerTokenAuthenticator implements Authenticator<HttpObject, SnoopyHttpRequest> {

  private final TokenProvider<? super Authentication, ? super Token> tokenProvider;
  private final Authentication authentication;

  /**
   * Instantiates a new Bearer token authenticator.
   *
   * @param tokenProvider the token provider
   * @param authentication the authentication
   */
  @Builder
  public BearerTokenAuthenticator(
      final TokenProvider<? super Authentication, ? super Token> tokenProvider,
      final Authentication authentication) {
    this.tokenProvider = tokenProvider;
    this.authentication = authentication;
  }

  @Override
  public Single<SnoopyHttpRequest> authenticate(SnoopyHttpRequest request) {
    if (Objects.isNull(authentication)) {
      return Single.just(request);
    }
    return tokenProvider
        .token(authentication)
        .map(
            token -> {
              request
                  .headers()
                  .set(HttpHeaderNames.AUTHORIZATION, String.format("Bearer %s", token.content()));
              return request;
            });
  }
}
