package org.bitbucket.abuwandi.snoopy.client.bootstrap;

import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import java.net.InetSocketAddress;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;
import lombok.Builder;
import lombok.NonNull;
import org.bitbucket.abuwandi.snoopy.client.ErrorCallback;
import org.bitbucket.abuwandi.snoopy.client.Registrar;
import org.bitbucket.abuwandi.snoopy.client.RequestRegistrar;
import org.bitbucket.abuwandi.snoopy.client.ResponseRegistrar;
import org.bitbucket.abuwandi.snoopy.client.SnoopyRequest;
import org.bitbucket.abuwandi.snoopy.client.SnoopyResponse;
import org.bitbucket.abuwandi.snoopy.client.SuccessCallback;
import org.bitbucket.abuwandi.snoopy.client.handlers.SnoopyInboundMessageTimeoutHandler;
import org.bitbucket.abuwandi.snoopy.util.IOUtils;

/** The type Connection. */
public class Connection {

  private final Channel channel;
  private final long connectedAt;
  private final AtomicLong idleStartTime;
  private final Consumer<Connection> onRelease;
  private final long connectionTime;
  private final AtomicBoolean cached;

  /**
   * Instantiates a new Connection.
   *
   * @param channel the channel
   * @param onRelease the on release
   * @param connectionTime the connection time
   */
  @Builder
  protected Connection(
      @NonNull final Channel channel,
      final Consumer<Connection> onRelease,
      final long connectionTime) {
    this.onRelease = onRelease;
    this.channel = channel;
    this.connectedAt = System.nanoTime();
    this.idleStartTime = new AtomicLong(0);
    this.connectionTime = connectionTime;
    this.cached = new AtomicBoolean(false);
  }

  /**
   * Register boolean.
   *
   * @param <T> the type parameter
   * @param response the response
   * @return the boolean
   */
  @SuppressWarnings("unchecked")
  public <T> boolean register(SnoopyResponse<T> response) {
    final AtomicBoolean result = new AtomicBoolean(true);
    channel
        .pipeline()
        .forEach(
            entry -> {
              if (entry.getValue() instanceof ResponseRegistrar) {
                final boolean res = ((ResponseRegistrar<T>) entry.getValue()).register(response);
                result.set(result.get() && res);
                result.get();
              }
            });
    return result.get();
  }

  /**
   * Register boolean.
   *
   * @param <T> the type parameter
   * @param request the request
   * @return the boolean
   */
  @SuppressWarnings("unchecked")
  public <T> boolean register(SnoopyRequest<T> request) {
    final AtomicBoolean result = new AtomicBoolean(true);
    channel
        .pipeline()
        .forEach(
            entry -> {
              if (entry.getValue() instanceof RequestRegistrar) {
                final boolean res =
                    ((RequestRegistrar<T, SnoopyRequest<T>>) entry.getValue()).register(request);
                result.set(result.get() && res);
              }
            });
    return result.get();
  }

  /**
   * Write and flush.
   *
   * @param msg the msg
   * @param successCallback the success callback
   * @param errorCallback the error callback
   */
  public void writeAndFlush(
      @NonNull final Object msg,
      @NonNull final SuccessCallback successCallback,
      @NonNull final ErrorCallback errorCallback) {
    final ChannelFuture channelFuture = channel().writeAndFlush(msg);
    IOUtils.handleChannelFuture(channelFuture, successCallback, errorCallback);
  }

  /**
   * Gets host.
   *
   * @return the host
   */
  public String getHost() {
    final InetSocketAddress inetAddr = (InetSocketAddress) channel.remoteAddress();
    return inetAddr.getHostString();
  }

  /**
   * Gets port.
   *
   * @return the port
   */
  public int getPort() {
    final InetSocketAddress inetAddr = (InetSocketAddress) channel.remoteAddress();
    return inetAddr.getPort();
  }

  /**
   * Is cached boolean.
   *
   * @return the boolean
   */
  public boolean isCached() {
    return this.cached.get();
  }

  /** Cached. */
  public void cached() {
    this.cached.set(true);
  }

  /**
   * Time long.
   *
   * @return the long
   */
  public long time() {
    return this.cached.get() ? 0L : this.connectionTime;
  }

  /**
   * Gets idle time.
   *
   * @return the idle time
   */
  public long getIdleTime() {
    return System.nanoTime() - this.idleStartTime.get();
  }

  /** Release. */
  public void release() {
    this.idleStartTime.set(System.nanoTime());
    channel
        .pipeline()
        .forEach(
            entry -> {
              if (entry.getValue() instanceof Registrar) {
                ((Registrar<?>) entry.getValue()).deregisterAll();
              }
            });
    onRelease.accept(this);
  }

  public void startResponseTimeout() {
    final SnoopyInboundMessageTimeoutHandler<?> timeoutHandler =
        channel.pipeline().get(SnoopyInboundMessageTimeoutHandler.class);
    if (Objects.nonNull(timeoutHandler)) {
      timeoutHandler.watch();
    }
  }

  /** Close. */
  public void close() {
    try {
      final ChannelFuture channelFuture = channel.close();
      if (channelFuture.isDone()) {
        release();
        return;
      }
      channelFuture.addListener(future -> release());
    } catch (Exception exc) {
      release();
    } catch (Throwable throwable) {
      release();
      throw throwable;
    }
  }

  /**
   * Is valid boolean.
   *
   * @return the boolean
   */
  public boolean isValid() {
    return Objects.nonNull(channel)
        && (channel.isActive() && channel.isOpen() && channel.isWritable());
  }

  /**
   * Channel channel.
   *
   * @return the channel
   */
  public Channel channel() {
    return channel;
  }
}
