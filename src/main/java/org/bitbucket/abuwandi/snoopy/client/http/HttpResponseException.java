package org.bitbucket.abuwandi.snoopy.client.http;

import org.bitbucket.abuwandi.snoopy.client.ClientException;

/** The type Http response exception. */
public class HttpResponseException extends ClientException {

  /**
   * Instantiates a new Http response exception.
   *
   * @param message the message
   */
  public HttpResponseException(String message) {
    super(message);
  }

  /**
   * Instantiates a new Http response exception.
   *
   * @param cause the cause
   */
  public HttpResponseException(Throwable cause) {
    super(cause);
  }

  /**
   * Instantiates a new Http response exception.
   *
   * @param message the message
   * @param cause the cause
   */
  public HttpResponseException(String message, Throwable cause) {
    super(message, cause);
  }
}
