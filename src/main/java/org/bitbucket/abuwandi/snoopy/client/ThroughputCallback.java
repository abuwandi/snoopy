package org.bitbucket.abuwandi.snoopy.client;

/** The interface Throughput callback. */
public interface ThroughputCallback {

  /** The constant DO_NOTHING_THROUGHPUT_CALLBACK. */
  ThroughputCallback DO_NOTHING_THROUGHPUT_CALLBACK = newThroughput -> {};

  /**
   * On throughput change.
   *
   * @param newThroughput the new throughput
   */
  void onThroughputChange(long newThroughput);
}
