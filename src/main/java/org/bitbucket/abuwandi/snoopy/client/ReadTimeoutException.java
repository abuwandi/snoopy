package org.bitbucket.abuwandi.snoopy.client;

import org.bitbucket.abuwandi.snoopy.exceptions.SnoopyException;

/** The type Read timeout exception. */
public class ReadTimeoutException extends SnoopyException {

  /**
   * Instantiates a new Read timeout exception.
   *
   * @param message the message
   */
  public ReadTimeoutException(String message) {
    super(message);
  }

  /**
   * Instantiates a new Read timeout exception.
   *
   * @param cause the cause
   */
  public ReadTimeoutException(Throwable cause) {
    super(cause);
  }

  /**
   * Instantiates a new Read timeout exception.
   *
   * @param message the message
   * @param cause the cause
   */
  public ReadTimeoutException(String message, Throwable cause) {
    super(message, cause);
  }
}
