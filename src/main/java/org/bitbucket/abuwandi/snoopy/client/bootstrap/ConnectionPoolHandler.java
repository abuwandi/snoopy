package org.bitbucket.abuwandi.snoopy.client.bootstrap;

import io.netty.channel.Channel;

/** The interface Connection pool handler. */
interface ConnectionPoolHandler {

  /**
   * Channel created.
   *
   * @param channel the channel
   */
  void channelCreated(Channel channel);

  /**
   * Acquired.
   *
   * @param connection the connection
   */
  void acquired(Connection connection);

  /**
   * Released.
   *
   * @param connection the connection
   */
  void released(Connection connection);
}
