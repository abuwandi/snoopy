package org.bitbucket.abuwandi.snoopy.client.http;

import io.netty.buffer.ByteBuf;
import io.netty.handler.codec.http.HttpHeaderValues;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;
import lombok.Builder;
import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;

/** The type File request body. */
public class FileRequestBody extends InputStreamRequestBody {

  @Override
  protected InputStream inputStream() throws IOException {
    return Files.newInputStream(path);
  }

  @Override
  public long contentLength() {
    return contentLength;
  }

  @Override
  public CharSequence contentType() {
    return HttpHeaderValues.APPLICATION_OCTET_STREAM;
  }

  @Override
  public CharSequence name() {
    return name;
  }

  @Override
  public CharSequence contentDisposition() {
    return String.format("form-data; name=\"%s\"; filename=\"%s\"", name, filename);
  }

  @Override
  protected int getBufferInitialCapacity() {
    return chunkSize;
  }

  @Override
  protected void preWrite(ByteBuf buffer) {
    // not interested
  }

  @Override
  protected void postWrite(ByteBuf buffer) {
    // not interested
  }

  private final Path path;
  private final long contentLength;
  private final String filename;
  private final String name;

  /**
   * Instantiates a new File request body.
   *
   * @param path the path
   * @param chunkSize the chunk size
   * @param name the name
   * @param filename the filename
   */
  @Builder
  public FileRequestBody(
      @NonNull final Path path, final int chunkSize, final String name, final String filename) {
    super(chunkSize);
    try {
      this.contentLength = Files.size(path);
    } catch (IOException exc) {
      throw new HttpRequestException(exc);
    }
    this.path = path;
    this.name = name;
    this.filename =
        Optional.ofNullable(filename)
            .filter(StringUtils::isNotBlank)
            .orElseGet(() -> path.getFileName().toString());
  }
}
