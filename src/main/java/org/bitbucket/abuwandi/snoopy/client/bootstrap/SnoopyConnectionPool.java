package org.bitbucket.abuwandi.snoopy.client.bootstrap;

import io.netty.channel.Channel;
import io.netty.channel.ChannelDuplexHandler;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.disposables.Disposable;
import java.net.URI;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import lombok.Builder;
import lombok.NonNull;
import org.bitbucket.abuwandi.snoopy.client.ChannelInitializer;
import org.bitbucket.abuwandi.snoopy.client.SnoopySchedulers;

/** The type Snoopy connection pool. */
public class SnoopyConnectionPool extends ChannelDuplexHandler
    implements ConnectionProvider, ConnectionPoolStatsListener {

  private final ConcurrentHashMap<String, ConnectionPoolHolder> channelPool;
  private final int maximumConnections;
  private final BootstrapProvider bootstrapProvider;
  private final ChannelInitializer channelInitializer;
  private final int maxIdleConnections;
  private final long maxIdleConnectionDuration;
  private final Lock lock;
  private final ConnectionPoolMode mode;
  private final ConnectionPoolProvider connectionPoolProvider;
  private final ConnectionPoolStatsListener statsListener;
  private final AtomicInteger activeConnectionsTotal;
  private final AtomicInteger idleConnectionsTotal;
  private final AtomicInteger connectionsTotal;
  private final Disposable cleanUpDisposable;

  /**
   * Instantiates a new Snoopy connection pool.
   *
   * @param bootstrapProvider the bootstrap provider
   * @param channelInitializer the channel initializer
   * @param maximumConnections the maximum connections
   * @param mode the mode
   * @param connectionPoolProvider the connection pool provider
   * @param statsListener the stats listener
   */
  @Builder
  public SnoopyConnectionPool(
      @NonNull final BootstrapProvider bootstrapProvider,
      @NonNull final ChannelInitializer channelInitializer,
      final int maximumConnections,
      final int maxIdleConnections,
      final long maxIdleConnectionDuration,
      final ConnectionPoolMode mode,
      final ConnectionPoolProvider connectionPoolProvider,
      final ConnectionPoolStatsListener statsListener) {
    if (mode == ConnectionPoolMode.CUSTOM && Objects.isNull(connectionPoolProvider)) {
      throw new IllegalArgumentException("Invalid connection pool supplier for custom mode");
    }
    this.maximumConnections = maximumConnections;
    this.channelPool = new ConcurrentHashMap<>();
    this.bootstrapProvider = bootstrapProvider;
    this.channelInitializer = channelInitializer;
    this.maxIdleConnections = maxIdleConnections;
    this.maxIdleConnectionDuration = maxIdleConnectionDuration;
    this.lock = new ReentrantLock();
    this.mode = Optional.ofNullable(mode).orElse(ConnectionPoolMode.FIXED);
    this.connectionPoolProvider = connectionPoolProvider;
    this.statsListener =
        Optional.ofNullable(statsListener)
            .orElse(ConnectionPoolStatsListener.DO_NOTHING_STATS_LISTENER);
    this.idleConnectionsTotal = new AtomicInteger(0);
    this.activeConnectionsTotal = new AtomicInteger(0);
    this.connectionsTotal = new AtomicInteger(0);
    this.cleanUpDisposable =
        Observable.intervalRange(
                1, Long.MAX_VALUE, 0, 5, TimeUnit.SECONDS, SnoopySchedulers.timer())
            .subscribe(
                counter ->
                    channelPool
                        .values()
                        .forEach(
                            connectionPoolHolder -> connectionPoolHolder.connectionPool.cleanUp()));
  }

  @Override
  public Single<Connection> connect(final URI uri) {
    final String key = String.format("%s:%d", uri.getHost(), uri.getPort());
    ConnectionPoolHolder connectionPoolHolder = this.channelPool.get(key);
    if (Objects.isNull(connectionPoolHolder)) {
      lock.lock();
      try {
        connectionPoolHolder =
            this.channelPool.computeIfAbsent(key, keyString -> createConnectionPool(uri));
      } finally {
        lock.unlock();
      }
    }
    return connectionPoolHolder.connectionPool.acquire();
  }

  @Override
  public void release(Connection connection) {
    if (Objects.isNull(connection)) {
      return;
    }
    final String key = String.format("%s:%d", connection.getHost(), connection.getPort());
    channelPool.computeIfPresent(
        key,
        (keyString, channelPoolHolder) -> {
          channelPoolHolder.connectionPool.release(connection);
          return channelPoolHolder;
        });
  }

  @Override
  public void shutdown() {
    if (!cleanUpDisposable.isDisposed()) {
      cleanUpDisposable.dispose();
    }
  }

  @Override
  public void stats(final ConnectionPoolStats stats) {
    final ConnectionPoolHolder connectionPoolHolder = channelPool.get(stats.getName());
    if (Objects.nonNull(connectionPoolHolder)) {
      connectionPoolHolder.stats.set(stats);
      statsListener.stats(stats);
    }
  }

  private ConnectionPoolHolder createConnectionPool(@NonNull final URI uri) {
    return bootstrapProvider
        .bootstrap(uri)
        .map(
            bootstrap -> {
              switch (mode) {
                case FIXED:
                  return ConnectionPoolHolder.builder()
                      .connectionPool(
                          FixedConnectionPool.builder()
                              .bootstrap(bootstrap)
                              .maxConnections(maximumConnections)
                              .maxIdleConnections(maxIdleConnections)
                              .maxIdleConnectionDuration(maxIdleConnectionDuration)
                              .name(String.format("%s:%s", uri.getHost(), uri.getPort()))
                              .statsListener(this)
                              .handler(
                                  new ConnectionPoolHandler() {
                                    @Override
                                    public void released(Connection connection) {
                                      channelInitializer.uninstallHandlers(connection.channel());
                                    }

                                    @Override
                                    public void acquired(Connection connection) {
                                      channelInitializer.installHandlers(connection.channel());
                                    }

                                    @Override
                                    public void channelCreated(Channel channel) {
                                      channelInitializer.initialize(uri, channel);
                                    }
                                  })
                              .build())
                      .build();
                case CACHED:
                  return ConnectionPoolHolder.builder()
                      .connectionPool(
                          CachedConnectionPool.builder()
                              .bootstrap(bootstrap)
                              .maxIdleConnections(maxIdleConnections)
                              .maxIdleConnectionDuration(maxIdleConnectionDuration)
                              .name(String.format("%s:%s", uri.getHost(), uri.getPort()))
                              .statsListener(this)
                              .handler(
                                  new ConnectionPoolHandler() {
                                    @Override
                                    public void released(Connection connection) {
                                      channelInitializer.uninstallHandlers(connection.channel());
                                    }

                                    @Override
                                    public void acquired(Connection connection) {
                                      channelInitializer.installHandlers(connection.channel());
                                    }

                                    @Override
                                    public void channelCreated(Channel channel) {
                                      channelInitializer.initialize(uri, channel);
                                    }
                                  })
                              .build())
                      .build();
                case SINGLE:
                  return ConnectionPoolHolder.builder()
                      .connectionPool(
                          FixedConnectionPool.builder()
                              .bootstrap(bootstrap)
                              .maxConnections(1)
                              .maxIdleConnections(maxIdleConnections)
                              .maxIdleConnectionDuration(maxIdleConnectionDuration)
                              .name(String.format("%s:%s", uri.getHost(), uri.getPort()))
                              .statsListener(this)
                              .handler(
                                  new ConnectionPoolHandler() {
                                    @Override
                                    public void released(Connection connection) {
                                      channelInitializer.uninstallHandlers(connection.channel());
                                    }

                                    @Override
                                    public void acquired(Connection connection) {
                                      channelInitializer.installHandlers(connection.channel());
                                    }

                                    @Override
                                    public void channelCreated(Channel channel) {
                                      channelInitializer.initialize(uri, channel);
                                    }
                                  })
                              .build())
                      .build();
                case CUSTOM:
                  final ConnectionPool connectionPool = connectionPoolProvider.connectionPool(this);
                  if (Objects.isNull(connectionPool)) {
                    throw new ConnectionException(
                        "Connection pool provider returned null connection pool");
                  }
                  return ConnectionPoolHolder.builder().connectionPool(connectionPool).build();
                default:
                  throw new ConnectionException("Invalid connection pool mode");
              }
            })
        .blockingGet();
  }

  private static class ConnectionPoolHolder {

    private final AtomicReference<ConnectionPoolStats> stats;
    private final ConnectionPool connectionPool;

    /**
     * Instantiates a new Connection pool holder.
     *
     * @param connectionPool the connection pool
     * @param stats the stats
     */
    @Builder
    ConnectionPoolHolder(final ConnectionPool connectionPool, final ConnectionPoolStats stats) {
      this.connectionPool = connectionPool;
      this.stats = new AtomicReference<>(stats);
    }
  }
}
