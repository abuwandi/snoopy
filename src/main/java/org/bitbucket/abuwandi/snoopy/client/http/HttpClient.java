package org.bitbucket.abuwandi.snoopy.client.http;

import java.net.URI;

/** The interface Http client. */
public interface HttpClient {

  /**
   * Post http post request builder.
   *
   * @param uri the uri
   * @return the http post request builder
   */
  HttpPostRequestBuilder post(URI uri);

  /**
   * Get http get request builder.
   *
   * @param uri the uri
   * @return the http get request builder
   */
  HttpGetRequestBuilder get(URI uri);

  /**
   * Head http request builder.
   *
   * @param uri the uri
   * @return the http request builder
   */
  HttpRequestBuilder head(URI uri);

  /**
   * Put http request body builder.
   *
   * @param uri the uri
   * @return the http request body builder
   */
  HttpRequestBodyBuilder put(URI uri);

  /**
   * Patch http request body builder.
   *
   * @param uri the uri
   * @return the http request body builder
   */
  HttpRequestBodyBuilder patch(URI uri);

  /**
   * Delete http request body builder.
   *
   * @param uri the uri
   * @return the http request body builder
   */
  HttpRequestBodyBuilder delete(URI uri);

  /**
   * Options http request builder.
   *
   * @param uri the uri
   * @return the http request builder
   */
  HttpRequestBuilder options(URI uri);
}
