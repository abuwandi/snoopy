package org.bitbucket.abuwandi.snoopy.client;

/** The interface Completable. */
public interface Completable {
  /**
   * Complete boolean.
   *
   * @return the boolean
   */
  boolean complete();

  /**
   * Is completable boolean.
   *
   * @return the boolean
   */
  boolean isCompleted();
}
