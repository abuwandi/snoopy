package org.bitbucket.abuwandi.snoopy.client;

import org.bitbucket.abuwandi.snoopy.client.bootstrap.BootstrapProvider;
import org.bitbucket.abuwandi.snoopy.client.bootstrap.ConnectionProvider;

/** The interface Application protocol. */
public interface ApplicationProtocol
    extends ConnectionProvider, ChannelInitializer, BootstrapProvider {}
