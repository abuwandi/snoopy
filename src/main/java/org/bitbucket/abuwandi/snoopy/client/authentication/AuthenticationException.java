package org.bitbucket.abuwandi.snoopy.client.authentication;

import org.bitbucket.abuwandi.snoopy.client.ClientException;

/** The type Authentication exception. */
public class AuthenticationException extends ClientException {

  /**
   * Instantiates a new Authentication exception.
   *
   * @param message the message
   */
  public AuthenticationException(String message) {
    super(message);
  }

  /**
   * Instantiates a new Authentication exception.
   *
   * @param cause the cause
   */
  public AuthenticationException(Throwable cause) {
    super(cause);
  }

  /**
   * Instantiates a new Authentication exception.
   *
   * @param message the message
   * @param cause the cause
   */
  public AuthenticationException(String message, Throwable cause) {
    super(message, cause);
  }
}
