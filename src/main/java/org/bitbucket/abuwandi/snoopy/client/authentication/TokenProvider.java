package org.bitbucket.abuwandi.snoopy.client.authentication;

import io.reactivex.rxjava3.core.Single;
import org.bitbucket.abuwandi.snoopy.model.Token;

/**
 * The interface Token provider.
 *
 * @param <T> the type parameter
 * @param <U> the type parameter
 */
public interface TokenProvider<T extends Authentication, U extends Token> {

  /**
   * Token single.
   *
   * @param authentication the authentication
   * @return the single
   */
  Single<U> token(T authentication);
}
