package org.bitbucket.abuwandi.snoopy.client.http;

import io.netty.buffer.ByteBuf;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import lombok.Builder;
import lombok.NonNull;
import org.bitbucket.abuwandi.snoopy.util.Serializable;

/**
 * The type Serializable request body.
 *
 * @param <T> the type parameter
 */
public class SerializableRequestBody<T extends Serializable> extends InputStreamRequestBody {

  private final T serializable;
  private final String name;
  private final CharSequence contentType;

  /**
   * Instantiates a new Serializable request body.
   *
   * @param serializable the serializable
   * @param chunkSize the chunk size
   * @param name the name
   * @param contentType the content type
   */
  @Builder
  public SerializableRequestBody(
      @NonNull final T serializable,
      final int chunkSize,
      final String name,
      final CharSequence contentType) {
    super(chunkSize);
    this.serializable = serializable;
    this.name = name;
    this.contentType = contentType;
  }

  @Override
  protected int getBufferInitialCapacity() {
    return chunkSize;
  }

  @Override
  protected void preWrite(ByteBuf buffer) {}

  @Override
  protected void postWrite(ByteBuf buffer) {}

  @Override
  protected InputStream inputStream() {
    return new ByteArrayInputStream(serializable.serialize());
  }

  @Override
  public CharSequence contentType() {
    return contentType;
  }

  @Override
  public long contentLength() {
    return serializable.serialize().length;
  }

  @Override
  public CharSequence contentDisposition() {
    return String.format("form-data; name=\"%s\"", name);
  }

  @Override
  public CharSequence name() {
    return name;
  }
}
