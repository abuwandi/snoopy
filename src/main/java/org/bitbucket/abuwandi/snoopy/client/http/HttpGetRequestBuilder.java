package org.bitbucket.abuwandi.snoopy.client.http;

import io.netty.handler.codec.http.HttpHeaderNames;
import io.netty.handler.codec.http.HttpVersion;
import java.net.URI;
import java.util.List;
import java.util.Map;
import org.bitbucket.abuwandi.snoopy.client.ApplicationProtocol;
import org.bitbucket.abuwandi.snoopy.client.HandshakeCallback;
import org.bitbucket.abuwandi.snoopy.client.ProgressCallback;
import org.bitbucket.abuwandi.snoopy.client.StatusCallback;
import org.bitbucket.abuwandi.snoopy.client.ThroughputCallback;
import org.bitbucket.abuwandi.snoopy.client.authentication.Authentication;
import org.bitbucket.abuwandi.snoopy.client.authentication.TokenProvider;
import org.bitbucket.abuwandi.snoopy.model.Token;

/** The type Http get request builder. */
public class HttpGetRequestBuilder extends HttpRequestBuilder {

  /** Instantiates a new Http get request builder. */
  HttpGetRequestBuilder() {
    super(HttpMethod.GET);
  }

  @Override
  public HttpGetRequestBuilder tokenProvider(TokenProvider<Authentication, Token> tokenProvider) {
    super.tokenProvider(tokenProvider);
    return this;
  }

  @Override
  public HttpGetRequestBuilder reference(String reference) {
    super.reference(reference);
    return this;
  }

  @Override
  public HttpGetRequestBuilder version(HttpVersion version) {
    super.version(version);
    return this;
  }

  @Override
  public HttpGetRequestBuilder host(CharSequence host) {
    super.host(host);
    return this;
  }

  @Override
  public HttpGetRequestBuilder closeConnection() {
    super.closeConnection();
    return this;
  }

  @Override
  public HttpGetRequestBuilder keepConnectionAlive() {
    super.keepConnectionAlive();
    return this;
  }

  @Override
  public HttpGetRequestBuilder uri(URI uri) {
    super.uri(uri);
    return this;
  }

  @Override
  public HttpGetRequestBuilder header(CharSequence name, CharSequence... values) {
    super.header(name, values);
    return this;
  }

  @Override
  public HttpGetRequestBuilder headers(Map<CharSequence, List<CharSequence>> headerMap) {
    super.headers(headerMap);
    return this;
  }

  public HttpGetRequestBuilder ignoreResponseBody(final boolean ignoreResponseBody) {
    this.ignoreResponseBody = ignoreResponseBody;
    return this;
  }

  public HttpGetRequestBuilder failIfNotSuccessfulResponse(
      final boolean failIfNotSuccessfulResponse) {
    this.failIfNotSuccessfulResponse = failIfNotSuccessfulResponse;
    return this;
  }

  public HttpGetRequestBuilder responseStatusCallback(final StatusCallback responseStatusCallback) {
    this.responseStatusCallback = responseStatusCallback;
    return this;
  }

  public HttpGetRequestBuilder responseThroughputCallback(
      final ThroughputCallback responseThroughputCallback) {
    this.responseThroughputCallback = responseThroughputCallback;
    return this;
  }

  public HttpGetRequestBuilder responseProgressCallback(
      final ProgressCallback responseProgressCallback) {
    this.responseProgressCallback = responseProgressCallback;
    return this;
  }

  public HttpGetRequestBuilder statusCallback(final StatusCallback statusCallback) {
    this.statusCallback = statusCallback;
    return this;
  }

  public HttpGetRequestBuilder handshakeCallback(final HandshakeCallback handshakeCallback) {
    this.handshakeCallback = handshakeCallback;
    return this;
  }

  /**
   * Range http get request builder.
   *
   * @param from the from
   * @param to the to
   * @return the http get request builder
   */
  public HttpGetRequestBuilder range(long from, long to) {
    if (from < 0 || to < 0 || from >= to) {
      return this;
    }
    this.headers.set(HttpHeaderNames.RANGE, String.format("bytes=%d-%d", from, to));
    return this;
  }

  @Override
  public HttpGetRequestBuilder followRedirects(boolean followRedirects) {
    super.followRedirects(followRedirects);
    return this;
  }

  @Override
  protected HttpGetRequestBuilder redirectsDepth(int redirectsDepth) {
    super.redirectsDepth(redirectsDepth);
    return this;
  }

  public HttpGetRequestBuilder connectionRetry(final int connectionRetry) {
    this.connectionRetry = connectionRetry;
    return this;
  }

  protected HttpGetRequestBuilder applicationProtocol(
      final ApplicationProtocol applicationProtocol) {
    this.applicationProtocol = applicationProtocol;
    return this;
  }
}
