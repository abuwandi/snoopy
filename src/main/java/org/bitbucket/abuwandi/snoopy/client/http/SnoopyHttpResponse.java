package org.bitbucket.abuwandi.snoopy.client.http;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufHolder;
import io.netty.handler.codec.http.HttpContent;
import io.netty.handler.codec.http.HttpHeaderNames;
import io.netty.handler.codec.http.HttpHeaderValues;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpMessage;
import io.netty.handler.codec.http.HttpObject;
import io.netty.handler.codec.http.HttpResponse;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpUtil;
import io.netty.handler.codec.http.LastHttpContent;
import io.netty.util.ReferenceCountUtil;
import io.reactivex.rxjava3.core.BackpressureStrategy;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.flowables.ConnectableFlowable;
import io.reactivex.rxjava3.subjects.PublishSubject;
import java.nio.charset.Charset;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.Builder;
import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;
import org.bitbucket.abuwandi.snoopy.client.ProgressCallback;
import org.bitbucket.abuwandi.snoopy.client.ReadTimeoutException;
import org.bitbucket.abuwandi.snoopy.client.SnoopyResponse;
import org.bitbucket.abuwandi.snoopy.client.Status;
import org.bitbucket.abuwandi.snoopy.client.StatusCallback;
import org.bitbucket.abuwandi.snoopy.client.ThroughputCallback;
import org.bitbucket.abuwandi.snoopy.client.bootstrap.Connection;
import org.bitbucket.abuwandi.snoopy.util.ByteBufInputStream;
import org.bitbucket.abuwandi.snoopy.util.IOUtils;
import org.bitbucket.abuwandi.snoopy.util.StopWatch;

/** The type Snoopy http response. */
public class SnoopyHttpResponse implements SnoopyResponse<HttpObject> {

  public boolean onNext(final HttpObject httpObject) {
    if (isReleased()) {
      responseTime.set(stopWatch.time());
      throw new HttpResponseException("Response is released");
    }
    setStatus(Status.ACTIVE);
    if (!httpObject.decoderResult().isSuccess()) {
      responseTime.set(stopWatch.time());
      throw Optional.ofNullable(httpObject.decoderResult().cause())
          .map(cause -> new HttpResponseException("Response decode error", cause))
          .orElseGet(() -> new HttpResponseException("Response decode error"));
    }

    if (httpObject instanceof HttpResponse && !responseComplete((HttpResponse) httpObject)) {
      responseTime.set(stopWatch.time());
      throw new HttpResponseException("Response publisher is terminated");
    }

    if (!httpResponseFuture.isCompletedExceptionally() && !httpResponseFuture.isDone()) {
      responseTime.set(stopWatch.time());
      throw new HttpResponseException("Empty response");
    }

    if (ignoreBody || !request.method().hasResponseBody()) {
      responseTime.set(stopWatch.time());
      ReferenceCountUtil.safeRelease(httpObject);
      setStatus(Status.COMPLETE);
      release("All content received");
      return false;
    }

    if (httpObject instanceof HttpContent) {
      final HttpContent httpContent = (HttpContent) httpObject;
      if (!contentSubmit(httpContent)) {
        responseTime.set(stopWatch.time());
        throw new HttpResponseException("Response content publisher is terminated");
      }
      progressCallback.onProgress(httpContent.content().readableBytes());
      if (httpObject instanceof LastHttpContent) {
        responseTime.set(stopWatch.time());
        if (contentComplete()) {
          setStatus(Status.COMPLETE);
          return false;
        }
        throw new HttpResponseException("Response content publisher is terminated");
      }
    }
    return true;
  }

  public boolean onError(Throwable throwable) {
    if (!isReleased()) {
      final boolean released = this.release(String.format("Error: %s", throwable.getMessage()));
      responseTime.updateAndGet(current -> System.nanoTime() - current);
      responseError(throwable);
      contentError(throwable);
      setStatus(Status.FAILED);
      return released;
    }
    return false;
  }

  public boolean release(@NonNull final String reason) {
    if (this.released.compareAndSet(false, true)) {
      this.releaseReason.set(reason);
      if (this.closeConnection) {
        request.connection().close();
      } else {
        request.connection().release();
      }
      return true;
    }
    return false;
  }

  public boolean isReleased() {
    return released.get();
  }

  @Override
  public Connection connection() {
    return request().connection();
  }

  public SnoopyHttpRequest request() {
    return request;
  }

  public void setStatus(final Status status) {
    final Status before = this.status.get();
    final Status after =
        this.status.updateAndGet(
            current -> {
              if (current == Status.COMPLETE) {
                return current;
              }
              if (current == Status.FAILED && status != Status.RETRY) {
                return current;
              }
              return status;
            });
    if (before != after) {
      statusChangeCallback.onStatusChange(status);
    }
  }

  @Override
  public void setThroughput(final long throughput) {
    final long before = this.throughput.get();
    final long after = this.throughput.updateAndGet(current -> throughput);
    if (before != after) {
      throughputChangeCallback.onThroughputChange(throughput);
    }
  }

  public String reference() {
    return request.reference();
  }

  private boolean responseComplete(final HttpResponse response) {
    return !httpResponseFuture.isCompletedExceptionally()
        && !httpResponseFuture.isDone()
        && httpResponseFuture.complete(response);
  }

  private boolean responseError(final Throwable throwable) {
    return !httpResponseFuture.isCompletedExceptionally()
        && !httpResponseFuture.isDone()
        && httpResponseFuture.completeExceptionally(throwable);
  }

  private boolean contentSubmit(final HttpContent content) {
    if (Objects.nonNull(publisher) && !publisher.hasComplete() && !publisher.hasThrowable()) {
      publisher.onNext(content);
      return true;
    }
    return false;
  }

  private boolean contentError(final Throwable throwable) {
    if (Objects.nonNull(publisher) && !publisher.hasComplete() && !publisher.hasThrowable()) {
      publisher.onError(throwable);
      return true;
    }
    return false;
  }

  private boolean contentComplete() {
    if (Objects.nonNull(publisher) && !publisher.hasComplete() && !publisher.hasThrowable()) {
      publisher.onComplete();
      return true;
    }
    return false;
  }

  /**
   * Status http response status.
   *
   * @return the http response status
   */
  public HttpResponseStatus status() {
    return Optional.ofNullable(httpResponse()).map(HttpResponse::status).orElse(null);
  }

  /**
   * Content length long.
   *
   * @return the long
   */
  public long contentLength() {
    return Optional.ofNullable(httpResponse()).map(HttpUtil::getContentLength).orElse(0L);
  }

  /**
   * Content type char sequence.
   *
   * @return the char sequence
   */
  public CharSequence contentType() {
    return Optional.ofNullable(httpResponse()).map(HttpUtil::getMimeType).orElse(null);
  }

  /**
   * Location char sequence.
   *
   * @return the char sequence
   */
  public CharSequence location() {
    final String locationUri = headers().get(HttpHeaderNames.LOCATION);
    if (StringUtils.isBlank(locationUri)) {
      return null;
    }
    return locationUri;
  }

  /**
   * Charset charset.
   *
   * @return the charset
   */
  public Charset charset() {
    return Optional.ofNullable(httpResponse()).map(HttpUtil::getCharset).orElse(null);
  }

  /**
   * Filename char sequence.
   *
   * @return the char sequence
   */
  public CharSequence filename() {
    final String contentDisposition = headers().get(HttpHeaderNames.CONTENT_DISPOSITION);
    if (StringUtils.isBlank(contentDisposition)) {
      return null;
    }
    final Matcher matcher =
        Pattern.compile("filename=((['\"])[^'\"\\n]+\\2|[^'\";=\\n]+)").matcher(contentDisposition);
    if (!matcher.find()) {
      return null;
    }
    final String match = matcher.group();
    final String[] split = StringUtils.split(match, '=');
    return Paths.get(StringUtils.replaceChars(split[1], "'\"", "")).getFileName().toString();
  }

  /**
   * Support partial requests boolean.
   *
   * @return the boolean
   */
  public boolean supportPartialRequests() {
    final String acceptRangesText = headers().get(HttpHeaderNames.ACCEPT_RANGES);
    return StringUtils.equalsIgnoreCase(HttpHeaderValues.BYTES, acceptRangesText);
  }

  /**
   * Body as string single.
   *
   * @return the single
   */
  public Single<String> bodyAsString() {
    return bodyAs(String.class)
        .collect(StringBuilder::new, StringBuilder::append)
        .map(StringBuilder::toString);
  }

  /**
   * Dispose body long.
   *
   * @return the long
   */
  public long disposeBody() {
    return bodyAs(ByteBuf.class)
        .map(
            buffer -> {
              try {
                return buffer.readableBytes();
              } finally {
                ReferenceCountUtil.safeRelease(buffer);
              }
            })
        .collect(AtomicLong::new, AtomicLong::addAndGet)
        .map(AtomicLong::get)
        .blockingGet();
  }

  /**
   * Body as flowable.
   *
   * @param <T> the type parameter
   * @param type the type
   * @return the flowable
   */
  public <T> Flowable<T> bodyAs(Class<T> type) {
    {
      if (!hasBody()) {
        return Flowable.error(new HttpResponseException("No body"));
      }
      if (Objects.isNull(type)) {
        return Flowable.error(
            new HttpResponseException(new IllegalArgumentException("Invalid type")));
      }
      try {
        final CharSequence contentType = contentType();
        if (type == byte[].class) {
          return receivePipeline
              // .observeOn(SnoopySchedulers.io())
              .map(this::httpContentToBytes)
              .cast(type)
              .doFinally(() -> release("Received all http content"));
        } else if (type == ByteBuf.class) {
          return receivePipeline
              // .observeOn(SnoopySchedulers.io())
              .map(ByteBufHolder::content)
              .cast(type)
              .doFinally(() -> release("Received all http content"));
        } else if (type == String.class
            && (HttpHeaderValues.TEXT_PLAIN.contentEquals(contentType)
                || HttpHeaderValues.TEXT_CSS.contentEquals(contentType)
                || HttpHeaderValues.TEXT_HTML.contentEquals(contentType)
                || HttpHeaderValues.TEXT_EVENT_STREAM.contentEquals(contentType)
                || HttpHeaderValues.APPLICATION_JSON.contentEquals(contentType)
                || HttpHeaderValues.APPLICATION_XHTML.contentEquals(contentType)
                || HttpHeaderValues.APPLICATION_XML.contentEquals(contentType)
                || StringUtils.isBlank(contentType))) {
          return receivePipeline
              // .observeOn(SnoopySchedulers.io())
              .map(this::httpContentToString)
              .cast(type)
              .doFinally(() -> release("Received all http content"));
        } else if (HttpHeaderValues.APPLICATION_JSON.contentEquals(contentType)) {
          final LinkedList<ByteBuf> buffers = new LinkedList<>();
          final AtomicLong totalSize = new AtomicLong(0);
          return receivePipeline
              // .observeOn(SnoopySchedulers.io())
              .map(HttpContent::content)
              .collect(
                  () -> buffers,
                  (collector, buffer) -> {
                    totalSize.addAndGet(buffer.readableBytes());
                    collector.add(buffer);
                  })
              .onErrorResumeNext(
                  throwable -> {
                    if (!buffers.isEmpty()) {
                      buffers.forEach(ReferenceCountUtil::safeRelease);
                    }
                    return Single.error(throwable);
                  })
              .map(byteBufs -> new ByteBufInputStream(byteBufs, totalSize.get()))
              .flatMapPublisher(
                  inputStream ->
                      Flowable.just(inputStream)
                          .map(mapper::readTree)
                          .flatMap(
                              jsonNode -> {
                                if (jsonNode.isArray()) {
                                  return Flowable.fromIterable(jsonNode)
                                      .map(node -> mapper.convertValue(node, type));
                                }
                                return Flowable.just(jsonNode)
                                    .map(node -> mapper.convertValue(node, type));
                              })
                          .doFinally(inputStream::close))
              .doFinally(() -> release("Received all http content"));
        }
        return Flowable.error(
            new HttpResponseException(
                String.format(
                    "Cannot transform unknown content type to %s", type.getSimpleName())));
      } catch (Throwable throwable) {
        return Flowable.error(throwable);
      }
    }
  }

  private String httpContentToString(final HttpContent httpContent) {
    try {
      final ByteBuf buffer = httpContent.content();
      if (buffer.readableBytes() > IOUtils.ZERO) {
        return buffer.toString(Charset.forName(charset().toString()));
      }
      return IOUtils.EMPTY_STRING;
    } finally {
      ReferenceCountUtil.safeRelease(httpContent);
    }
  }

  private byte[] httpContentToBytes(final HttpContent httpContent) {
    try {
      final ByteBuf buffer = httpContent.content();
      if (buffer.readableBytes() <= IOUtils.ZERO) {
        return IOUtils.EMPTY_BUFFER;
      }
      final byte[] dst = new byte[buffer.readableBytes()];
      buffer.getBytes(IOUtils.ZERO, dst);
      return dst;
    } finally {
      ReferenceCountUtil.safeRelease(httpContent);
    }
  }

  /** Start stop watch. */
  void startStopWatch() {
    stopWatch.start();
  }

  /**
   * Connection time long.
   *
   * @return the long
   */
  public long connectionTime() {
    return request.connection().time();
  }

  /**
   * Time long.
   *
   * @return the long
   */
  public long time() {
    return this.responseTime.get();
  }

  /**
   * Headers http headers.
   *
   * @return the http headers
   */
  public HttpHeaders headers() {
    return Optional.ofNullable(httpResponse()).map(HttpMessage::headers).orElse(null);
  }

  /**
   * Is successful boolean.
   *
   * @return the boolean
   */
  public boolean isSuccessful() {
    return (status().code() / 100) - (HttpResponseStatus.OK.code() / 100) == 0;
  }

  /**
   * Is redirect boolean.
   *
   * @return the boolean
   */
  public boolean isRedirect() {
    return (status().code() / 100) - (HttpResponseStatus.MULTIPLE_CHOICES.code() / 100) == 0;
  }

  /**
   * Is client error boolean.
   *
   * @return the boolean
   */
  public boolean isClientError() {
    return (status().code() / 100) - (HttpResponseStatus.BAD_REQUEST.code() / 100) == 0;
  }

  /**
   * Is server error boolean.
   *
   * @return the boolean
   */
  public boolean isServerError() {
    return (status().code() / 100) - (HttpResponseStatus.INTERNAL_SERVER_ERROR.code() / 100) == 0;
  }

  /**
   * Is informational boolean.
   *
   * @return the boolean
   */
  public boolean isInformational() {
    return (status().code() / 100) - (HttpResponseStatus.CONTINUE.code() / 100) == 0;
  }

  /**
   * Is error boolean.
   *
   * @return the boolean
   */
  public boolean isError() {
    return isClientError() || isServerError();
  }

  /**
   * Has body boolean.
   *
   * @return the boolean
   */
  public boolean hasBody() {
    return request.method().hasResponseBody();
  }

  private HttpResponse httpResponse() {
    try {
      return httpResponseFuture.get();
    } catch (ExecutionException exc) {
      final HttpResponseException responseException =
          Optional.ofNullable(exc.getCause())
              .map(this::mapException)
              .orElseGet(() -> new HttpResponseException("Http response failed"));
      onError(responseException);
      // throw responseException;
    } catch (Exception exc) {
      final HttpResponseException responseException =
          new HttpResponseException(
              String.format(
                  "Http method: %s Url: %s Ignore Response Body: %s",
                  request.method(), request.uri(), ignoreBody),
              exc);
      onError(responseException);
      // throw responseException;
    } catch (Throwable throwable) {
      onError(throwable);
      // throw throwable;
    }
    return null;
  }

  private HttpResponseException mapException(Throwable throwable) {
    if (throwable instanceof ReadTimeoutException) {
      return new HttpResponseException("Response read timeout", throwable);
    }
    return new HttpResponseException(
        Optional.ofNullable(throwable.getMessage())
            .filter(StringUtils::isNotBlank)
            .orElse("Http response failed"),
        throwable);
  }

  private final PublishSubject<HttpContent> publisher;
  private final CompletableFuture<HttpResponse> httpResponseFuture;
  private final ConnectableFlowable<HttpContent> receivePipeline;
  private final AtomicLong responseTime;
  private final AtomicBoolean released;
  private final AtomicReference<String> releaseReason;
  private final SnoopyHttpRequest request;
  private final boolean closeConnection;
  private final AtomicReference<Status> status;
  private final AtomicLong throughput;
  private final StatusCallback statusChangeCallback;
  private final ThroughputCallback throughputChangeCallback;
  private final ProgressCallback progressCallback;
  private final StopWatch stopWatch;
  private final boolean ignoreBody;
  private final ObjectMapper mapper;

  /**
   * Instantiates a new Snoopy http response.
   *
   * @param request the request
   * @param statusCallback the status callback
   * @param throughputCallback the throughput callback
   * @param progressCallback the progress callback
   * @param ignoreBody the ignore body
   */
  @Builder
  protected SnoopyHttpResponse(
      @NonNull final SnoopyHttpRequest request,
      final StatusCallback statusCallback,
      final ThroughputCallback throughputCallback,
      final ProgressCallback progressCallback,
      final boolean ignoreBody) {
    this.request = request;
    this.httpResponseFuture = new CompletableFuture<>();
    this.ignoreBody = ignoreBody;
    if (!ignoreBody && request.method().hasResponseBody()) {
      this.publisher = PublishSubject.create();
      this.receivePipeline =
          this.publisher
              .materialize()
              .toFlowable(BackpressureStrategy.BUFFER)
              .dematerialize(event -> event)
              .replay();
      this.receivePipeline.connect();
    } else {
      this.publisher = null;
      this.receivePipeline = null;
    }
    this.responseTime = new AtomicLong(0);
    this.released = new AtomicBoolean(false);
    this.releaseReason = new AtomicReference<>(null);
    this.closeConnection =
        StringUtils.equalsIgnoreCase(
            request.headers().get(HttpHeaderNames.CONNECTION), HttpHeaderValues.CLOSE);
    this.statusChangeCallback =
        Optional.ofNullable(statusCallback).orElse(StatusCallback.DO_NOTHING_STATUS_CALLBACK);
    this.throughputChangeCallback =
        Optional.ofNullable(throughputCallback)
            .orElse(ThroughputCallback.DO_NOTHING_THROUGHPUT_CALLBACK);
    this.progressCallback =
        Optional.ofNullable(progressCallback).orElse(ProgressCallback.DO_NOTHING_PROGRESS_CALLBACK);
    this.throughput = new AtomicLong(0);
    this.status = new AtomicReference<>(Status.PENDING);
    this.stopWatch = StopWatch.builder().build();
    this.mapper = new ObjectMapper();
  }
}
