package org.bitbucket.abuwandi.snoopy.client;

import io.reactivex.rxjava3.core.Single;
import java.net.URI;
import javax.net.ssl.SSLSession;
import org.bitbucket.abuwandi.snoopy.client.bootstrap.Connection;

/**
 * The interface Snoopy request.
 *
 * @param <T> the type parameter
 */
public interface SnoopyRequest<T> {

  /**
   * Execute single.
   *
   * @return the single
   */
  Single<? extends SnoopyResponse<T>> execute();

  /**
   * Reference string.
   *
   * @return the string
   */
  String reference();

  /**
   * Connection connection.
   *
   * @return the connection
   */
  Connection connection();

  /**
   * Time long.
   *
   * @return the long
   */
  long time();

  /**
   * Uri uri.
   *
   * @return the uri
   */
  URI uri();

  /**
   * Sets status.
   *
   * @param status the status
   */
  void setStatus(Status status);

  /**
   * Sets throughput.
   *
   * @param throughput the throughput
   */
  void setThroughput(long throughput);

  /**
   * Gets status.
   *
   * @return the status
   */
  Status getStatus();

  /**
   * Handshake.
   *
   * @param sslSession the ssl session
   */
  void handshake(SSLSession sslSession);
}
