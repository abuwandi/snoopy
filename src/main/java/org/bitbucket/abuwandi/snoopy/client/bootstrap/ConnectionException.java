package org.bitbucket.abuwandi.snoopy.client.bootstrap;

import org.bitbucket.abuwandi.snoopy.exceptions.SnoopyException;

/** The type Connection exception. */
public class ConnectionException extends SnoopyException {

  /**
   * Instantiates a new Connection exception.
   *
   * @param message the message
   */
  public ConnectionException(String message) {
    super(message);
  }

  /**
   * Instantiates a new Connection exception.
   *
   * @param cause the cause
   */
  public ConnectionException(Throwable cause) {
    super(cause);
  }

  /**
   * Instantiates a new Connection exception.
   *
   * @param message the message
   * @param cause the cause
   */
  public ConnectionException(String message, Throwable cause) {
    super(message, cause);
  }
}
