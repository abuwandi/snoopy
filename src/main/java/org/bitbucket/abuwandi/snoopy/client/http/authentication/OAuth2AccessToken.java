package org.bitbucket.abuwandi.snoopy.client.http.authentication;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.bitbucket.abuwandi.snoopy.model.Token;

/** The type O auth 2 access token. */
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OAuth2AccessToken implements Token {

  @JsonIgnore
  @Setter(AccessLevel.NONE)
  private final Long createdTime;

  @JsonProperty("access_token")
  private String accessToken;

  @JsonProperty("token_type")
  private String tokenType;

  @JsonProperty("expires_in")
  private Long expiresIn;

  @JsonProperty("instance_url")
  private String instanceUrl;

  private String id;

  @JsonProperty("issued_at")
  private String issuedAt;

  private String signature;

  /** Instantiates a new O auth 2 access token. */
  public OAuth2AccessToken() {
    createdTime = System.currentTimeMillis();
  }

  @JsonIgnore
  public boolean isValid() {
    return TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - createdTime)
        < Optional.ofNullable(expiresIn).orElse(0L);
  }

  @JsonIgnore
  @Override
  public String content() {
    return getAccessToken();
  }

  @JsonIgnore
  @Override
  public Duration duration() {
    return Duration.of(Optional.ofNullable(expiresIn).orElse(0L), ChronoUnit.MILLIS);
  }

  @Override
  public String toString() {
    return new ToStringBuilder(this, ToStringStyle.JSON_STYLE) {
      @Override
      public ToStringBuilder append(String fieldName, Object obj) {
        if (obj == null) {
          return this;
        }
        return super.append(fieldName, obj);
      }
    }.append("id", id)
        .append("accessToken", accessToken)
        .append("createdTime", createdTime)
        .append("signature", signature)
        .append("expiresIn", expiresIn)
        .append("tokenType", tokenType)
        .append("issuedAt", issuedAt)
        .append("instanceUrl", instanceUrl)
        .build();
  }

  @Override
  public boolean equals(Object obj) {
    OAuth2AccessToken other;
    return this == obj
        || (obj instanceof OAuth2AccessToken
            && new EqualsBuilder()
                .append(id, (other = (OAuth2AccessToken) obj).id)
                .append(accessToken, other.accessToken)
                .append(createdTime, other.createdTime)
                .append(signature, other.signature)
                .append(expiresIn, other.expiresIn)
                .append(tokenType, other.tokenType)
                .append(issuedAt, other.issuedAt)
                .append(instanceUrl, other.instanceUrl)
                .build());
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder()
        .append(id)
        .append(accessToken)
        .append(createdTime)
        .append(signature)
        .append(expiresIn)
        .append(tokenType)
        .append(issuedAt)
        .append(instanceUrl)
        .build();
  }
}
