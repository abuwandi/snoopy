package org.bitbucket.abuwandi.snoopy.client.http.authentication;

import java.net.URI;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import lombok.Builder;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.bitbucket.abuwandi.snoopy.client.authentication.Authentication;

/** The type O auth 2. */
@Getter
public class OAuth2 implements Authentication {

  private final URI uri;
  private final String clientId;
  private final String secret;
  private final String username;
  private final String password;
  private final GrantType grantType;
  private final Map<String, String> query;

  /**
   * Instantiates a new O auth 2.
   *
   * @param uri the uri
   * @param clientId the client id
   * @param secret the secret
   * @param username the username
   * @param password the password
   * @param grantType the grant type
   * @param query the query
   */
  @Builder
  public OAuth2(
      URI uri,
      String clientId,
      String secret,
      String username,
      String password,
      GrantType grantType,
      Map<String, String> query) {
    this.uri = uri;
    this.clientId = clientId;
    this.secret = secret;
    this.username = username;
    this.password = password;
    this.grantType = grantType;
    this.query = query;
  }

  @Override
  public boolean equals(Object obj) {
    OAuth2 rhs;
    return obj == this
        || (obj instanceof OAuth2
            && new EqualsBuilder()
                .append(this.uri, (rhs = (OAuth2) obj).uri)
                .append(this.clientId, rhs.clientId)
                .append(this.secret, rhs.secret)
                .append(this.username, rhs.username)
                .append(this.password, rhs.password)
                .append(this.grantType, rhs.grantType)
                .append(this.query, rhs.query)
                .isEquals());
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder()
        .append(uri)
        .append(clientId)
        .append(secret)
        .append(username)
        .append(password)
        .append(grantType)
        .append(query)
        .toHashCode();
  }

  @Override
  public String toString() {
    return new ToStringBuilder(this)
        .append("uri", uri)
        .append("clientId", clientId)
        .append("secret", secret)
        .append("username", username)
        .append("password", password)
        .append("grantType", grantType)
        .append("query", query)
        .toString();
  }

  /** The enum Grant type. */
  public enum GrantType {
    /** Client credentials grant type. */
    CLIENT_CREDENTIALS("client_credentials"),
    /** Device code grant type. */
    DEVICE_CODE("device_code"),
    /** Authorization code grant type. */
    AUTHORIZATION_CODE("authorization_cde"),
    /** Refresh token grant type. */
    REFRESH_TOKEN("refresh_token");

    private String name;

    GrantType(String name) {
      this.name = name;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
      return name;
    }

    /**
     * Value for name grant type.
     *
     * @param name the name
     * @return the grant type
     */
    public static GrantType valueForName(final String name) {
      return Optional.ofNullable(name)
          .filter(StringUtils::isNoneBlank)
          .map(
              target ->
                  Arrays.stream(GrantType.values())
                      .filter(arg -> arg.name.equals(target))
                      .findFirst()
                      .orElseThrow(() -> new IllegalArgumentException("Invalid argument name")))
          .orElseThrow(() -> new IllegalArgumentException("Invalid argument name"));
    }
  }
}
