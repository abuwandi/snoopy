package org.bitbucket.abuwandi.snoopy.client.http;

import io.netty.handler.codec.http.HttpVersion;
import java.net.URI;
import lombok.Builder;
import org.bitbucket.abuwandi.snoopy.client.ApplicationProtocol;
import org.bitbucket.abuwandi.snoopy.client.SnoopyConfig;

/** The type Http client. */
public class HttpClientImpl implements HttpClient {

  private final SnoopyConfig config;
  private final ApplicationProtocol applicationProtocol;

  /**
   * Instantiates a new Http client.
   *
   * @param config the config
   */
  @Builder
  public HttpClientImpl(final SnoopyConfig config) {
    this.config = config;
    this.applicationProtocol = Http11ApplicationProtocol.builder().config(config).build();
  }

  public HttpPostRequestBuilder post(URI uri) {
    return new HttpPostRequestBuilder()
        .version(HttpVersion.HTTP_1_1)
        .applicationProtocol(applicationProtocol)
        .uri(uri)
        .host(uri.getHost())
        .keepConnectionAlive();
  }

  public HttpGetRequestBuilder get(URI uri) {
    return new HttpGetRequestBuilder()
        .version(HttpVersion.HTTP_1_1)
        .applicationProtocol(applicationProtocol)
        .uri(uri)
        .host(uri.getHost())
        .keepConnectionAlive();
  }

  public HttpRequestBuilder head(URI uri) {
    return new HttpRequestBuilder(HttpMethod.HEAD)
        .version(HttpVersion.HTTP_1_1)
        .applicationProtocol(applicationProtocol)
        .uri(uri)
        .host(uri.getHost())
        .keepConnectionAlive();
  }

  public HttpRequestBodyBuilder put(URI uri) {
    return new HttpRequestBodyBuilder(HttpMethod.PUT)
        .version(HttpVersion.HTTP_1_1)
        .applicationProtocol(applicationProtocol)
        .uri(uri)
        .host(uri.getHost())
        .keepConnectionAlive();
  }

  public HttpRequestBodyBuilder patch(URI uri) {
    return new HttpRequestBodyBuilder(HttpMethod.PATCH)
        .version(HttpVersion.HTTP_1_1)
        .applicationProtocol(applicationProtocol)
        .uri(uri)
        .host(uri.getHost())
        .keepConnectionAlive();
  }

  public HttpRequestBodyBuilder delete(URI uri) {
    return new HttpRequestBodyBuilder(HttpMethod.DELETE)
        .version(HttpVersion.HTTP_1_1)
        .applicationProtocol(applicationProtocol)
        .uri(uri)
        .host(uri.getHost())
        .keepConnectionAlive();
  }

  public HttpRequestBuilder options(URI uri) {
    return new HttpRequestBuilder(HttpMethod.OPTIONS)
        .version(HttpVersion.HTTP_1_1)
        .applicationProtocol(applicationProtocol)
        .uri(uri)
        .host(uri.getHost())
        .keepConnectionAlive();
  }
}
