package org.bitbucket.abuwandi.snoopy.client.http.authentication;

import io.netty.handler.codec.http.HttpObject;
import org.bitbucket.abuwandi.snoopy.client.authentication.Authentication;
import org.bitbucket.abuwandi.snoopy.client.authentication.Authenticator;
import org.bitbucket.abuwandi.snoopy.client.authentication.TokenProvider;
import org.bitbucket.abuwandi.snoopy.client.http.SnoopyHttpRequest;
import org.bitbucket.abuwandi.snoopy.model.Token;

/** The type Authenticators. */
public class Authenticators {

  /**
   * Bearer authenticator authenticator.
   *
   * @param tokenProvider the token provider
   * @param authentication the authentication
   * @return the authenticator
   */
  public static Authenticator<HttpObject, SnoopyHttpRequest> bearerAuthenticator(
      TokenProvider<Authentication, Token> tokenProvider, Authentication authentication) {
    return BearerTokenAuthenticator.builder()
        .authentication(authentication)
        .tokenProvider(tokenProvider)
        .build();
  }

  private Authenticators() {
    throw new AssertionError("Authenticators cannot be instantiated");
  }
}
