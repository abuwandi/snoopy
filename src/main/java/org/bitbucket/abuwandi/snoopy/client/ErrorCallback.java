package org.bitbucket.abuwandi.snoopy.client;

/** The interface Error callback. */
public interface ErrorCallback {

  /**
   * On error.
   *
   * @param throwable the throwable
   */
  void onError(Throwable throwable);
}
