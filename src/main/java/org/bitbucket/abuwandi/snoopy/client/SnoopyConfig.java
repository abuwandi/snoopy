package org.bitbucket.abuwandi.snoopy.client;

import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.bitbucket.abuwandi.snoopy.client.bootstrap.ConnectionPoolMode;
import org.bitbucket.abuwandi.snoopy.client.bootstrap.ConnectionPoolStatsListener;
import org.bitbucket.abuwandi.snoopy.util.ByteUnit;

/** The type Snoopy config. */
@Getter
@Setter
public class SnoopyConfig {

  /**
   * Defaults snoopy config.
   *
   * @return the snoopy config
   */
  public static SnoopyConfig defaults() {
    return SnoopyConfig.builder().build();
  }

  @Override
  public String toString() {
    return new ToStringBuilder(this)
        .append("maximumHostConnections", maximumHostConnections)
        .append("maximumHostIdleConnections", maximumHostIdleConnections)
        .append("maximumConnectionIdleDuration", maximumConnectionIdleDuration)
        .append("maximumConnectionIdleDurationUnit", maximumConnectionIdleDurationUnit)
        .append("workerExecutorThreads", workerExecutorThreads)
        .append("tcpNoDelay", tcpNoDelay)
        .append("connectionTimeout", connectionTimeout)
        .append("sendBufferSize", sendBufferSize)
        .append("receiveBufferSize", receiveBufferSize)
        .append("writeBufferLowWaterMark", writeBufferLowWaterMark)
        .append("writeBufferHighWaterMark", writeBufferHighWaterMark)
        .toString();
  }

  @Override
  public boolean equals(Object other) {
    SnoopyConfig rhs;
    return other == this
        || (other instanceof SnoopyConfig
            && new EqualsBuilder()
                .append(
                    this.maximumHostConnections,
                    (rhs = (SnoopyConfig) other).maximumHostConnections)
                .append(this.maximumHostIdleConnections, rhs.maximumHostIdleConnections)
                .append(this.maximumConnectionIdleDuration, rhs.maximumConnectionIdleDuration)
                .append(
                    this.maximumConnectionIdleDurationUnit, rhs.maximumConnectionIdleDurationUnit)
                .append(this.workerExecutorThreads, rhs.workerExecutorThreads)
                .append(this.tcpNoDelay, rhs.tcpNoDelay)
                .append(this.connectionTimeout, rhs.connectionTimeout)
                .append(this.sendBufferSize, rhs.sendBufferSize)
                .append(this.receiveBufferSize, rhs.receiveBufferSize)
                .append(this.writeBufferLowWaterMark, rhs.writeBufferLowWaterMark)
                .append(this.writeBufferHighWaterMark, rhs.writeBufferHighWaterMark)
                .isEquals());
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder()
        .append(maximumHostConnections)
        .append(maximumHostIdleConnections)
        .append(maximumConnectionIdleDuration)
        .append(maximumConnectionIdleDurationUnit)
        .append(workerExecutorThreads)
        .append(tcpNoDelay)
        .append(connectionTimeout)
        .append(sendBufferSize)
        .append(receiveBufferSize)
        .append(writeBufferLowWaterMark)
        .append(writeBufferHighWaterMark)
        .toHashCode();
  }

  private int maximumHostConnections;
  private int workerExecutorThreads;
  private int maximumHostIdleConnections;
  private long maximumConnectionIdleDuration;
  private TimeUnit maximumConnectionIdleDurationUnit;
  private long connectionTimeout;
  private TimeUnit connectionTimeoutUnit;
  private long readTimeout;
  private TimeUnit readTimeoutUnit;
  private long idleTimeout;
  private TimeUnit idleTimeoutUnit;
  private long writeTimeout;
  private TimeUnit writeTimeoutUnit;
  private long sendBufferSize;
  private long receiveBufferSize;
  private long writeBufferLowWaterMark;
  private long writeBufferHighWaterMark;
  private boolean tcpNoDelay;
  private boolean insecure;
  private ConnectionPoolMode connectionPoolMode;
  private ConnectionPoolStatsListener connectionPoolStatsListener;

  /**
   * Instantiates a new Snoopy config.
   *
   * @param maximumHostConnections the maximum host connections
   * @param workerExecutorThreads the worker executor threads
   * @param connectionTimeout the connection timeout
   * @param connectionTimeoutUnit the connection timeout unit
   * @param readTimeout the read timeout
   * @param readTimeoutUnit the read timeout unit
   * @param writeTimeout the write timeout
   * @param writeTimeoutUnit the write timeout unit
   * @param sendBufferSize the send buffer size
   * @param receiveBufferSize the receive buffer size
   * @param writeBufferLowWaterMark the write buffer low water mark
   * @param writeBufferHighWaterMark the write buffer high water mark
   * @param tcpNoDelay the tcp no delay
   * @param insecure the insecure
   * @param connectionPoolMode the connection pool mode
   * @param connectionPoolStatsListener the connection pool stats listener
   */
  @Builder
  public SnoopyConfig(
      final int maximumHostConnections,
      final int maximumHostIdleConnections,
      final long maximumConnectionIdleDuration,
      final TimeUnit maximumConnectionIdleDurationUnit,
      final int workerExecutorThreads,
      final long connectionTimeout,
      final TimeUnit connectionTimeoutUnit,
      final long readTimeout,
      final TimeUnit readTimeoutUnit,
      final long idleTimeout,
      final TimeUnit idleTimeoutUnit,
      final long writeTimeout,
      final TimeUnit writeTimeoutUnit,
      final long sendBufferSize,
      final long receiveBufferSize,
      final long writeBufferLowWaterMark,
      final long writeBufferHighWaterMark,
      final boolean tcpNoDelay,
      final boolean insecure,
      final ConnectionPoolMode connectionPoolMode,
      final ConnectionPoolStatsListener connectionPoolStatsListener) {
    this.maximumHostConnections =
        maximumHostConnections <= 0
            ? Runtime.getRuntime().availableProcessors()
            : maximumHostConnections;

    this.maximumHostIdleConnections =
        maximumHostIdleConnections <= 0 ? maximumHostConnections : maximumHostIdleConnections;

    if (maximumConnectionIdleDuration > 0 && Objects.nonNull(maximumConnectionIdleDurationUnit)) {
      this.maximumConnectionIdleDuration = maximumConnectionIdleDuration;
      this.maximumConnectionIdleDurationUnit = maximumConnectionIdleDurationUnit;
    } else {
      this.maximumConnectionIdleDuration = 10;
      this.maximumConnectionIdleDurationUnit = TimeUnit.MINUTES;
    }

    if (connectionTimeout > 0 && Objects.nonNull(connectionTimeoutUnit)) {
      this.connectionTimeout = connectionTimeout;
      this.connectionTimeoutUnit = connectionTimeoutUnit;
    } else {
      this.connectionTimeout = 10;
      this.connectionTimeoutUnit = TimeUnit.SECONDS;
    }

    if (readTimeout > 0 && Objects.nonNull(readTimeoutUnit)) {
      this.readTimeout = readTimeout;
      this.readTimeoutUnit = readTimeoutUnit;
    } else {
      this.readTimeout = 10;
      this.readTimeoutUnit = TimeUnit.SECONDS;
    }

    if (idleTimeout > 0
        && Objects.nonNull(idleTimeoutUnit)
        && idleTimeoutUnit.toSeconds(idleTimeout)
            <= (this.readTimeoutUnit.toSeconds(this.readTimeout) >> 1)) {
      this.idleTimeout = idleTimeout;
      this.idleTimeoutUnit = idleTimeoutUnit;
    } else {
      this.idleTimeout = this.readTimeoutUnit.toSeconds(this.readTimeout) >> 1;
      this.idleTimeoutUnit = TimeUnit.SECONDS;
    }

    if (writeTimeout > 0 && Objects.nonNull(writeTimeoutUnit)) {
      this.writeTimeout = writeTimeout;
      this.writeTimeoutUnit = writeTimeoutUnit;
    } else {
      this.writeTimeout = 10;
      this.writeTimeoutUnit = TimeUnit.SECONDS;
    }

    this.workerExecutorThreads =
        workerExecutorThreads == 0
            ? Runtime.getRuntime().availableProcessors()
            : workerExecutorThreads;

    this.tcpNoDelay = tcpNoDelay;
    this.sendBufferSize = sendBufferSize == 0 ? (int) ByteUnit.KIB.toBytes(8) : sendBufferSize;
    this.receiveBufferSize =
        receiveBufferSize == 0 ? (int) ByteUnit.KIB.toBytes(8) : receiveBufferSize;
    this.writeBufferLowWaterMark =
        writeBufferLowWaterMark == 0 ? (int) ByteUnit.KIB.toBytes(8) : writeBufferLowWaterMark;
    this.writeBufferHighWaterMark =
        writeBufferHighWaterMark == 0 ? (int) ByteUnit.KIB.toBytes(32) : writeBufferHighWaterMark;
    this.insecure = insecure;
    this.connectionPoolMode =
        Optional.ofNullable(connectionPoolMode).orElse(ConnectionPoolMode.FIXED);
    this.connectionPoolStatsListener = connectionPoolStatsListener;
  }

  /** The type Snoopy config builder. */
  public static class SnoopyConfigBuilder {

    private ConnectionPoolMode connectionPoolMode;

    /**
     * Fixed connection pool snoopy config builder.
     *
     * @return the snoopy config builder
     */
    public SnoopyConfigBuilder fixedConnectionPool() {
      this.connectionPoolMode = ConnectionPoolMode.FIXED;
      return this;
    }

    /**
     * Cached connection pool snoopy config builder.
     *
     * @return the snoopy config builder
     */
    public SnoopyConfigBuilder cachedConnectionPool() {
      this.connectionPoolMode = ConnectionPoolMode.CACHED;
      return this;
    }

    /**
     * Single connection pool snoopy config builder.
     *
     * @return the snoopy config builder
     */
    public SnoopyConfigBuilder singleConnectionPool() {
      this.connectionPoolMode = ConnectionPoolMode.SINGLE;
      return this;
    }
  }
}
