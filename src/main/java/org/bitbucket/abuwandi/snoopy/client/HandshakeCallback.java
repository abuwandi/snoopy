package org.bitbucket.abuwandi.snoopy.client;

/** The interface Handshake callback. */
public interface HandshakeCallback {

  /** The constant DO_NOTHING_HANDSHAKE_CALLBACK. */
  HandshakeCallback DO_NOTHING_HANDSHAKE_CALLBACK = sslSessionInfo -> {};

  /**
   * On handshake.
   *
   * @param sslSessionInfo the ssl session info
   */
  void onHandshake(SslSessionInfo sslSessionInfo);
}
