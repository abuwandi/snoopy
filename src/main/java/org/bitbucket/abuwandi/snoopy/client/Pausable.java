package org.bitbucket.abuwandi.snoopy.client;

/** The interface Pausable. */
public interface Pausable {

  /**
   * Pause boolean.
   *
   * @return the boolean
   */
  boolean pause();

  /**
   * Resume boolean.
   *
   * @return the boolean
   */
  boolean resume();

  /**
   * Is paused boolean.
   *
   * @return the boolean
   */
  boolean isPaused();
}
