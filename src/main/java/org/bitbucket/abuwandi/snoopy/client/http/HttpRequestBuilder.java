package org.bitbucket.abuwandi.snoopy.client.http;

import io.netty.handler.codec.http.DefaultHttpHeaders;
import io.netty.handler.codec.http.HttpHeaderNames;
import io.netty.handler.codec.http.HttpHeaderValues;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpObject;
import io.netty.handler.codec.http.HttpVersion;
import io.reactivex.rxjava3.core.Single;
import java.net.URI;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.Base64;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.bitbucket.abuwandi.snoopy.client.ApplicationProtocol;
import org.bitbucket.abuwandi.snoopy.client.HandshakeCallback;
import org.bitbucket.abuwandi.snoopy.client.ProgressCallback;
import org.bitbucket.abuwandi.snoopy.client.StatusCallback;
import org.bitbucket.abuwandi.snoopy.client.ThroughputCallback;
import org.bitbucket.abuwandi.snoopy.client.authentication.Authentication;
import org.bitbucket.abuwandi.snoopy.client.authentication.Authenticator;
import org.bitbucket.abuwandi.snoopy.client.authentication.TokenProvider;
import org.bitbucket.abuwandi.snoopy.client.bootstrap.ConnectionException;
import org.bitbucket.abuwandi.snoopy.client.http.authentication.BearerTokenAuthenticator;
import org.bitbucket.abuwandi.snoopy.model.Token;
import org.bitbucket.abuwandi.snoopy.util.RetryStrategy;

/** The type Http request builder. */
public class HttpRequestBuilder {

  /** The Do nothing authenticator. */
  protected Authenticator<HttpObject, SnoopyHttpRequest> DO_NOTHING_AUTHENTICATOR = Single::just;

  /** The Method. */
  protected HttpMethod method;

  /** The Uri. */
  protected URI uri;

  /** The Version. */
  protected HttpVersion version;

  /** The Headers. */
  protected HttpHeaders headers;

  /** The Authentication. */
  protected Authentication authentication;

  /** The Token provider. */
  protected TokenProvider<? super Authentication, ? super Token> tokenProvider;

  /** The Reference. */
  protected String reference;

  /** The Follow redirects. */
  protected boolean followRedirects;

  /** The Redirects depth. */
  protected int redirectsDepth;

  /** The Fail if not successful response. */
  protected boolean failIfNotSuccessfulResponse;

  /** The Response status callback. */
  protected StatusCallback responseStatusCallback;

  /** The Response throughput callback. */
  protected ThroughputCallback responseThroughputCallback;

  /** The Response progress callback. */
  protected ProgressCallback responseProgressCallback;

  /** The Status callback. */
  protected StatusCallback statusCallback;

  /** The Handshake callback. */
  protected HandshakeCallback handshakeCallback;

  /** The Connection retry. */
  protected int connectionRetry;

  /** The Ignore response body. */
  protected boolean ignoreResponseBody;

  /** The Query map. */
  protected final Map<CharSequence, List<CharSequence>> queryMap;

  /** The Application protocol. */
  protected ApplicationProtocol applicationProtocol;

  /**
   * Instantiates a new Http request builder.
   *
   * @param method the method
   */
  HttpRequestBuilder(final HttpMethod method) {
    this.method = method;
    this.headers = new DefaultHttpHeaders();
    this.queryMap = new HashMap<>();
  }

  /**
   * Content length char sequence.
   *
   * @return the char sequence
   */
  public CharSequence contentLength() {
    return headers.get(HttpHeaderNames.CONTENT_LENGTH);
  }

  /**
   * Token provider http request builder.
   *
   * @param tokenProvider the token provider
   * @return the http request builder
   */
  public HttpRequestBuilder tokenProvider(TokenProvider<Authentication, Token> tokenProvider) {
    this.tokenProvider = tokenProvider;
    return this;
  }

  /**
   * Reference http request builder.
   *
   * @param reference the reference
   * @return the http request builder
   */
  public HttpRequestBuilder reference(final String reference) {
    this.reference = reference;
    return this;
  }

  /**
   * Version http request builder.
   *
   * @param version the version
   * @return the http request builder
   */
  public HttpRequestBuilder version(final HttpVersion version) {
    this.version = version;
    return this;
  }

  /**
   * Host http request builder.
   *
   * @param host the host
   * @return the http request builder
   */
  public HttpRequestBuilder host(final CharSequence host) {
    this.headers.set(HttpHeaderNames.HOST, host);
    return this;
  }

  /**
   * Close connection http request builder.
   *
   * @return the http request builder
   */
  public HttpRequestBuilder closeConnection() {
    this.headers.set(HttpHeaderNames.CONNECTION, HttpHeaderValues.CLOSE);
    return this;
  }

  /**
   * Keep connection alive http request builder.
   *
   * @return the http request builder
   */
  public HttpRequestBuilder keepConnectionAlive() {
    this.headers.set(HttpHeaderNames.CONNECTION, HttpHeaderValues.KEEP_ALIVE);
    return this;
  }

  /**
   * Uri http request builder.
   *
   * @param uri the uri
   * @return the http request builder
   */
  public HttpRequestBuilder uri(final URI uri) {
    this.uri = uri;
    return this;
  }

  /**
   * Authentication http request builder.
   *
   * @param authentication the authentication
   * @return the http request builder
   */
  public HttpRequestBuilder authentication(final Authentication authentication) {
    if (Objects.nonNull(authentication)) {
      this.authentication = authentication;
    }
    return this;
  }

  /**
   * Header http request builder.
   *
   * @param name the name
   * @param values the values
   * @return the http request builder
   */
  public HttpRequestBuilder header(CharSequence name, CharSequence... values) {
    if (values == null || values.length < 1) {
      return this;
    }
    this.headers.set(name, Stream.of(values).collect(Collectors.toCollection(LinkedList::new)));
    return this;
  }

  /**
   * Query http request builder.
   *
   * @param name the name
   * @param values the values
   * @return the http request builder
   */
  public HttpRequestBuilder query(CharSequence name, CharSequence... values) {
    if (StringUtils.isBlank(name) || ArrayUtils.isEmpty(values)) {
      return this;
    }
    final List<CharSequence> encodedValues =
        Stream.of(values)
            .filter(StringUtils::isNoneBlank)
            .flatMap(
                value -> {
                  try {
                    return Stream.of(
                        URLEncoder.encode(value.toString(), StandardCharsets.US_ASCII.name()));
                  } catch (Exception exc) {
                    return Stream.empty();
                  }
                })
            .collect(Collectors.toCollection(LinkedList::new));
    if (encodedValues.isEmpty()) {
      return this;
    }
    this.queryMap.put(name, encodedValues);
    return this;
  }

  /**
   * Headers http request builder.
   *
   * @param headerMap the header map
   * @return the http request builder
   */
  public HttpRequestBuilder headers(Map<CharSequence, List<CharSequence>> headerMap) {
    if (headerMap == null || headerMap.isEmpty()) {
      return this;
    }
    headerMap.forEach((key, values) -> this.headers.set(key, values));
    return this;
  }

  /**
   * Follow redirects http request builder.
   *
   * @param followRedirects the follow redirects
   * @return the http request builder
   */
  public HttpRequestBuilder followRedirects(final boolean followRedirects) {
    this.followRedirects = followRedirects;
    return this;
  }

  /**
   * Fail if not successful response http request builder.
   *
   * @param failIfNotSuccessfulResponse the fail if not successful response
   * @return the http request builder
   */
  public HttpRequestBuilder failIfNotSuccessfulResponse(final boolean failIfNotSuccessfulResponse) {
    this.failIfNotSuccessfulResponse = failIfNotSuccessfulResponse;
    return this;
  }

  /**
   * Response status callback http request builder.
   *
   * @param statusCallback the status callback
   * @return the http request builder
   */
  public HttpRequestBuilder responseStatusCallback(final StatusCallback statusCallback) {
    this.responseStatusCallback = statusCallback;
    return this;
  }

  /**
   * Response throughput callback http request builder.
   *
   * @param throughputCallback the throughput callback
   * @return the http request builder
   */
  public HttpRequestBuilder responseThroughputCallback(
      final ThroughputCallback throughputCallback) {
    this.responseThroughputCallback = throughputCallback;
    return this;
  }

  /**
   * Response progress callback http request builder.
   *
   * @param responseProgressCallback the response progress callback
   * @return the http request builder
   */
  public HttpRequestBuilder responseProgressCallback(
      final ProgressCallback responseProgressCallback) {
    this.responseProgressCallback = responseProgressCallback;
    return this;
  }

  /**
   * Status callback http request builder.
   *
   * @param statusCallback the status callback
   * @return the http request builder
   */
  public HttpRequestBuilder statusCallback(final StatusCallback statusCallback) {
    this.statusCallback = statusCallback;
    return this;
  }

  /**
   * Handshake callback http request builder.
   *
   * @param handshakeCallback the handshake callback
   * @return the http request builder
   */
  public HttpRequestBuilder handshakeCallback(final HandshakeCallback handshakeCallback) {
    this.handshakeCallback = handshakeCallback;
    return this;
  }

  /**
   * Connection retry http request builder.
   *
   * @param connectionRetry the connection retry
   * @return the http request builder
   */
  public HttpRequestBuilder connectionRetry(final int connectionRetry) {
    this.connectionRetry = connectionRetry;
    return this;
  }

  /**
   * Ignore response body http request builder.
   *
   * @param ignoreResponseBody the ignore response body
   * @return the http request builder
   */
  public HttpRequestBuilder ignoreResponseBody(final boolean ignoreResponseBody) {
    this.ignoreResponseBody = ignoreResponseBody;
    return this;
  }

  /**
   * Basic http request builder.
   *
   * @param username the username
   * @param password the password
   * @return the http request builder
   */
  public HttpRequestBuilder basic(final CharSequence username, final CharSequence password) {
    if (StringUtils.isBlank(username) || StringUtils.isBlank(password)) {
      return this;
    }
    header(
        HttpHeaderNames.AUTHORIZATION,
        String.format(
            "Basic %s",
            Base64.getEncoder()
                .encodeToString(
                    String.format("%s:%s", username, password).getBytes(StandardCharsets.UTF_8))));
    return this;
  }

  /**
   * Bearer http request builder.
   *
   * @param token the token
   * @return the http request builder
   */
  public HttpRequestBuilder bearer(final CharSequence token) {
    if (StringUtils.isBlank(token)) {
      return this;
    }
    header(HttpHeaderNames.AUTHORIZATION, String.format("Bearer %s", token));
    return this;
  }

  /**
   * Redirects depth http request builder.
   *
   * @param redirectsDepth the redirects depth
   * @return the http request builder
   */
  protected HttpRequestBuilder redirectsDepth(final int redirectsDepth) {
    this.redirectsDepth = redirectsDepth;
    return this;
  }

  /**
   * Application protocol http request builder.
   *
   * @param applicationProtocol the application protocol
   * @return the http request builder
   */
  protected HttpRequestBuilder applicationProtocol(final ApplicationProtocol applicationProtocol) {
    this.applicationProtocol = applicationProtocol;
    return this;
  }

  /**
   * Authenticator authenticator.
   *
   * @param tokenProvider the token provider
   * @param authentication the authentication
   * @return the authenticator
   */
  protected Authenticator<HttpObject, SnoopyHttpRequest> authenticator(
      final TokenProvider<? super Authentication, ? super Token> tokenProvider,
      final Authentication authentication) {
    if (Objects.isNull(tokenProvider) || Objects.isNull(authentication)) {
      return DO_NOTHING_AUTHENTICATOR;
    }
    return BearerTokenAuthenticator.builder()
        .authentication(authentication)
        .tokenProvider(tokenProvider)
        .build();
  }

  /**
   * Build uri uri.
   *
   * @param uri the uri
   * @return the uri
   */
  protected URI buildUri(final URI uri) {
    if (queryMap.isEmpty()) {
      return uri;
    }
    final String queryString =
        queryMap.entrySet().stream()
            .map(
                entry -> {
                  final String pattern = String.format("%s=%%s", entry.getKey());
                  return entry.getValue().stream()
                      .map(value -> String.format(pattern, value))
                      .collect(Collectors.joining("&"));
                })
            .collect(Collectors.joining("&"));
    final String newUriString;
    if (StringUtils.isNoneBlank(uri.getRawQuery())) {
      newUriString = String.format("%s&%s", uri.toString(), queryString);
    } else {
      newUriString = String.format("%s?%s", uri.toString(), queryString);
    }
    return URI.create(newUriString);
  }

  /**
   * Build snoopy http request.
   *
   * @return the snoopy http request
   */
  public SnoopyHttpRequest build() {
    return SnoopyHttpRequest.builder()
        .uri(buildUri(uri))
        .reference(reference)
        .version(version)
        .method(method)
        .headers(headers)
        .followRedirects(followRedirects)
        .redirectsDepth(redirectsDepth)
        .applicationProtocol(applicationProtocol)
        .authenticator(authenticator(tokenProvider, authentication))
        .failIfNotSuccessfulResponse(failIfNotSuccessfulResponse)
        .responseStatusCallback(responseStatusCallback)
        .responseThroughputCallback(responseThroughputCallback)
        .responseProgressCallback(responseProgressCallback)
        .statusChangeCallback(statusCallback)
        .handshakeCallback(handshakeCallback)
        .ignoreResponseBody(ignoreResponseBody)
        .builder(this)
        .connectionRetryStrategy(
            connectionRetry > 0
                ? RetryStrategy.builder()
                    .maxAttempts(connectionRetry)
                    .lowestDelay(100)
                    .growBy(100)
                    .highestDelay(500)
                    .unit(TimeUnit.MILLISECONDS)
                    .errorPredicate(ConnectionException.class::isInstance)
                    .build()
                : null)
        .build();
  }

  /**
   * Execute single.
   *
   * @return the single
   */
  public Single<SnoopyHttpResponse> execute() {
    return build().execute();
  }

  /**
   * Consume as string string.
   *
   * @return the string
   */
  public String consumeAsString() {
    return build().consumeAsString();
  }

  /**
   * Consume as file path.
   *
   * @param path the path
   * @return the path
   */
  public Path consumeAsFile(final Path path) {
    return build().consumeAsFile(path);
  }

  /**
   * Consume as t.
   *
   * @param <T> the type parameter
   * @param clazz the clazz
   * @return the t
   */
  public <T> T consumeAs(final Class<T> clazz) {
    return build().consumeAs(clazz);
  }
}
