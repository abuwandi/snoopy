package org.bitbucket.abuwandi.snoopy.client.http.util;

import io.netty.buffer.ByteBuf;
import io.reactivex.rxjava3.core.Maybe;
import io.reactivex.rxjava3.core.Single;
import java.nio.file.Path;
import lombok.NonNull;
import org.bitbucket.abuwandi.snoopy.client.http.SnoopyHttpRequest;
import org.bitbucket.abuwandi.snoopy.client.http.SnoopyHttpResponse;
import org.bitbucket.abuwandi.snoopy.util.Collector;
import org.bitbucket.abuwandi.snoopy.util.Collectors;
import org.bitbucket.abuwandi.snoopy.util.Disposal;

/** The type Extractors. */
public class Extractors {

  /**
   * Dump headers string.
   *
   * @param response the response
   * @return the string
   */
  public static String dumpHeaders(SnoopyHttpResponse response) {
    return response.headers().entries().stream()
        .collect(
            StringBuilder::new,
            (bldr, header) ->
                bldr.append(String.format("<< %s: %s%n", header.getKey(), header.getValue())),
            (bldr1, bldr2) -> {})
        .toString();
  }

  /**
   * Dump headers string.
   *
   * @param request the request
   * @return the string
   */
  public static String dumpHeaders(SnoopyHttpRequest request) {
    return request.headers().entries().stream()
        .collect(
            StringBuilder::new,
            (bldr, header) ->
                bldr.append(String.format(">> %s: %s%n", header.getKey(), header.getValue())),
            (bldr1, bldr2) -> {})
        .toString();
  }

  /**
   * Dump info string.
   *
   * @param request the request
   * @return the string
   */
  public static String dumpInfo(SnoopyHttpRequest request) {
    return String.format(">> %s %s", request.protocolVersion().text(), request.method().name());
  }

  /**
   * Dump status string.
   *
   * @param response the response
   * @return the string
   */
  public static String dumpStatus(SnoopyHttpResponse response) {
    return String.format(
        "<< %s %d %s",
        response.request().protocolVersion().text(),
        response.status().code(),
        response.status().reasonPhrase());
  }

  /**
   * Dispose body maybe.
   *
   * @param response the response
   * @return the maybe
   */
  public static Maybe<Long> disposeBody(@NonNull final SnoopyHttpResponse response) {
    final Disposal<ByteBuf> disposal = Collectors.byteBufferBlackHoleDisposal(null);
    return response
        .bodyAs(ByteBuf.class)
        .collect(() -> disposal, Collector::collect)
        .map(Collector::finish)
        .doFinally(disposal::close)
        .toMaybe();
  }

  /**
   * Save body to file single.
   *
   * @param response the response
   * @param path the path
   * @return the single
   */
  public static Single<Path> saveBodyToFile(
      @NonNull final SnoopyHttpResponse response, @NonNull final Path path) {
    final Collector<ByteBuf, Path> collector = Collectors.byteBufferFileCollector(path, null);
    return response
        .bodyAs(ByteBuf.class)
        .collect(() -> collector, Collector::collect)
        .map(Collector::finish)
        .doFinally(collector::close);
  }

  private Extractors() {
    throw new AssertionError("Extractors class cannot be instantiated");
  }
}
