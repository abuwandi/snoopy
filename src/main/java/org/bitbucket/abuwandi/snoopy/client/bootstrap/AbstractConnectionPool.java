package org.bitbucket.abuwandi.snoopy.client.bootstrap;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.EventLoop;
import io.reactivex.rxjava3.core.BackpressureStrategy;
import io.reactivex.rxjava3.core.Scheduler;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.internal.schedulers.RxThreadFactory;
import io.reactivex.rxjava3.schedulers.Schedulers;
import io.reactivex.rxjava3.subjects.PublishSubject;
import java.util.Objects;
import java.util.Optional;
import java.util.Queue;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Supplier;
import org.apache.commons.lang3.StringUtils;
import org.bitbucket.abuwandi.snoopy.util.StopWatch;
import org.bitbucket.abuwandi.snoopy.util.Utils;

/** The type Abstract connection pool. */
public abstract class AbstractConnectionPool implements ConnectionPool {

  /** The Connections queue. */
  protected final Queue<Connection> connectionsQueue;

  /** The Bootstrap. */
  protected final Bootstrap bootstrap;

  /** The Max connections. */
  protected final int maxConnections;

  /** The Connections total. */
  protected final AtomicInteger connectionsTotal;

  /** The Idle connections total. */
  protected final AtomicInteger idleConnectionsTotal;

  /** The Handler. */
  protected final ConnectionPoolHandler handler;

  /** The Executor. */
  protected final EventLoop executor;

  /** The Name. */
  protected final String name;

  /** The Stats listener. */
  protected final ConnectionPoolStatsListener statsListener;

  /** The Stats. */
  protected final AtomicReference<ConnectionPoolStats> stats;

  /** The Stats scheduler. */
  protected final Scheduler statsScheduler;

  /** The Publisher. */
  protected final PublishSubject<ConnectionPoolStats> publisher;

  /** The Stats disposable. */
  protected final Disposable statsDisposable;

  protected final int maxIdleConnections;
  protected final long maxIdleConnectionDuration;

  /**
   * Instantiates a new Abstract connection pool.
   *
   * @param bootstrap the bootstrap
   * @param handler the handler
   * @param maxConnections the max connections
   * @param name the name
   * @param statsListener the stats listener
   */
  AbstractConnectionPool(
      final Bootstrap bootstrap,
      final ConnectionPoolHandler handler,
      final int maxConnections,
      final int maxIdleConnections,
      final long maxIdleConnectionDuration,
      final String name,
      final ConnectionPoolStatsListener statsListener,
      final Supplier<Queue<Connection>> connectionsQueueSupplier) {
    this.connectionsQueue = connectionsQueueSupplier.get();
    this.bootstrap = bootstrap;
    this.executor = bootstrap.config().group().next();
    this.maxConnections = maxConnections;
    this.maxIdleConnections = maxIdleConnections;
    this.maxIdleConnectionDuration = maxIdleConnectionDuration;
    this.connectionsTotal = new AtomicInteger(0);
    this.idleConnectionsTotal = new AtomicInteger(0);
    this.handler = handler;
    this.name = name;
    this.bootstrap.handler(
        new io.netty.channel.ChannelInitializer<Channel>() {
          @Override
          protected void initChannel(Channel ch) {
            handler.channelCreated(ch);
          }
        });
    this.statsListener =
        Optional.ofNullable(statsListener)
            .orElse(ConnectionPoolStatsListener.DO_NOTHING_STATS_LISTENER);
    this.stats = new AtomicReference<>(ConnectionPoolStats.builder().build());
    this.statsScheduler =
        Schedulers.from(
            Executors.newSingleThreadExecutor(new RxThreadFactory("snoopy-stats-worker-")));
    this.publisher = PublishSubject.create();
    this.statsDisposable =
        publisher
            .toFlowable(BackpressureStrategy.BUFFER)
            .observeOn(statsScheduler)
            .subscribe(this.statsListener::stats);
  }

  public void cleanUp() {
    if (connectionsQueue.removeIf(
        connection -> {
          final boolean old = connection.getIdleTime() > maxIdleConnectionDuration;
          if (old) {
            connection.channel().close();
          }
          return old;
        })) {
      updateIdleAndTotalConnections(
          idleConnectionsTotal.updateAndGet(this::decrement),
          connectionsTotal.updateAndGet(this::decrement));
    }
  }

  public Single<Connection> acquire() {
    try {
      final Connection connection = acquire0();
      return Single.just(connection);
    } catch (Throwable throwable) {
      return Single.error(throwable);
    }
  }

  @Override
  public void release(Connection connection) {
    if (Objects.isNull(connection)) {
      return;
    }
    if (!connection.isValid() || idleConnectionsTotal.get() >= maxIdleConnections) {
      connection.channel().close();
      updateConnectionsTotal(connectionsTotal.updateAndGet(this::decrement));
      return;
    }
    if (this.connectionsQueue.offer(connection)) {
      postRelease(connection);
      updateIdleConnections(idleConnectionsTotal.updateAndGet(this::incrementIdleConnections));
    }
  }

  /**
   * Post release.
   *
   * @param connection the connection
   */
  protected void postRelease(final Connection connection) {
    if (executor.inEventLoop()) {
      this.handler.released(connection);
    } else {
      final CompletableFuture<Boolean> connectionRelease = new CompletableFuture<>();
      executor.execute(
          () -> {
            this.handler.released(connection);
            connectionRelease.complete(true);
          });
      try {
        connectionRelease.get();
      } catch (InterruptedException | ExecutionException exc) {
        // ignore
      }
    }
  }

  /**
   * Post acquire.
   *
   * @param connection the connection
   */
  protected void postAcquire(final Connection connection) {
    if (executor.inEventLoop()) {
      this.handler.acquired(connection);
    } else {
      final CompletableFuture<Boolean> connectionRelease = new CompletableFuture<>();
      executor.execute(
          () -> {
            this.handler.acquired(connection);
            connectionRelease.complete(true);
          });
      try {
        connectionRelease.get();
      } catch (InterruptedException | ExecutionException exc) {
        // ignore
      }
    }
  }

  /**
   * Update connections total.
   *
   * @param connectionsTotal the connections total
   */
  protected void updateConnectionsTotal(final int connectionsTotal) {
    publisher.onNext(
        stats.updateAndGet(
            stats ->
                ConnectionPoolStats.builder()
                    .name(name)
                    .maxConnections(maxConnections)
                    .idleConnectionsTotal(stats.getIdleConnectionsTotal())
                    .activeConnectionsTotal(connectionsTotal - stats.getIdleConnectionsTotal())
                    .connectionsTotal(connectionsTotal)
                    .build()));
  }

  /**
   * Update idle connections.
   *
   * @param idleConnections the idle connections
   */
  protected void updateIdleConnections(final int idleConnections) {
    publisher.onNext(
        stats.updateAndGet(
            stats ->
                ConnectionPoolStats.builder()
                    .name(name)
                    .maxConnections(maxConnections)
                    .idleConnectionsTotal(idleConnections)
                    .activeConnectionsTotal(stats.getConnectionsTotal() - idleConnections)
                    .connectionsTotal(stats.getConnectionsTotal())
                    .build()));
  }

  /**
   * Update idle and total connections.
   *
   * @param idleConnections the idle connections
   * @param connectionsTotal the connections total
   */
  protected void updateIdleAndTotalConnections(
      final int idleConnections, final int connectionsTotal) {
    publisher.onNext(
        stats.updateAndGet(
            stats ->
                ConnectionPoolStats.builder()
                    .name(name)
                    .maxConnections(maxConnections)
                    .idleConnectionsTotal(idleConnections)
                    .activeConnectionsTotal(connectionsTotal - idleConnections)
                    .connectionsTotal(connectionsTotal)
                    .build()));
  }

  @Override
  public void shutdown() {}

  /**
   * Acquire 0 connection.
   *
   * @return the connection
   */
  protected abstract Connection acquire0();

  /**
   * Create new connection connection.
   *
   * @return the connection
   */
  protected Connection createNewConnection() {
    updateConnectionsTotal(connectionsTotal.updateAndGet(this::incrementTotalConnections));
    try {
      final CompletableFuture<Connection> connectionFuture = new CompletableFuture<>();
      if (executor.inEventLoop()) {
        connect(connectionFuture);
      } else {
        executor.execute(() -> this.connect(connectionFuture));
      }
      return connectionFuture.get();
    } catch (ConnectionException exc) {
      updateConnectionsTotal(connectionsTotal.updateAndGet(this::decrement));
      throw exc;
    } catch (ExecutionException exc) {
      updateConnectionsTotal(connectionsTotal.updateAndGet(this::decrement));
      throw Optional.ofNullable(exc.getCause())
          .filter(ConnectionException.class::isInstance)
          .map(ConnectionException.class::cast)
          .orElseGet(() -> new ConnectionException("Connection failed", exc));
    } catch (InterruptedException exc) {
      updateConnectionsTotal(connectionsTotal.updateAndGet(this::decrement));
      throw new ConnectionException(
          String.format("Connection interrupted, %s", Utils.currentThreadDetails()), exc);
    } catch (Exception exc) {
      updateConnectionsTotal(connectionsTotal.updateAndGet(this::decrement));
      throw new ConnectionException(exc);
    } catch (Throwable throwable) {
      updateConnectionsTotal(connectionsTotal.updateAndGet(this::decrement));
      throw throwable;
    }
  }

  /**
   * Connect.
   *
   * @param connectionFuture the connection future
   */
  protected void connect(final CompletableFuture<Connection> connectionFuture) {
    if (!executor.inEventLoop()) {
      connectionFuture.completeExceptionally(new ConnectionException("Not in event loop"));
      return;
    }
    final Bootstrap bootstrap = this.bootstrap.clone(executor);
    final StopWatch stopWatch = new StopWatch();
    stopWatch.start();
    final ChannelFuture channelFuture = bootstrap.connect();
    if (channelFuture.isDone()) {
      handleConnectFuture(stopWatch.time(), channelFuture, connectionFuture);
      return;
    }
    channelFuture.addListener(
        (ChannelFutureListener) cf -> handleConnectFuture(stopWatch.time(), cf, connectionFuture));
  }

  /**
   * Handle connect future.
   *
   * @param connectionTime the connection time
   * @param channelFuture the channel future
   * @param connectionFuture the connection future
   */
  protected void handleConnectFuture(
      final long connectionTime,
      final ChannelFuture channelFuture,
      final CompletableFuture<Connection> connectionFuture) {
    if (channelFuture.isSuccess()) {
      if (validate(channelFuture.channel())) {
        final Connection newConnection =
            Connection.builder()
                .channel(channelFuture.channel())
                .onRelease(this::release)
                .connectionTime(connectionTime)
                .build();
        handler.acquired(newConnection);
        connectionFuture.complete(newConnection);
        return;
      }
    }
    connectionFuture.completeExceptionally(
        Optional.ofNullable(channelFuture.cause())
            .map(this::mapException)
            .orElseGet(() -> new ConnectionException("Connection failed")));
  }

  /**
   * Map exception connection exception.
   *
   * @param throwable the throwable
   * @return the connection exception
   */
  protected ConnectionException mapException(Throwable throwable) {
    return new ConnectionException(
        Optional.ofNullable(throwable.getMessage())
            .filter(StringUtils::isNotBlank)
            .orElse("Connection failed"),
        throwable);
  }

  /**
   * Decrement int.
   *
   * @param value the value
   * @return the int
   */
  protected int decrement(int value) {
    if (value > 0) {
      --value;
    }
    return value;
  }

  /**
   * Increment int.
   *
   * @param value the value
   * @return the int
   */
  protected int incrementTotalConnections(int value) {
    if (value < maxConnections) {
      ++value;
    }
    return value;
  }

  protected int incrementIdleConnections(int value) {
    if (value < maxIdleConnections) {
      ++value;
    }
    return value;
  }

  /**
   * Validate boolean.
   *
   * @param channel the channel
   * @return the boolean
   */
  protected boolean validate(Channel channel) {
    if (!channel.isActive() || !channel.isWritable() || !channel.isOpen()) {
      try {
        // called within the same channel thread
        channel.unsafe().closeForcibly();
      } catch (Exception exc) {
        // ignore
      }
      return false;
    }
    return true;
  }
}
