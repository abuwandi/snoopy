package org.bitbucket.abuwandi.snoopy.client.http;

import io.netty.buffer.ByteBuf;
import io.netty.handler.codec.http.HttpHeaderValues;
import java.io.InputStream;
import lombok.Builder;
import lombok.NonNull;

/** The type Default input stream request body. */
public class DefaultInputStreamRequestBody extends InputStreamRequestBody {

  @Override
  protected InputStream inputStream() {
    return inputStream;
  }

  @Override
  protected int getBufferInitialCapacity() {
    return chunkSize;
  }

  @Override
  protected void preWrite(ByteBuf buffer) {}

  @Override
  protected void postWrite(ByteBuf buffer) {}

  @Override
  public long contentLength() {
    return -1;
  }

  @Override
  public CharSequence contentType() {
    return HttpHeaderValues.APPLICATION_OCTET_STREAM;
  }

  @Override
  public CharSequence name() {
    return name;
  }

  @Override
  public CharSequence contentDisposition() {
    return String.format("form-data; name=\"%s\"; filename=\"%s\"", name, name);
  }

  private final InputStream inputStream;
  private final String name;

  /**
   * Instantiates a new Default input stream request body.
   *
   * @param inputStream the input stream
   * @param chunkSize the chunk size
   * @param name the name
   */
  @Builder
  public DefaultInputStreamRequestBody(
      @NonNull final InputStream inputStream, final int chunkSize, final String name) {
    super(chunkSize);
    this.inputStream = inputStream;
    this.name = name;
  }
}
