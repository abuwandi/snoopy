package org.bitbucket.abuwandi.snoopy.client.bootstrap;

import io.netty.bootstrap.Bootstrap;
import java.util.concurrent.ConcurrentLinkedQueue;
import lombok.Builder;

/** The type Cached connection pool. */
public class CachedConnectionPool extends AbstractConnectionPool {

  /**
   * Instantiates a new Cached connection pool.
   *
   * @param bootstrap the bootstrap
   * @param handler the handler
   * @param name the name
   * @param statsListener the stats listener
   */
  @Builder
  public CachedConnectionPool(
      final Bootstrap bootstrap,
      final ConnectionPoolHandler handler,
      final int maxIdleConnections,
      final long maxIdleConnectionDuration,
      final String name,
      final ConnectionPoolStatsListener statsListener) {
    super(
        bootstrap,
        handler,
        Integer.MAX_VALUE,
        maxIdleConnections,
        maxIdleConnectionDuration,
        name,
        statsListener,
        ConcurrentLinkedQueue::new);
  }

  public Connection acquire0() {
    Connection connection;
    while ((connection = this.connectionsQueue.poll()) != null) {
      if (connection.isValid()) {
        postAcquire(connection);
        connection.cached();
        updateIdleConnections(idleConnectionsTotal.updateAndGet(this::decrement));
        return connection;
      }
      connection.channel().close();
      updateIdleAndTotalConnections(
          idleConnectionsTotal.updateAndGet(this::decrement),
          connectionsTotal.updateAndGet(this::decrement));
    }
    return createNewConnection();
  }
}
