package org.bitbucket.abuwandi.snoopy.client.bootstrap;

/** The interface Connection pool provider. */
@FunctionalInterface
public interface ConnectionPoolProvider {

  /**
   * Connection pool connection pool.
   *
   * @param statsListener the stats listener
   * @return the connection pool
   */
  ConnectionPool connectionPool(ConnectionPoolStatsListener statsListener);
}
