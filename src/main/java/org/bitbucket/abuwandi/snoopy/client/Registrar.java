package org.bitbucket.abuwandi.snoopy.client;

/**
 * The interface Registrar.
 *
 * @param <T> the type parameter
 */
public interface Registrar<T> {

  /**
   * Register boolean.
   *
   * @param registrant the registrant
   * @return the boolean
   */
  boolean register(T registrant);

  /**
   * Deregister boolean.
   *
   * @param registrant the registrant
   * @return the boolean
   */
  boolean deregister(T registrant);

  /** Deregister all. */
  void deregisterAll();
}
