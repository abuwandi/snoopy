package org.bitbucket.abuwandi.snoopy.client;

import java.net.URI;
import lombok.Builder;
import org.bitbucket.abuwandi.snoopy.client.http.HttpClient;
import org.bitbucket.abuwandi.snoopy.client.http.HttpClientImpl;
import org.bitbucket.abuwandi.snoopy.client.http.HttpGetRequestBuilder;
import org.bitbucket.abuwandi.snoopy.client.http.HttpPostRequestBuilder;
import org.bitbucket.abuwandi.snoopy.client.http.HttpRequestBodyBuilder;
import org.bitbucket.abuwandi.snoopy.client.http.HttpRequestBuilder;

/** The type Snoopy. */
public class Snoopy implements HttpClient {

  /** The constant VERSION. */
  public static final String VERSION = "0.9.15";

  private final HttpClient httpClient;

  /**
   * Instantiates a new Snoopy.
   *
   * @param config the config
   */
  @Builder
  public Snoopy(final SnoopyConfig config) {
    this.httpClient = new HttpClientImpl(config);
  }

  /** Shutdown. */
  public void shutdown() {}

  public HttpPostRequestBuilder post(URI uri) {
    return this.httpClient.post(uri);
  }

  public HttpGetRequestBuilder get(URI uri) {
    return this.httpClient.get(uri);
  }

  public HttpRequestBuilder head(URI uri) {
    return this.httpClient.head(uri);
  }

  public HttpRequestBodyBuilder put(URI uri) {
    return this.httpClient.put(uri);
  }

  public HttpRequestBodyBuilder patch(URI uri) {
    return this.httpClient.patch(uri);
  }

  public HttpRequestBodyBuilder delete(URI uri) {
    return this.httpClient.delete(uri);
  }

  public HttpRequestBuilder options(URI uri) {
    return this.httpClient.options(uri);
  }
}
