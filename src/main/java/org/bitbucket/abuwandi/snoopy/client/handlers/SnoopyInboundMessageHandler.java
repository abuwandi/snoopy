package org.bitbucket.abuwandi.snoopy.client.handlers;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.ReferenceCountUtil;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;
import lombok.NonNull;
import org.bitbucket.abuwandi.snoopy.client.ResponseRegistrar;
import org.bitbucket.abuwandi.snoopy.client.SnoopyResponse;

/**
 * The type Snoopy inbound message handler.
 *
 * @param <T> the type parameter
 */
public class SnoopyInboundMessageHandler<T> extends ChannelInboundHandlerAdapter
    implements ResponseRegistrar<T> {

  @Override
  public void channelRead(ChannelHandlerContext ctx, Object msg) {
    final SnoopyResponse<T> response = atomicResponse.get();
    if (Objects.isNull(response) || response.isReleased() || !clazz.isInstance(msg)) {
      ReferenceCountUtil.safeRelease(msg);
      if (Objects.nonNull(response)) {
        atomicResponse.compareAndSet(response, null);
      }
      return;
    }
    final T messageObject = clazz.cast(msg);
    try {
      if (!response.onNext(messageObject)) {
        atomicResponse.compareAndSet(response, null);
      }
    } catch (Exception exc) {
      ReferenceCountUtil.safeRelease(msg);
      atomicResponse.compareAndSet(response, null);
      response.onError(exc);
    } catch (Throwable throwable) {
      ReferenceCountUtil.safeRelease(msg);
      atomicResponse.compareAndSet(response, null);
      response.onError(throwable);
      throw throwable;
    }
  }

  @Override
  public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
    final SnoopyResponse<T> response = atomicResponse.get();
    deregisterAll();
    if (Objects.nonNull(response)) {
      response.onError(cause);
    }
    ctx.close();
  }

  @Override
  public void channelInactive(ChannelHandlerContext ctx) {
    ctx.fireChannelInactive();
  }

  @Override
  public void channelUnregistered(ChannelHandlerContext ctx) throws Exception {
    deregisterAll();
    super.channelUnregistered(ctx);
  }

  @Override
  public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {
    deregisterAll();
    super.handlerRemoved(ctx);
  }

  public boolean deregister(@NonNull final SnoopyResponse<T> response) {
    return atomicResponse.compareAndSet(response, null);
  }

  public void deregisterAll() {
    atomicResponse.set(null);
  }

  public boolean register(@NonNull final SnoopyResponse<T> response) {
    return atomicResponse.compareAndSet(null, response);
  }

  private final AtomicReference<SnoopyResponse<T>> atomicResponse;
  private final Class<T> clazz;

  /**
   * Instantiates a new Snoopy inbound message handler.
   *
   * @param clazz the clazz
   */
  public SnoopyInboundMessageHandler(@NonNull final Class<T> clazz) {
    this.atomicResponse = new AtomicReference<>(null);
    this.clazz = clazz;
  }
}
