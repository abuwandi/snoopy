package org.bitbucket.abuwandi.snoopy.client.http;

import io.netty.buffer.ByteBuf;
import io.netty.handler.codec.http.HttpContent;
import io.netty.handler.codec.http.LastHttpContent;
import io.reactivex.rxjava3.core.Flowable;
import java.util.Objects;
import org.bitbucket.abuwandi.snoopy.client.Completable;
import org.bitbucket.abuwandi.snoopy.client.Pausable;
import org.bitbucket.abuwandi.snoopy.util.BufferSupplier;

/** The interface Request body. */
public interface RequestBody extends Pausable, Completable {

  /**
   * Next http content.
   *
   * @param supplier the supplier
   * @return the http content
   */
  HttpContent next(BufferSupplier<ByteBuf> supplier);

  /**
   * To flowable flowable.
   *
   * @param supplier the supplier
   * @return the flowable
   */
  default Flowable<HttpContent> toFlowable(BufferSupplier<ByteBuf> supplier) {
    return Flowable.generate(
        emitter -> {
          if (isPaused() || isCompleted()) {
            emitter.onComplete();
            return;
          }
          try {
            final HttpContent content = next(supplier);
            if (Objects.nonNull(content)) {
              emitter.onNext(content);
              return;
            }
            emitter.onNext(LastHttpContent.EMPTY_LAST_CONTENT);
            complete();
            emitter.onComplete();
          } catch (Throwable throwable) {
            emitter.onError(throwable);
          }
        });
  }

  /**
   * Content type char sequence.
   *
   * @return the char sequence
   */
  CharSequence contentType();

  /**
   * Content length long.
   *
   * @return the long
   */
  long contentLength();

  /**
   * Can resume boolean.
   *
   * @return the boolean
   */
  boolean canResume();
}
