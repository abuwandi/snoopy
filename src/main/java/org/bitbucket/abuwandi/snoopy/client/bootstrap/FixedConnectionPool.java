package org.bitbucket.abuwandi.snoopy.client.bootstrap;

import io.netty.bootstrap.Bootstrap;
import java.util.Objects;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import lombok.Builder;

/** The type Fixed connection pool. */
public class FixedConnectionPool extends AbstractConnectionPool {

  private final ReentrantLock lock;
  private final Condition idleConnectionsAvailable;

  /**
   * Instantiates a new Fixed connection pool.
   *
   * @param bootstrap the bootstrap
   * @param handler the handler
   * @param maxConnections the max connections
   * @param name the name
   * @param statsListener the stats listener
   */
  @Builder
  public FixedConnectionPool(
      final Bootstrap bootstrap,
      final ConnectionPoolHandler handler,
      final int maxConnections,
      final int maxIdleConnections,
      final long maxIdleConnectionDuration,
      final String name,
      final ConnectionPoolStatsListener statsListener) {
    super(
        bootstrap,
        handler,
        maxConnections,
        maxIdleConnections,
        maxIdleConnectionDuration,
        name,
        statsListener,
        ConcurrentLinkedQueue::new);
    this.lock = new ReentrantLock();
    this.idleConnectionsAvailable = lock.newCondition();
  }

  @Override
  public void release(Connection connection) {
    if (Objects.isNull(connection)) {
      return;
    }
    if (!connection.isValid() || idleConnectionsTotal.get() >= maxIdleConnections) {
      updateConnectionsTotal(connectionsTotal.updateAndGet(this::decrement));
      connection.channel().close();
    } else if (this.connectionsQueue.offer(connection)) {
      updateIdleConnections(idleConnectionsTotal.updateAndGet(this::incrementIdleConnections));
      postRelease(connection);
    }
    lock.lock();
    try {
      idleConnectionsAvailable.signal();
    } finally {
      lock.unlock();
    }
  }

  public Connection acquire0() {
    lock.lock();
    try {
      while (true) {
        final Connection connection = connectionsQueue.poll();
        if (Objects.isNull(connection) && connectionsTotal.get() >= maxConnections) {
          idleConnectionsAvailable.await();
          continue;
        }
        if (Objects.isNull(connection) && connectionsTotal.get() < maxConnections) {
          return createNewConnection();
        }
        if (Objects.isNull(connection)) {
          continue;
        }
        if (!connection.isValid()) {
          updateIdleAndTotalConnections(
              idleConnectionsTotal.updateAndGet(this::decrement),
              connectionsTotal.updateAndGet(this::decrement));
          connection.channel().close();
          continue;
        }
        updateIdleConnections(idleConnectionsTotal.updateAndGet(this::decrement));
        postAcquire(connection);
        connection.cached();
        return connection;
      }
    } catch (InterruptedException exc) {
      throw new ConnectionException("Interrupted while waiting for a connection");
    } finally {
      lock.unlock();
    }
  }
}
