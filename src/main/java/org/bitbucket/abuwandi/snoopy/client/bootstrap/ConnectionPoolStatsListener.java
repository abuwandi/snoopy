package org.bitbucket.abuwandi.snoopy.client.bootstrap;

/** The interface Connection pool stats listener. */
public interface ConnectionPoolStatsListener {

  /** The constant DO_NOTHING_STATS_LISTENER. */
  ConnectionPoolStatsListener DO_NOTHING_STATS_LISTENER = state -> {};

  /**
   * Stats.
   *
   * @param stats the stats
   */
  void stats(ConnectionPoolStats stats);
}
