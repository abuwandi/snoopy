package org.bitbucket.abuwandi.snoopy.client.bootstrap;

import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.channel.Channel;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.WriteBufferWaterMark;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.reactivex.rxjava3.core.Single;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.net.URI;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Stream;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.bitbucket.abuwandi.snoopy.client.SnoopyConfig;
import org.bitbucket.abuwandi.snoopy.model.Scheme;

/** The type Snoopy bootstrap provider. */
public class SnoopyBootstrapProvider implements BootstrapProvider {

  private final SnoopyConfig config;
  private final ExecutorService workerExecutor;

  /**
   * Instantiates a new Snoopy bootstrap provider.
   *
   * @param config the config
   */
  @Builder
  SnoopyBootstrapProvider(final SnoopyConfig config) {
    this.config = config;
    this.workerExecutor =
        Executors.newFixedThreadPool(
            this.config.getWorkerExecutorThreads() > 0
                ? this.config.getWorkerExecutorThreads()
                : Runtime.getRuntime().availableProcessors());
  }

  @Override
  public void shutdown() {}

  private EventLoopGroupConfig createEventLoopGroup(final int workerThreads) {

    EventLoopGroupConfig eventLoopGroupConfig =
        createEventLoopGroup(
            "io.netty.channel.epoll.Epoll",
            "io.netty.channel.epoll.EpollEventLoopGroup",
            "io.netty.channel.epoll.EpollSocketChannel",
            "io.netty.channel.epoll.EpollChannelOption",
            workerThreads,
            Option.builder()
                .name("EPOLL_MODE")
                .value("EDGE_TRIGGERED")
                .valueEnumClassName("io.netty.channel.epoll.EpollMode")
                .build(),
            Option.builder().name("TCP_QUICKACK").value(true).build(),
            // Option.builder().name("TCP_FASTOPEN_CONNECT").value(true).build(),
            // Option.builder().name("SO_REUSEADDR").value(true).build(),
            // Option.builder().name("TCP_FASTOPEN").value(1).build(),
            // Option.builder().name("SO_REUSEPORT").value(true).build(),
            // Option.builder().name("SO_BACKLOG").value(100).build(),
            Option.builder().name("TCP_CORK").value(true).build());
    if (Objects.nonNull(eventLoopGroupConfig)) {
      return eventLoopGroupConfig;
    }

    eventLoopGroupConfig =
        createEventLoopGroup(
            "io.netty.channel.kqueue.KQueue",
            "io.netty.channel.kqueue.KQueueEventLoopGroup",
            "io.netty.channel.kqueue.KQueueSocketChannel",
            "io.netty.channel.kqueue.KQueueChannelOption",
            workerThreads,
            Option.builder().name("TCP_NOPUSH").value(false).build(),
            // Option.builder().name("SO_REUSEADDR").value(true).build(),
            Option.builder().name("RCV_ALLOC_TRANSPORT_PROVIDES_GUESS").value(false).build());
    if (Objects.nonNull(eventLoopGroupConfig)) {
      return eventLoopGroupConfig;
    }
    return EventLoopGroupConfig.builder()
        .eventLoopGroup(new NioEventLoopGroup(workerThreads, workerExecutor))
        .channelClass(NioSocketChannel.class)
        .options(Collections.emptyList())
        .build();
  }

  private EventLoopGroupConfig createEventLoopGroup(
      final String mainClassName,
      final String eventLoopGroupClassName,
      final String channelClassName,
      final String optionClassName,
      final int workerThreads,
      final Option<Object>... options) {
    try {
      final Class<?> clazz = Class.forName(mainClassName);
      final Method method = clazz.getMethod("isAvailable", (Class<?>[]) null);
      final Boolean result = (Boolean) method.invoke(null, (Object[]) null);
      if (!result) {
        return null;
      }
      final Class<? extends Channel> channelClass =
          (Class<? extends Channel>) Class.forName(channelClassName);
      final List<Option<Object>> optionList = new LinkedList<>();
      if (ArrayUtils.isNotEmpty(options)) {
        final Class<?> optionClass = Class.forName(optionClassName);
        Stream.of(options)
            .map(
                option -> {
                  if (StringUtils.isNoneBlank(option.getValueEnumClassName())) {
                    try {
                      final Class<Enum> valueEnumClass =
                          (Class<Enum>) Class.forName(option.getValueEnumClassName());
                      return Option.builder()
                          .name(option.getName())
                          .option(ChannelOption.valueOf(optionClass, option.getName()))
                          .value(Enum.valueOf(valueEnumClass, option.getValue().toString()))
                          .build();
                    } catch (ClassNotFoundException e) {
                      throw new IllegalStateException(e);
                    }
                  }
                  option.setOption(ChannelOption.valueOf(optionClass, option.getName()));
                  return option;
                })
            .collect(() -> optionList, List::add, (list1, list2) -> {});
      }
      final Class<?> eventLoopGroupType = Class.forName(eventLoopGroupClassName);
      final Constructor<?> constructor =
          eventLoopGroupType.getConstructor(Integer.TYPE, Executor.class);
      return EventLoopGroupConfig.builder()
          .eventLoopGroup((EventLoopGroup) constructor.newInstance(workerThreads, workerExecutor))
          .channelClass(channelClass)
          .options(optionList)
          .build();
    } catch (Exception exc) {
      // exc.printStackTrace();
      return null;
    }
  }

  @Override
  public Single<Bootstrap> bootstrap(URI uri) {
    final String hostname = uri.getHost();
    final Scheme scheme = Scheme.forValue(uri.getScheme());
    final int port = uri.getPort() == -1 ? scheme.port() : uri.getPort();
    final int workerThreads =
        this.config.getWorkerExecutorThreads() > 0
            ? this.config.getWorkerExecutorThreads()
            : Runtime.getRuntime().availableProcessors();
    final EventLoopGroupConfig eventLoopGroupConfig = createEventLoopGroup(workerThreads);
    final Bootstrap bootstrap =
        new Bootstrap()
            .remoteAddress(hostname, port)
            .group(eventLoopGroupConfig.getEventLoopGroup())
            .channel(eventLoopGroupConfig.getChannelClass())
            // .option(ChannelOption.AUTO_READ, false)
            .option(ChannelOption.ALLOCATOR, PooledByteBufAllocator.DEFAULT)
            .option(ChannelOption.SO_KEEPALIVE, true)
            .option(ChannelOption.TCP_NODELAY, this.config.isTcpNoDelay())
            .option(ChannelOption.SO_SNDBUF, (int) this.config.getSendBufferSize())
            .option(ChannelOption.SO_RCVBUF, (int) this.config.getReceiveBufferSize())
            .option(
                ChannelOption.CONNECT_TIMEOUT_MILLIS,
                (int)
                    this.config
                        .getConnectionTimeoutUnit()
                        .toMillis(this.config.getConnectionTimeout()))
            .option(
                ChannelOption.WRITE_BUFFER_WATER_MARK,
                new WriteBufferWaterMark(
                    (int) this.config.getWriteBufferLowWaterMark(),
                    (int) this.config.getWriteBufferHighWaterMark()));
    eventLoopGroupConfig
        .getOptions()
        .forEach(option -> bootstrap.option(option.getOption(), option.getValue()));
    return Single.just(bootstrap);
  }

  @Builder
  @Getter
  @AllArgsConstructor
  private static class EventLoopGroupConfig {

    private final EventLoopGroup eventLoopGroup;
    private final Class<? extends Channel> channelClass;
    private final List<Option<Object>> options;
  }

  @Builder
  @Getter
  @Setter
  @AllArgsConstructor
  private static class Option<T> {

    private final String name;
    private ChannelOption<T> option;
    private final String valueEnumClassName;
    private T value;
  }
}
