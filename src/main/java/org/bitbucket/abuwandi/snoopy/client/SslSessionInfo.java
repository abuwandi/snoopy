package org.bitbucket.abuwandi.snoopy.client;

import javax.security.cert.X509Certificate;
import lombok.Builder;
import lombok.Getter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/** The type Ssl session info. */
@Getter
public class SslSessionInfo {

  private final X509Certificate[] certificates;
  private final String protocol;
  private final String host;
  private final int port;
  private final String cipherSuite;

  /**
   * Instantiates a new Ssl session info.
   *
   * @param certificates the certificates
   * @param protocol the protocol
   * @param host the host
   * @param port the port
   * @param cipherSuite the cipher suite
   */
  @Builder
  public SslSessionInfo(
      final X509Certificate[] certificates,
      final String protocol,
      final String host,
      final int port,
      final String cipherSuite) {
    this.certificates = certificates;
    this.protocol = protocol;
    this.host = host;
    this.port = port;
    this.cipherSuite = cipherSuite;
  }

  @Override
  public boolean equals(Object other) {
    SslSessionInfo that;
    return other == this
        || (other instanceof SslSessionInfo
            && new EqualsBuilder()
                .append(port, (that = (SslSessionInfo) other).port)
                .append(certificates, that.certificates)
                .append(protocol, that.protocol)
                .append(host, that.host)
                .append(cipherSuite, that.cipherSuite)
                .isEquals());
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder(17, 37)
        .append(certificates)
        .append(protocol)
        .append(host)
        .append(port)
        .append(cipherSuite)
        .toHashCode();
  }

  @Override
  public String toString() {
    return new ToStringBuilder(this)
        .append("certificates", certificates)
        .append("protocol", protocol)
        .append("host", host)
        .append("port", port)
        .append("cipherSuite", cipherSuite)
        .toString();
  }
}
