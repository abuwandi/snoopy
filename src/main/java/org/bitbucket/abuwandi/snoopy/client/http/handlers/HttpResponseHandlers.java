package org.bitbucket.abuwandi.snoopy.client.http.handlers;

/** The type Http response handlers. */
public class HttpResponseHandlers {

  private HttpResponseHandlers() {
    throw new AssertionError("HttpResponseHandlers can not be instantiated.");
  }
}
