package org.bitbucket.abuwandi.snoopy.client;

import org.bitbucket.abuwandi.snoopy.client.bootstrap.Connection;

/**
 * The interface Snoopy response.
 *
 * @param <T> the type parameter
 */
public interface SnoopyResponse<T> {

  /**
   * On next boolean.
   *
   * @param message the message
   * @return the boolean
   */
  boolean onNext(T message);

  /**
   * On error boolean.
   *
   * @param throwable the throwable
   * @return the boolean
   */
  boolean onError(Throwable throwable);

  /**
   * Release boolean.
   *
   * @param reason the reason
   * @return the boolean
   */
  boolean release(String reason);

  /**
   * Is released boolean.
   *
   * @return the boolean
   */
  boolean isReleased();

  /**
   * Reference string.
   *
   * @return the string
   */
  String reference();

  /**
   * Request snoopy request.
   *
   * @return the snoopy request
   */
  SnoopyRequest<T> request();

  /**
   * Connection connection.
   *
   * @return the connection
   */
  Connection connection();

  /**
   * Sets status.
   *
   * @param status the status
   */
  void setStatus(Status status);

  /**
   * Sets throughput.
   *
   * @param throughput the throughput
   */
  void setThroughput(long throughput);
}
