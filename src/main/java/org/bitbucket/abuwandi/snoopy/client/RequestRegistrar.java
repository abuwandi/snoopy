package org.bitbucket.abuwandi.snoopy.client;

/**
 * The interface Request registrar.
 *
 * @param <U> the type parameter
 * @param <T> the type parameter
 */
public interface RequestRegistrar<U, T extends SnoopyRequest<U>> extends Registrar<T> {}
