package org.bitbucket.abuwandi.snoopy.client;

/** The interface Progress callback. */
public interface ProgressCallback {

  /** The constant DO_NOTHING_PROGRESS_CALLBACK. */
  ProgressCallback DO_NOTHING_PROGRESS_CALLBACK = progress -> {};

  /**
   * On progress.
   *
   * @param progress the progress
   */
  void onProgress(long progress);
}
