package org.bitbucket.abuwandi.snoopy.client.bootstrap;

/** The enum Connection pool mode. */
public enum ConnectionPoolMode {
  /** Fixed connection pool mode. */
  FIXED,
  /** Cached connection pool mode. */
  CACHED,
  /** Single connection pool mode. */
  SINGLE,
  /** Custom connection pool mode. */
  CUSTOM;
}
