package org.bitbucket.abuwandi.snoopy.client.bootstrap;

import io.reactivex.rxjava3.core.Single;
import java.net.URI;

/** The interface Connection provider. */
public interface ConnectionProvider {

  /**
   * Connect single.
   *
   * @param uri the uri
   * @return the single
   */
  Single<Connection> connect(URI uri);

  /**
   * Release.
   *
   * @param connection the connection
   */
  void release(Connection connection);

  /** Shutdown. */
  void shutdown();
}
