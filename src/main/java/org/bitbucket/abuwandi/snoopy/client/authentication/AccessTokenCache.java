package org.bitbucket.abuwandi.snoopy.client.authentication;

import io.reactivex.rxjava3.core.Single;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import lombok.Builder;
import org.bitbucket.abuwandi.snoopy.model.Token;

/** The type Access token cache. */
public class AccessTokenCache implements TokenProvider<Authentication, Token> {

  private final ConcurrentHashMap<Authentication, Token> tokenCache;
  private final TokenProvider<Authentication, Token> tokenProvider;
  private final Lock lock;

  /**
   * Instantiates a new Access token cache.
   *
   * @param tokenProvider the token provider
   */
  @Builder
  public AccessTokenCache(TokenProvider<Authentication, Token> tokenProvider) {
    this.tokenProvider = tokenProvider;
    this.lock = new ReentrantLock();
    this.tokenCache = new ConcurrentHashMap<>();
  }

  @Override
  public Single<Token> token(Authentication authentication) {
    if (Objects.isNull(authentication)) {
      return Single.error(new AuthenticationException("Authentication is null"));
    }
    Token accessToken = tokenCache.get(authentication);
    if (Objects.isNull(accessToken) || !accessToken.isValid()) {
      lock.lock();
      try {
        accessToken =
            tokenCache.computeIfAbsent(
                authentication, auth -> tokenProvider.token(authentication).blockingGet());
      } catch (Exception exc) {
        return Single.error(new AuthenticationException(exc));
      } catch (Throwable throwable) {
        return Single.error(throwable);
      } finally {
        lock.unlock();
      }
    }
    return Single.just(accessToken);
  }
}
