package org.bitbucket.abuwandi.snoopy.client.http;

import io.netty.channel.Channel;
import io.netty.handler.codec.http.HttpClientCodec;
import io.netty.handler.codec.http.HttpObject;
import io.netty.handler.ssl.ApplicationProtocolConfig;
import io.netty.handler.ssl.ApplicationProtocolConfig.Protocol;
import io.netty.handler.ssl.ApplicationProtocolConfig.SelectedListenerFailureBehavior;
import io.netty.handler.ssl.ApplicationProtocolConfig.SelectorFailureBehavior;
import io.netty.handler.ssl.ApplicationProtocolNames;
import io.netty.handler.ssl.OpenSsl;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.SslHandler;
import io.netty.handler.ssl.SslProvider;
import io.netty.handler.ssl.util.InsecureTrustManagerFactory;
import java.net.URI;
import java.util.concurrent.TimeUnit;
import lombok.Builder;
import org.bitbucket.abuwandi.snoopy.client.ChannelInitializer;
import org.bitbucket.abuwandi.snoopy.client.handlers.SnoopyInboundMessageHandler;
import org.bitbucket.abuwandi.snoopy.client.handlers.SnoopyInboundMessageTimeoutHandler;
import org.bitbucket.abuwandi.snoopy.client.handlers.SnoopyInboundTrafficHandler;
import org.bitbucket.abuwandi.snoopy.client.handlers.SnoopySslHandler;
import org.bitbucket.abuwandi.snoopy.client.http.handlers.SnoopyHttpRequestOutboundHandler;
import org.bitbucket.abuwandi.snoopy.model.Scheme;

/** The type Snoopy http 11 channel initializer. */
public class SnoopyHttp11ChannelInitializer implements ChannelInitializer {

  /** The constant SNOOPY_RESPONSE_HANDLER. */
  protected static final String SNOOPY_RESPONSE_HANDLER = "snoopy-response-handler";
  /** The constant SNOOPY_REQUEST_HANDLER. */
  protected static final String SNOOPY_REQUEST_HANDLER = "snoopy-request-handler";

  /** The constant SNOOPY_SSL_HANDLER. */
  protected static final String SNOOPY_SSL_HANDLER = "snoopy-ssl-handler";
  /** The constant HTTP_HANDLER. */
  protected static final String HTTP_HANDLER = "http-handler";
  /** The constant SSL_HANDLER. */
  protected static final String SSL_HANDLER = "ssl-handler";
  /** The constant PROTOCOL_NEGOTIATION_HANDLER. */
  protected static final String PROTOCOL_NEGOTIATION_HANDLER = "protocol-handler";
  /** The constant READ_TIMEOUT_HANDLER. */
  protected static final String READ_TIMEOUT_HANDLER = "read-timeout-handler";
  /** The constant TRAFFIC_COUNTER. */
  protected static final String TRAFFIC_COUNTER = "traffic-handler";

  private final long readTimeOut;
  private final TimeUnit readTimeOutUnit;
  private final long idleTime;
  private final TimeUnit idleTimeUnit;
  private final int maxInitialLineSize;
  private final int maxChunkSize;
  private final int maxHeaderSize;
  private final boolean failOnMissingResponse;
  private final boolean validateHeaders;
  private final long trafficCheckInterval;
  private final TimeUnit trafficCheckIntervalUnit;
  private final boolean insecure;

  /**
   * Instantiates a new Snoopy http 11 channel initializer.
   *
   * @param readTimeOut the read time out
   * @param readTimeOutUnit the read time out unit
   * @param idleTime the idle time
   * @param idleTimeUnit the idle time unit
   * @param maxInitialLineSize the max initial line size
   * @param maxHeaderSize the max header size
   * @param maxChunkSize the max chunk size
   * @param failOnMissingResponse the fail on missing response
   * @param validateHeaders the validate headers
   * @param trafficCheckInterval the traffic check interval
   * @param trafficCheckIntervalUnit the traffic check interval unit
   * @param insecure the insecure
   */
  @Builder
  protected SnoopyHttp11ChannelInitializer(
      final long readTimeOut,
      final TimeUnit readTimeOutUnit,
      final long idleTime,
      final TimeUnit idleTimeUnit,
      final int maxInitialLineSize,
      final int maxHeaderSize,
      final int maxChunkSize,
      final boolean failOnMissingResponse,
      final boolean validateHeaders,
      final long trafficCheckInterval,
      final TimeUnit trafficCheckIntervalUnit,
      final boolean insecure) {
    this.readTimeOut = readTimeOut;
    this.readTimeOutUnit = readTimeOutUnit;
    this.idleTime = idleTime;
    this.idleTimeUnit = idleTimeUnit;
    this.maxInitialLineSize = maxInitialLineSize;
    this.maxChunkSize = maxChunkSize;
    this.maxHeaderSize = maxHeaderSize;
    this.failOnMissingResponse = failOnMissingResponse;
    this.validateHeaders = validateHeaders;
    this.trafficCheckInterval = trafficCheckInterval;
    this.trafficCheckIntervalUnit = trafficCheckIntervalUnit;
    this.insecure = insecure;
  }

  public void initialize(final URI uri, final Channel channel) {
    try {
      final Scheme scheme = Scheme.forValue(uri.getScheme());
      final int port = uri.getPort() == -1 ? scheme.port() : uri.getPort();
      if (scheme.isSecure()) {
        final SslContext sslContext =
            SslContextBuilder.forClient()
                .sslProvider(OpenSsl.isAvailable() ? SslProvider.OPENSSL : SslProvider.JDK)
                .trustManager(insecure ? InsecureTrustManagerFactory.INSTANCE : null)
                .applicationProtocolConfig(
                    new ApplicationProtocolConfig(
                        Protocol.ALPN,
                        SelectorFailureBehavior.NO_ADVERTISE,
                        SelectedListenerFailureBehavior.ACCEPT,
                        ApplicationProtocolNames.HTTP_1_1))
                .build();
        channel.pipeline().addFirst(SNOOPY_SSL_HANDLER, new SnoopySslHandler<HttpObject>());
        final SslHandler sslHandler = sslContext.newHandler(channel.alloc(), uri.getHost(), port);
        channel.pipeline().addFirst(SSL_HANDLER, sslHandler);
      }
    } catch (Exception exc) {
      throw new IllegalStateException(exc);
    }
  }

  public void installHandlers(final Channel channel) {
    channel
        .pipeline()
        .addFirst(
            TRAFFIC_COUNTER,
            SnoopyInboundTrafficHandler.builder()
                .checkInterval(this.trafficCheckInterval)
                .checkIntervalUnit(this.trafficCheckIntervalUnit)
                .build());
    channel
        .pipeline()
        .addLast(
            READ_TIMEOUT_HANDLER,
            SnoopyInboundMessageTimeoutHandler.<HttpObject>builder()
                .readTimeout(readTimeOut)
                .readTimeoutUnit(readTimeOutUnit)
                .idleTime(idleTime)
                .idleTimeUnit(idleTimeUnit)
                .build());
    channel
        .pipeline()
        .addLast(
            HTTP_HANDLER,
            new HttpClientCodec(
                maxInitialLineSize,
                maxHeaderSize,
                maxChunkSize,
                failOnMissingResponse,
                validateHeaders));
    channel
        .pipeline()
        .addLast(SNOOPY_RESPONSE_HANDLER, new SnoopyInboundMessageHandler<>(HttpObject.class));
    channel.pipeline().addLast(SNOOPY_REQUEST_HANDLER, new SnoopyHttpRequestOutboundHandler());
  }

  public void uninstallHandlers(final Channel channel) {
    channel.pipeline().remove(TRAFFIC_COUNTER);
    channel.pipeline().remove(READ_TIMEOUT_HANDLER);
    channel.pipeline().remove(HTTP_HANDLER);
    channel.pipeline().remove(SNOOPY_RESPONSE_HANDLER);
    channel.pipeline().remove(SNOOPY_REQUEST_HANDLER);
  }

  @Override
  public void shutdown() {}
}
