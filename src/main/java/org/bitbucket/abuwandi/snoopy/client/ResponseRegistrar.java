package org.bitbucket.abuwandi.snoopy.client;

/**
 * The interface Response registrar.
 *
 * @param <T> the type parameter
 */
public interface ResponseRegistrar<T> extends Registrar<SnoopyResponse<T>> {}
