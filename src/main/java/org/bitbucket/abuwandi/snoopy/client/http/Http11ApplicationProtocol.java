package org.bitbucket.abuwandi.snoopy.client.http;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.reactivex.rxjava3.core.Single;
import java.net.URI;
import java.util.concurrent.TimeUnit;
import lombok.Builder;
import org.bitbucket.abuwandi.snoopy.client.ApplicationProtocol;
import org.bitbucket.abuwandi.snoopy.client.ChannelInitializer;
import org.bitbucket.abuwandi.snoopy.client.SnoopyConfig;
import org.bitbucket.abuwandi.snoopy.client.bootstrap.BootstrapProvider;
import org.bitbucket.abuwandi.snoopy.client.bootstrap.Connection;
import org.bitbucket.abuwandi.snoopy.client.bootstrap.ConnectionProvider;
import org.bitbucket.abuwandi.snoopy.client.bootstrap.SnoopyBootstrapProvider;
import org.bitbucket.abuwandi.snoopy.client.bootstrap.SnoopyConnectionPool;
import org.bitbucket.abuwandi.snoopy.util.ByteUnit;

/** The type Http 11 application protocol. */
public class Http11ApplicationProtocol implements ApplicationProtocol {

  private final BootstrapProvider bootstrapProvider;
  private final ConnectionProvider connectionProvider;
  private final ChannelInitializer channelInitializer;

  /**
   * Instantiates a new Http 11 application protocol.
   *
   * @param config the config
   */
  @Builder
  public Http11ApplicationProtocol(final SnoopyConfig config) {
    this.bootstrapProvider = SnoopyBootstrapProvider.builder().config(config).build();
    this.channelInitializer =
        SnoopyHttp11ChannelInitializer.builder()
            .readTimeOut(config.getReadTimeout())
            .readTimeOutUnit(config.getReadTimeoutUnit())
            .idleTime(config.getIdleTimeout())
            .idleTimeUnit(config.getIdleTimeoutUnit())
            .maxChunkSize((int) ByteUnit.KIB.toBytes(8))
            .maxInitialLineSize((int) ByteUnit.KIB.toBytes(4))
            .maxHeaderSize((int) ByteUnit.KIB.toBytes(8))
            .failOnMissingResponse(true)
            .validateHeaders(false)
            .trafficCheckInterval(2)
            .trafficCheckIntervalUnit(TimeUnit.SECONDS)
            .insecure(config.isInsecure())
            .build();
    this.connectionProvider =
        SnoopyConnectionPool.builder()
            .bootstrapProvider(this)
            .channelInitializer(this)
            .mode(config.getConnectionPoolMode())
            .statsListener(config.getConnectionPoolStatsListener())
            .maximumConnections(config.getMaximumHostConnections())
            .maxIdleConnections(config.getMaximumHostIdleConnections())
            .maxIdleConnectionDuration(
                config
                    .getMaximumConnectionIdleDurationUnit()
                    .toNanos(config.getMaximumConnectionIdleDuration()))
            .build();
  }

  @Override
  public Single<Bootstrap> bootstrap(URI uri) {
    return bootstrapProvider.bootstrap(uri);
  }

  @Override
  public void installHandlers(Channel channel) {
    channelInitializer.installHandlers(channel);
  }

  @Override
  public void uninstallHandlers(Channel channel) {
    channelInitializer.uninstallHandlers(channel);
  }

  @Override
  public void initialize(URI uri, Channel channel) {
    channelInitializer.initialize(uri, channel);
  }

  @Override
  public Single<Connection> connect(URI uri) {
    return connectionProvider.connect(uri);
  }

  @Override
  public void release(Connection connection) {
    connectionProvider.release(connection);
  }

  @Override
  public void shutdown() {
    channelInitializer.shutdown();
    connectionProvider.shutdown();
    bootstrapProvider.shutdown();
  }
}
