package org.bitbucket.abuwandi.snoopy.client.http;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.handler.codec.http.DefaultHttpContent;
import io.netty.handler.codec.http.HttpContent;
import io.netty.handler.codec.http.LastHttpContent;
import io.netty.util.ReferenceCountUtil;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.disposables.Disposable;
import java.util.Objects;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import lombok.Builder;
import org.bitbucket.abuwandi.snoopy.util.BufferSupplier;

/** The type Byte buf request body. */
public class ByteBufRequestBody implements RequestBodyPart {

  private final CharSequence contentType;
  private final long contentLength;
  private final CharSequence name;
  private final CharSequence contentDisposition;
  private final Flowable<HttpContent> byteBufFlowable;
  private final Queue<HttpContent> contentQueue;
  /** The Pause. */
  protected final AtomicBoolean pause;

  private final AtomicReference<Throwable> error;
  private final Disposable contentQueueDisposable;
  private final AtomicLong consumedBytesTotal;

  protected final AtomicBoolean completed;

  /**
   * Instantiates a new Byte buf request body.
   *
   * @param contentType the content type
   * @param name the name
   * @param contentDisposition the content disposition
   * @param contentLength the content length
   * @param byteBufFlowable the byte buf flowable
   */
  @Builder
  public ByteBufRequestBody(
      final CharSequence contentType,
      final CharSequence name,
      final CharSequence contentDisposition,
      final long contentLength,
      final Flowable<HttpContent> byteBufFlowable) {
    this.contentType = contentType;
    this.name = name;
    this.contentDisposition = contentDisposition;
    this.contentLength = contentLength;
    this.byteBufFlowable = byteBufFlowable;
    this.contentQueue = new ConcurrentLinkedQueue<>();
    this.pause = new AtomicBoolean(false);
    this.error = new AtomicReference<>(null);
    this.consumedBytesTotal = new AtomicLong(0);
    this.contentQueueDisposable =
        byteBufFlowable.subscribe(
            content -> {
              final int bytesCount = content.content().readableBytes();
              if (!contentQueue.offer(content)) {
                ReferenceCountUtil.safeRelease(content);
                throw new HttpRequestException("Failed to queue response content");
              }
              consumedBytesTotal.addAndGet(bytesCount);
            },
            throwable -> error.compareAndSet(null, throwable));
    this.completed = new AtomicBoolean(false);
  }

  @Override
  public HttpContent next(BufferSupplier<ByteBuf> supplier) {
    if (Objects.nonNull(error.get())) {
      throw new HttpRequestException(error.get());
    }
    final HttpContent content = contentQueue.poll();
    if (Objects.nonNull(content)) {
      consumedBytesTotal.addAndGet(-content.content().readableBytes());
      if (content instanceof LastHttpContent && content.content().readableBytes() > 0) {
        return new DefaultHttpContent(content.content());
      } else if (content instanceof LastHttpContent) {
        ReferenceCountUtil.safeRelease(content);
        return new DefaultHttpContent(Unpooled.EMPTY_BUFFER);
      }
      return content;
    }
    if (!this.contentQueueDisposable.isDisposed()) {
      return new DefaultHttpContent(Unpooled.EMPTY_BUFFER);
    }
    return null;
  }

  @Override
  public CharSequence contentType() {
    return contentType;
  }

  @Override
  public long contentLength() {
    return contentLength;
  }

  @Override
  public CharSequence name() {
    return name;
  }

  @Override
  public CharSequence contentDisposition() {
    return contentDisposition;
  }

  @Override
  public boolean pause() {
    return pause.compareAndSet(false, true);
  }

  @Override
  public boolean resume() {
    return canResume() && pause.compareAndSet(true, false);
  }

  @Override
  public boolean isPaused() {
    return pause.get();
  }

  @Override
  public boolean canResume() {
    return !isCompleted();
  }

  @Override
  public boolean complete() {
    return this.completed.compareAndSet(false, true);
  }

  @Override
  public boolean isCompleted() {
    return completed.get();
  }

  /**
   * The entry point of application.
   *
   * @param args the input arguments
   */
  public static void main(String[] args) {}
}
