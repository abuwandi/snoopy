package org.bitbucket.abuwandi.snoopy.client.http.handlers;

import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;
import io.netty.handler.codec.http.HttpContent;
import io.netty.handler.codec.http.HttpObject;
import io.netty.handler.codec.http.LastHttpContent;
import io.netty.util.ReferenceCountUtil;
import io.reactivex.rxjava3.internal.subscribers.BoundedSubscriber;
import java.util.LinkedList;
import java.util.Objects;
import java.util.Optional;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import lombok.Builder;
import org.bitbucket.abuwandi.snoopy.client.ProgressCallback;
import org.bitbucket.abuwandi.snoopy.client.RequestRegistrar;
import org.bitbucket.abuwandi.snoopy.client.http.HttpRequestException;
import org.bitbucket.abuwandi.snoopy.client.http.SnoopyHttpRequest;

/** The type Snoopy http request outbound handler. */
public class SnoopyHttpRequestOutboundHandler extends ChannelDuplexHandler
    implements RequestRegistrar<HttpObject, SnoopyHttpRequest> {

  private final Queue<PendingWrite> pendingWriteQueue;
  private final AtomicReference<SnoopyHttpRequest> atomicRequest;
  private final AtomicInteger pendingWriteCount;
  private final AtomicLong pendingWriteSize;

  /** Instantiates a new Snoopy http request outbound handler. */
  public SnoopyHttpRequestOutboundHandler() {
    this.pendingWriteQueue = new LinkedList<>(); // new ConcurrentLinkedQueue<>();
    this.atomicRequest = new AtomicReference<>(null);
    this.pendingWriteCount = new AtomicInteger(0);
    this.pendingWriteSize = new AtomicLong(0);
  }

  @Override
  public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise)
      throws Exception {
    if (msg instanceof SnoopyHttpRequest) {
      final SnoopyHttpRequest request = (SnoopyHttpRequest) msg;
      if (!request.method().hasRequestBody()) {
        super.write(ctx, request.nativeRequest(), promise);
        return;
      }
      final SnoopyHttpRequest resumed = atomicRequest.get();
      if (resumed != msg) {
        final Exception error =
            new IllegalStateException("Resumed request is not equal to the passed request");
        resumed.onError(error);
        ((SnoopyHttpRequest) msg).onError(error);
        if (Objects.nonNull(promise)) {
          promise.setFailure(error);
        }
        throw error;
      }
      if (!writePending(ctx)) {
        request.pause();
        return;
      }
      if (request.isCompleted()) {
        return;
      }
      final BoundedSubscriber<HttpObject> subscriber =
          new BoundedSubscriber<>(
              httpObject -> trySend(ctx, request, httpObject, promise),
              throwable -> {
                request.onError(throwable);
                handleThrowable(throwable);
              },
              () -> {},
              subscription -> subscription.request(5),
              5);
      request.toFlowable(ctx.alloc()).blockingSubscribe(subscriber);
      return;
    }
    super.write(ctx, msg, promise);
  }

  @Override
  public void channelWritabilityChanged(ChannelHandlerContext ctx) throws Exception {
    final SnoopyHttpRequest request = atomicRequest.get();
    if (Objects.nonNull(request)) {
      if (ctx.channel().isWritable()) {
        request.resume();
      } else {
        request.pause();
      }
    }
    super.channelWritabilityChanged(ctx);
  }

  @Override
  public boolean register(final SnoopyHttpRequest registrant) {
    return atomicRequest.compareAndSet(null, registrant);
  }

  @Override
  public boolean deregister(final SnoopyHttpRequest registrant) {
    return atomicRequest.compareAndSet(registrant, null);
  }

  @Override
  public void channelInactive(ChannelHandlerContext ctx) {
    deregisterAll();
    ctx.fireChannelInactive();
  }

  @Override
  public void channelUnregistered(ChannelHandlerContext ctx) {
    deregisterAll();
    ctx.fireChannelUnregistered();
  }

  @Override
  public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {
    deregisterAll();
    super.handlerRemoved(ctx);
  }

  @Override
  public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
    deregisterAll();
    super.exceptionCaught(ctx, cause);
  }

  @Override
  public void deregisterAll() {
    atomicRequest.set(null);
    final int pendingWritesCount = pendingWriteCount.getAndSet(0);
    pendingWriteSize.set(0);
    // Dangerous!!!
    if (pendingWritesCount > 0) {
      PendingWrite pendingWrite;
      while ((pendingWrite = pendingWriteQueue.poll()) != null) {
        ReferenceCountUtil.safeRelease(pendingWrite.httpObject);
      }
    }
  }

  private void handleThrowable(final Throwable throwable) {
    if (throwable instanceof Error) {
      throw (Error) throwable;
    }
    throw new HttpRequestException(throwable);
  }

  private void enqueue(
      final SnoopyHttpRequest request, final HttpObject httpObject, final ChannelPromise promise) {
    if (!pendingWriteQueue.offer(
        PendingWrite.builder()
            .httpObject(httpObject)
            .successPromise(httpObject instanceof LastHttpContent ? promise : null)
            .errorPromise(promise)
            .progressCallback(request::onProgress)
            .build())) {
      throw new IllegalStateException("Cannot enqueue http object");
    }
    pendingWriteSize.addAndGet(
        httpObject instanceof HttpContent
            ? ((HttpContent) httpObject).content().readableBytes()
            : 0);
    pendingWriteCount.incrementAndGet();
  }

  private void trySend(
      final ChannelHandlerContext ctx,
      final SnoopyHttpRequest request,
      final HttpObject httpObject,
      final ChannelPromise promise) {
    writePending(ctx);
    if (!ctx.channel().isWritable()) {
      request.pause();
      enqueue(request, httpObject, promise);
      ctx.flush();
      return;
    }
    write(
        ctx,
        httpObject,
        httpObject instanceof LastHttpContent ? promise : null,
        promise,
        request::onProgress);
  }

  private boolean writePending(final ChannelHandlerContext ctx) {
    PendingWrite pendingWrite;
    while (ctx.channel().isWritable() && (pendingWrite = pendingWriteQueue.poll()) != null) {
      pendingWriteCount.decrementAndGet();
      pendingWriteSize.addAndGet(
          pendingWrite.httpObject instanceof HttpContent
              ? -((HttpContent) pendingWrite.httpObject).content().readableBytes()
              : 0);
      write(
          ctx,
          pendingWrite.httpObject,
          pendingWrite.successPromise,
          pendingWrite.errorPromise,
          pendingWrite.progressCallback);
    }
    if (!ctx.channel().isWritable()) {
      ctx.flush();
    }
    return pendingWriteCount.get() < 1;
  }

  private void write(
      final ChannelHandlerContext ctx,
      final HttpObject httpObject,
      final ChannelPromise successPromise,
      final ChannelPromise errorPromise,
      final ProgressCallback progressCallback) {
    try {
      final int bytesWritten =
          httpObject instanceof HttpContent
              ? ((HttpContent) httpObject).content().readableBytes()
              : 0;
      final ChannelFuture channelFuture =
          httpObject instanceof LastHttpContent
              ? ctx.writeAndFlush(httpObject)
              : ctx.write(httpObject);
      handleChannelFuture(
          channelFuture, successPromise, errorPromise, progressCallback, bytesWritten);
    } catch (Throwable throwable) {
      ReferenceCountUtil.safeRelease(httpObject);
      errorPromise.tryFailure(throwable);
      throw throwable;
    }
  }

  private void handleChannelFuture(
      final ChannelFuture channelFuture,
      final ChannelPromise successPromise,
      final ChannelPromise errorPromise,
      final ProgressCallback progressCallback,
      final int bytesWritten) {
    if (channelFuture.isDone()) {
      handleSuccessOrFailure(
          channelFuture, successPromise, errorPromise, progressCallback, bytesWritten);
      return;
    }
    channelFuture.addListener(
        cf ->
            handleSuccessOrFailure(
                channelFuture, successPromise, errorPromise, progressCallback, bytesWritten));
  }

  private void handleSuccessOrFailure(
      final ChannelFuture channelFuture,
      final ChannelPromise successPromise,
      final ChannelPromise errorPromise,
      final ProgressCallback progressCallback,
      final int bytesWritten) {
    if (channelFuture.isSuccess()) {
      progressCallback.onProgress(bytesWritten);
      if (Objects.nonNull(successPromise)) {
        successPromise.trySuccess();
      }
      return;
    }
    final HttpRequestException requestException =
        Optional.ofNullable(channelFuture.cause())
            .map(HttpRequestException::new)
            .orElseGet(() -> new HttpRequestException("Request write failed"));
    if (Objects.nonNull(errorPromise)) {
      errorPromise.tryFailure(requestException);
    }
  }

  private static class PendingWrite {

    private final HttpObject httpObject;
    private final ChannelPromise successPromise;
    private final ChannelPromise errorPromise;
    private final ProgressCallback progressCallback;

    /**
     * Instantiates a new Pending write.
     *
     * @param httpObject the http object
     * @param successPromise the success promise
     * @param errorPromise the error promise
     * @param progressCallback the progress callback
     */
    @Builder
    PendingWrite(
        final HttpObject httpObject,
        final ChannelPromise successPromise,
        final ChannelPromise errorPromise,
        final ProgressCallback progressCallback) {
      this.httpObject = httpObject;
      this.successPromise = successPromise;
      this.errorPromise = errorPromise;
      this.progressCallback = progressCallback;
    }
  }
}
