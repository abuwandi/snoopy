package org.bitbucket.abuwandi.snoopy.client.http;

/** The interface Request body part. */
public interface RequestBodyPart extends RequestBody {

  CharSequence contentType();

  /**
   * Name char sequence.
   *
   * @return the char sequence
   */
  CharSequence name();

  /**
   * Content disposition char sequence.
   *
   * @return the char sequence
   */
  CharSequence contentDisposition();
}
