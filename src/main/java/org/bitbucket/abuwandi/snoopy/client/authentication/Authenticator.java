package org.bitbucket.abuwandi.snoopy.client.authentication;

import io.reactivex.rxjava3.core.Single;
import org.bitbucket.abuwandi.snoopy.client.SnoopyRequest;

/**
 * The interface Authenticator.
 *
 * @param <T> the type parameter
 * @param <R> the type parameter
 */
public interface Authenticator<T, R extends SnoopyRequest<T>> {

  /**
   * Authenticate single.
   *
   * @param request the request
   * @return the single
   */
  Single<R> authenticate(R request);
}
