package org.bitbucket.abuwandi.snoopy.client.bootstrap;

import lombok.Builder;
import lombok.Getter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/** The type Connection pool stats. */
@Getter
public class ConnectionPoolStats {

  private final String name;
  private final int maxConnections;
  private final int activeConnectionsTotal;
  private final int idleConnectionsTotal;
  private final int connectionsTotal;

  /**
   * Instantiates a new Connection pool stats.
   *
   * @param name the name
   * @param maxConnections the max connections
   * @param activeConnectionsTotal the active connections total
   * @param idleConnectionsTotal the idle connections total
   * @param connectionsTotal the connections total
   */
  @Builder
  public ConnectionPoolStats(
      final String name,
      final int maxConnections,
      final int activeConnectionsTotal,
      final int idleConnectionsTotal,
      final int connectionsTotal) {
    this.name = name;
    this.maxConnections = maxConnections;
    this.activeConnectionsTotal = activeConnectionsTotal;
    this.idleConnectionsTotal = idleConnectionsTotal;
    this.connectionsTotal = connectionsTotal;
  }

  @Override
  public boolean equals(final Object obj) {
    final ConnectionPoolStats rhs;
    return obj == this
        || (obj instanceof ConnectionPoolStats
            && new EqualsBuilder()
                .append(this.maxConnections, (rhs = (ConnectionPoolStats) obj).maxConnections)
                .append(this.activeConnectionsTotal, rhs.activeConnectionsTotal)
                .append(this.idleConnectionsTotal, rhs.idleConnectionsTotal)
                .append(this.connectionsTotal, rhs.connectionsTotal)
                .append(this.name, rhs.name)
                .isEquals());
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder()
        .append(maxConnections)
        .append(activeConnectionsTotal)
        .append(idleConnectionsTotal)
        .append(connectionsTotal)
        .append(name)
        .toHashCode();
  }

  @Override
  public String toString() {
    return new ToStringBuilder(this)
        .append("name", name)
        .append("maxConnections", maxConnections)
        .append("activeConnectionsTotal", activeConnectionsTotal)
        .append("idleConnectionsTotal", idleConnectionsTotal)
        .append("connectionsTotal", connectionsTotal)
        .toString();
  }
}
