package org.bitbucket.abuwandi.snoopy.client.http;

import io.netty.handler.codec.http.HttpContent;
import io.netty.handler.codec.http.HttpHeaderNames;
import io.netty.handler.codec.http.HttpHeaderValues;
import io.netty.handler.codec.http.HttpVersion;
import io.netty.util.internal.PlatformDependent;
import java.io.File;
import java.io.InputStream;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import org.bitbucket.abuwandi.snoopy.client.ApplicationProtocol;
import org.bitbucket.abuwandi.snoopy.client.HandshakeCallback;
import org.bitbucket.abuwandi.snoopy.client.ProgressCallback;
import org.bitbucket.abuwandi.snoopy.client.StatusCallback;
import org.bitbucket.abuwandi.snoopy.client.ThroughputCallback;
import org.bitbucket.abuwandi.snoopy.client.authentication.Authentication;
import org.bitbucket.abuwandi.snoopy.client.authentication.TokenProvider;
import org.bitbucket.abuwandi.snoopy.model.Token;
import org.bitbucket.abuwandi.snoopy.util.CharSequenceSerializable;
import org.bitbucket.abuwandi.snoopy.util.JsonSerializable;
import org.bitbucket.abuwandi.snoopy.util.RetryStrategy;

/** The type Http request body builder. */
public class HttpRequestBodyBuilder extends HttpRequestBuilder {

  /** The Progress callback. */
  protected ProgressCallback progressCallback;

  /** The Throughput callback. */
  protected ThroughputCallback throughputCallback;

  /** The Body. */
  protected RequestBody body;

  /**
   * Instantiates a new Http request body builder.
   *
   * @param method the method
   */
  HttpRequestBodyBuilder(final HttpMethod method) {
    super(method);
  }

  /**
   * Body http request body builder.
   *
   * @param body the body
   * @return the http request body builder
   */
  public HttpRequestBodyBuilder body(final Object body) {
    this.body = createBodyFrom(body);
    return this;
  }

  @Override
  public HttpRequestBodyBuilder tokenProvider(TokenProvider<Authentication, Token> tokenProvider) {
    super.tokenProvider(tokenProvider);
    return this;
  }

  @Override
  public HttpRequestBodyBuilder reference(String reference) {
    super.reference(reference);
    return this;
  }

  @Override
  public HttpRequestBodyBuilder version(HttpVersion version) {
    this.version = version;
    return this;
  }

  @Override
  public HttpRequestBodyBuilder host(CharSequence host) {
    super.host(host);
    return this;
  }

  @Override
  public HttpRequestBodyBuilder closeConnection() {
    super.closeConnection();
    return this;
  }

  @Override
  public HttpRequestBodyBuilder keepConnectionAlive() {
    super.keepConnectionAlive();
    return this;
  }

  @Override
  public HttpRequestBodyBuilder uri(URI uri) {
    super.uri(uri);
    return this;
  }

  @Override
  public HttpRequestBodyBuilder header(CharSequence name, CharSequence... values) {
    super.header(name, values);
    return this;
  }

  @Override
  public HttpRequestBodyBuilder headers(Map<CharSequence, List<CharSequence>> headerMap) {
    super.headers(headerMap);
    return this;
  }

  @Override
  public HttpRequestBodyBuilder followRedirects(boolean followRedirects) {
    super.followRedirects(followRedirects);
    return this;
  }

  public HttpRequestBodyBuilder failIfNotSuccessfulResponse(
      final boolean failIfNotSuccessfulResponse) {
    this.failIfNotSuccessfulResponse = failIfNotSuccessfulResponse;
    return this;
  }

  public HttpRequestBodyBuilder responseStatusCallback(
      final StatusCallback responseStatusCallback) {
    this.responseStatusCallback = responseStatusCallback;
    return this;
  }

  public HttpRequestBodyBuilder responseThroughputCallback(
      final ThroughputCallback responseThroughputCallback) {
    this.responseThroughputCallback = responseThroughputCallback;
    return this;
  }

  public HttpRequestBodyBuilder responseProgressCallback(
      final ProgressCallback responseProgressCallback) {
    this.responseProgressCallback = responseProgressCallback;
    return this;
  }

  public HttpRequestBodyBuilder statusCallback(final StatusCallback statusCallback) {
    this.statusCallback = statusCallback;
    return this;
  }

  public HttpRequestBodyBuilder handshakeCallback(final HandshakeCallback handshakeCallback) {
    this.handshakeCallback = handshakeCallback;
    return this;
  }

  /**
   * Progress callback http request body builder.
   *
   * @param progressCallback the progress callback
   * @return the http request body builder
   */
  public HttpRequestBodyBuilder progressCallback(final ProgressCallback progressCallback) {
    this.progressCallback = progressCallback;
    return this;
  }

  /**
   * Throughput callback http request body builder.
   *
   * @param throughputCallback the throughput callback
   * @return the http request body builder
   */
  public HttpRequestBodyBuilder throughputCallback(final ThroughputCallback throughputCallback) {
    this.throughputCallback = throughputCallback;
    return this;
  }

  public HttpRequestBodyBuilder ignoreResponseBody(final boolean ignoreResponseBody) {
    this.ignoreResponseBody = ignoreResponseBody;
    return this;
  }

  @Override
  protected HttpRequestBodyBuilder redirectsDepth(int redirectsDepth) {
    super.redirectsDepth(redirectsDepth);
    return this;
  }

  public HttpRequestBodyBuilder connectionRetry(final int connectionRetry) {
    this.connectionRetry = connectionRetry;
    return this;
  }

  public HttpRequestBodyBuilder query(CharSequence name, CharSequence... values) {
    super.query(name, values);
    return this;
  }

  /**
   * Create body from request body.
   *
   * @param charSequence the char sequence
   * @return the request body
   */
  protected RequestBody createBodyFrom(final CharSequence charSequence) {
    final RequestBodyPart serializableRequestBody =
        SerializableRequestBody.builder()
            .serializable(
                CharSequenceSerializable.builder()
                    .charSequence(charSequence)
                    .charset(StandardCharsets.UTF_8)
                    .build())
            .contentType(HttpHeaderValues.TEXT_PLAIN)
            .build();
    final RequestBodyPart urlEncodedBody = createUrlEncodedBody();
    if (Objects.nonNull(urlEncodedBody)) {
      return MultipartRequestBody.builder()
          .boundary(
              "-----------------------"
                  + Long.toHexString(PlatformDependent.threadLocalRandom().nextLong()))
          .charset(StandardCharsets.UTF_8)
          .parts(urlEncodedBody, serializableRequestBody)
          .build();
    }
    return serializableRequestBody;
  }

  /**
   * Create body from request body.
   *
   * @param path the path
   * @return the request body
   */
  protected RequestBody createBodyFrom(final Path path) {
    return MultipartRequestBody.builder()
        .boundary(
            "-----------------------"
                + Long.toHexString(PlatformDependent.threadLocalRandom().nextLong()))
        .charset(StandardCharsets.UTF_8)
        .parts(createUrlEncodedBody(), FileRequestBody.builder().path(path).name("file").build())
        .build();
  }

  /**
   * Create body from request body.
   *
   * @param inputStream the input stream
   * @return the request body
   */
  protected RequestBody createBodyFrom(final InputStream inputStream) {
    return MultipartRequestBody.builder()
        .boundary(
            "-----------------------"
                + Long.toHexString(PlatformDependent.threadLocalRandom().nextLong()))
        .charset(StandardCharsets.UTF_8)
        .parts(
            createUrlEncodedBody(),
            DefaultInputStreamRequestBody.builder()
                .inputStream(inputStream)
                .name("stream-data")
                .build())
        .build();
  }

  /**
   * Create body from request body.
   *
   * @param response the response
   * @return the request body
   */
  protected RequestBody createBodyFrom(final SnoopyHttpResponse response) {
    return MultipartRequestBody.builder()
        .boundary(
            "-----------------------"
                + Long.toHexString(PlatformDependent.threadLocalRandom().nextLong()))
        .charset(StandardCharsets.UTF_8)
        .parts(
            createUrlEncodedBody(),
            ByteBufRequestBody.builder()
                .name("file")
                .contentType(response.contentType())
                .contentLength(response.contentLength())
                .contentDisposition(
                    String.format(
                        "form-data; name=\"%s\"; filename=\"%s\"", "file", response.filename()))
                .byteBufFlowable(response.bodyAs(HttpContent.class))
                .build())
        .build();
  }

  private RequestBody createNullRequestBody() {
    final RequestBodyPart urlEncodedBody = createUrlEncodedBody();
    if (Objects.nonNull(urlEncodedBody)) {
      return urlEncodedBody;
    }
    return EmptyRequestBody.builder().build();
  }

  private RequestBodyPart createUrlEncodedBody() {
    //    final URI uri = buildUri(this.uri);
    //    if (StringUtils.isNoneBlank(uri.getRawQuery())) {
    //      return SerializableRequestBody.<CharSequenceSerializable>builder()
    //          .name("query")
    //          .contentType(HttpHeaderValues.APPLICATION_X_WWW_FORM_URLENCODED)
    //          .serializable(
    //              CharSequenceSerializable.builder()
    //                  .charSequence(uri.getRawQuery())
    //                  .charset(StandardCharsets.UTF_8)
    //                  .build())
    //          .build();
    //    }
    return null;
  }

  protected HttpRequestBodyBuilder applicationProtocol(
      final ApplicationProtocol applicationProtocol) {
    this.applicationProtocol = applicationProtocol;
    return this;
  }

  /**
   * Create body from request body.
   *
   * @param body the body
   * @return the request body
   */
  protected RequestBody createBodyFrom(final Object body) {
    if (Objects.isNull(body)) {
      //      final RequestBody requestBody = createNullRequestBody();
      //      header(HttpHeaderNames.CONTENT_LENGTH, requestBody.contentLength() + "");
      //      if (StringUtils.isNoneBlank(requestBody.contentType())) {
      //        header(HttpHeaderNames.CONTENT_TYPE, requestBody.contentType());
      //      }
      //      return requestBody;
      header(HttpHeaderNames.CONTENT_LENGTH, "0");
      return null;
    }
    if (body instanceof CharSequence) {
      final RequestBody requestBody = createBodyFrom((CharSequence) body);
      header(HttpHeaderNames.CONTENT_LENGTH, requestBody.contentLength() + "");
      header(HttpHeaderNames.CONTENT_TYPE, requestBody.contentType());
      return requestBody;
    }
    if (body instanceof Path) {
      final RequestBody requestBody = createBodyFrom((Path) body);
      header(HttpHeaderNames.CONTENT_LENGTH, requestBody.contentLength() + "");
      header(HttpHeaderNames.CONTENT_TYPE, requestBody.contentType());
      return requestBody;
    }
    if (body instanceof File) {
      final RequestBody requestBody = createBodyFrom(((File) body).toPath());
      header(HttpHeaderNames.CONTENT_LENGTH, requestBody.contentLength() + "");
      header(HttpHeaderNames.CONTENT_TYPE, requestBody.contentType());
      return requestBody;
    }
    if (body instanceof URI) {
      final RequestBody requestBody = createBodyFrom(Paths.get((URI) body));
      header(HttpHeaderNames.CONTENT_LENGTH, requestBody.contentLength() + "");
      header(HttpHeaderNames.CONTENT_TYPE, requestBody.contentType());
      return requestBody;
    }
    if (body instanceof SnoopyHttpResponse) {
      final RequestBody requestBody = createBodyFrom((SnoopyHttpResponse) body);
      header(HttpHeaderNames.CONTENT_LENGTH, requestBody.contentLength() + "");
      header(HttpHeaderNames.CONTENT_TYPE, requestBody.contentType());
      return requestBody;
    }
    if (body instanceof InputStream) {
      final RequestBody requestBody = createBodyFrom((InputStream) body);
      header(HttpHeaderNames.TRANSFER_ENCODING, HttpHeaderValues.CHUNKED);
      header(HttpHeaderNames.CONTENT_TYPE, requestBody.contentType());
      return requestBody;
    }

    final RequestBody requestBody =
        SerializableRequestBody.builder()
            .serializable(
                JsonSerializable.builder().content(body).charset(StandardCharsets.UTF_8).build())
            .contentType(HttpHeaderValues.APPLICATION_JSON)
            .build();
    header(HttpHeaderNames.CONTENT_LENGTH, requestBody.contentLength() + "");
    header(HttpHeaderNames.CONTENT_TYPE, requestBody.contentType());
    return requestBody;
  }

  public SnoopyHttpRequest build() {
    //    if (Objects.isNull(body)) {
    //      body = createUrlEncodedBody();
    //    }
    return SnoopyHttpRequest.builder()
        .uri(buildUri(uri))
        .reference(reference)
        .version(version)
        .method(method)
        .headers(headers)
        .followRedirects(followRedirects)
        .redirectsDepth(redirectsDepth)
        .applicationProtocol(applicationProtocol)
        .authenticator(authenticator(tokenProvider, authentication))
        .failIfNotSuccessfulResponse(failIfNotSuccessfulResponse)
        .responseStatusCallback(responseStatusCallback)
        .responseThroughputCallback(responseThroughputCallback)
        .responseProgressCallback(responseProgressCallback)
        .statusChangeCallback(statusCallback)
        .throughputChangeCallback(throughputCallback)
        .progressCallback(progressCallback)
        .body(body)
        .handshakeCallback(handshakeCallback)
        .ignoreResponseBody(ignoreResponseBody)
        .builder(this)
        .connectionRetryStrategy(
            connectionRetry > 0
                ? RetryStrategy.builder()
                    .maxAttempts(connectionRetry)
                    .lowestDelay(100)
                    .growBy(100)
                    .highestDelay(500)
                    .unit(TimeUnit.MILLISECONDS)
                    .errorPredicate(throwable -> true)
                    .build()
                : null)
        .build();
  }
}
