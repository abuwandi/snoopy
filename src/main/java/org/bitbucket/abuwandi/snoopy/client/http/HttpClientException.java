package org.bitbucket.abuwandi.snoopy.client.http;

import org.bitbucket.abuwandi.snoopy.client.ClientException;

/** The type Http client exception. */
public class HttpClientException extends ClientException {

  /**
   * Instantiates a new Http client exception.
   *
   * @param message the message
   */
  public HttpClientException(String message) {
    super(message);
  }

  /**
   * Instantiates a new Http client exception.
   *
   * @param cause the cause
   */
  public HttpClientException(Throwable cause) {
    super(cause);
  }

  /**
   * Instantiates a new Http client exception.
   *
   * @param message the message
   * @param cause the cause
   */
  public HttpClientException(String message, Throwable cause) {
    super(message, cause);
  }
}
