package org.bitbucket.abuwandi.snoopy.client.http;

import io.netty.channel.Channel;
import io.netty.channel.ChannelPromise;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import lombok.Builder;

/** The type Delegate channel promise. */
public class DelegateChannelPromise implements ChannelPromise {

  private final ChannelPromise promiseDelegate;
  private final SnoopyHttpRequest requestDelegate;

  /**
   * Instantiates a new Delegate channel promise.
   *
   * @param promiseDelegate the promise delegate
   * @param requestDelegate the request delegate
   */
  @Builder
  public DelegateChannelPromise(
      final ChannelPromise promiseDelegate, final SnoopyHttpRequest requestDelegate) {
    this.promiseDelegate = promiseDelegate;
    this.requestDelegate = requestDelegate;
  }

  @Override
  public Channel channel() {
    return promiseDelegate.channel();
  }

  @Override
  public ChannelPromise setSuccess(Void result) {
    final ChannelPromise promise = promiseDelegate.setSuccess(result);
    requestDelegate.onSuccess();
    return promise;
  }

  @Override
  public boolean trySuccess(Void aVoid) {
    final boolean success = promiseDelegate.trySuccess(aVoid);
    if (success) {
      requestDelegate.onSuccess();
    }
    return success;
  }

  @Override
  public ChannelPromise setSuccess() {
    final ChannelPromise promise = promiseDelegate.setSuccess();
    requestDelegate.onSuccess();
    return promise;
  }

  @Override
  public boolean trySuccess() {
    final boolean success = promiseDelegate.trySuccess();
    if (success) {
      requestDelegate.onSuccess();
    }
    return success;
  }

  @Override
  public ChannelPromise setFailure(Throwable cause) {
    final ChannelPromise promise = promiseDelegate.setFailure(cause);
    requestDelegate.onError(cause);
    return promise;
  }

  @Override
  public boolean tryFailure(Throwable throwable) {
    final boolean success = promiseDelegate.tryFailure(throwable);
    if (success) {
      requestDelegate.onError(throwable);
    }
    return success;
  }

  @Override
  public boolean setUncancellable() {
    return promiseDelegate.setUncancellable();
  }

  @Override
  public boolean isSuccess() {
    return promiseDelegate.isSuccess();
  }

  @Override
  public boolean isCancellable() {
    return promiseDelegate.isCancellable();
  }

  @Override
  public Throwable cause() {
    return promiseDelegate.cause();
  }

  @Override
  public ChannelPromise addListener(
      GenericFutureListener<? extends Future<? super Void>> listener) {
    return promiseDelegate.addListener(listener);
  }

  @Override
  public ChannelPromise addListeners(
      GenericFutureListener<? extends Future<? super Void>>... listeners) {
    return promiseDelegate.addListeners(listeners);
  }

  @Override
  public ChannelPromise removeListener(
      GenericFutureListener<? extends Future<? super Void>> listener) {
    return promiseDelegate.removeListener(listener);
  }

  @Override
  public ChannelPromise removeListeners(
      GenericFutureListener<? extends Future<? super Void>>... listeners) {
    return promiseDelegate.removeListeners(listeners);
  }

  @Override
  public ChannelPromise sync() throws InterruptedException {
    return promiseDelegate.sync();
  }

  @Override
  public ChannelPromise syncUninterruptibly() {
    return promiseDelegate.syncUninterruptibly();
  }

  @Override
  public ChannelPromise await() throws InterruptedException {
    return promiseDelegate.await();
  }

  @Override
  public ChannelPromise awaitUninterruptibly() {
    return promiseDelegate.awaitUninterruptibly();
  }

  @Override
  public boolean await(long l, TimeUnit timeUnit) throws InterruptedException {
    return promiseDelegate.await(l, timeUnit);
  }

  @Override
  public boolean await(long l) throws InterruptedException {
    return promiseDelegate.await(l);
  }

  @Override
  public boolean awaitUninterruptibly(long l, TimeUnit timeUnit) {
    return promiseDelegate.awaitUninterruptibly(l, timeUnit);
  }

  @Override
  public boolean awaitUninterruptibly(long l) {
    return promiseDelegate.awaitUninterruptibly(l);
  }

  @Override
  public Void getNow() {
    return promiseDelegate.getNow();
  }

  @Override
  public boolean cancel(boolean b) {
    return promiseDelegate.cancel(b);
  }

  @Override
  public boolean isCancelled() {
    return promiseDelegate.isCancelled();
  }

  @Override
  public boolean isDone() {
    return promiseDelegate.isDone();
  }

  @Override
  public Void get() throws InterruptedException, ExecutionException {
    return promiseDelegate.get();
  }

  @Override
  public Void get(long timeout, TimeUnit unit)
      throws InterruptedException, ExecutionException, TimeoutException {
    return promiseDelegate.get(timeout, unit);
  }

  @Override
  public boolean isVoid() {
    return promiseDelegate.isVoid();
  }

  @Override
  public ChannelPromise unvoid() {
    return promiseDelegate.unvoid();
  }
}
