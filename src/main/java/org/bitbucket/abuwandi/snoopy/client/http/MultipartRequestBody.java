package org.bitbucket.abuwandi.snoopy.client.http;

import io.netty.buffer.ByteBuf;
import io.netty.handler.codec.http.DefaultHttpContent;
import io.netty.handler.codec.http.HttpContent;
import io.netty.handler.codec.http.HttpHeaderNames;
import io.netty.util.ReferenceCountUtil;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;
import lombok.Builder;
import lombok.NonNull;
import org.apache.commons.lang3.ArrayUtils;
import org.bitbucket.abuwandi.snoopy.util.BufferSupplier;

/** The type Multipart request body. */
public class MultipartRequestBody implements RequestBody {

  private final RequestBodyPart[] parts;
  private final String boundary;
  private final Charset charset;
  private final AtomicInteger index;
  private final AtomicBoolean partHeadersConsumed;
  private final AtomicBoolean completed;

  /**
   * Instantiates a new Multipart request body.
   *
   * @param boundary the boundary
   * @param charset the charset
   * @param parts the parts
   */
  @Builder
  public MultipartRequestBody(
      @NonNull final String boundary,
      @NonNull Charset charset,
      @NonNull final RequestBodyPart... parts) {
    if (ArrayUtils.isEmpty(parts)) {
      throw new HttpRequestException("Request body parts is empty");
    }
    this.boundary = boundary;
    this.charset = charset;
    this.parts = Stream.of(parts).filter(Objects::nonNull).toArray(RequestBodyPart[]::new);
    this.index = new AtomicInteger(0);
    this.partHeadersConsumed = new AtomicBoolean(false);
    this.completed = new AtomicBoolean(false);
  }

  /** The Pause. */
  protected final AtomicBoolean pause = new AtomicBoolean(false);

  @Override
  public boolean pause() {
    return pause.compareAndSet(false, true);
  }

  @Override
  public boolean resume() {
    return canResume() && pause.compareAndSet(true, false);
  }

  @Override
  public boolean canResume() {
    return !isCompleted();
  }

  @Override
  public boolean complete() {
    return this.completed.compareAndSet(false, true);
  }

  @Override
  public boolean isPaused() {
    return pause.get();
  }

  @Override
  public HttpContent next(final BufferSupplier<ByteBuf> supplier) {

    int index = this.index.get();
    HttpContent content = null;
    try {
      while (index < parts.length) {

        RequestBodyPart part = parts[index];

        if (partHeadersConsumed.compareAndSet(false, true)) {
          final byte[] bytes = partHeaders(part);
          content = new DefaultHttpContent(supplier.allocate(bytes.length).writeBytes(bytes));
          return content;
        }

        content = part.next(supplier);

        if (Objects.nonNull(content)) {
          return content;
        }

        if (!partHeadersConsumed.compareAndSet(true, false)
            || !this.index.compareAndSet(index, index += 1)) {
          throw new HttpRequestException("Broken index");
        }
      }

      if (index - parts.length == 0) {
        final byte[] endBoundary = multipartEnd();
        if (!this.index.compareAndSet(index, index += 1)) {
          throw new HttpRequestException("Broken index");
        }
        content =
            new DefaultHttpContent(supplier.allocate(endBoundary.length).writeBytes(endBoundary));
        return content;
      }

      if (index - parts.length == 1) {
        if (!this.index.compareAndSet(index, index += 1)) {
          throw new HttpRequestException("Broken index");
        }
      }
      return null;
    } catch (HttpRequestException exc) {
      ReferenceCountUtil.safeRelease(content);
      throw exc;
    } catch (Exception exc) {
      ReferenceCountUtil.safeRelease(content);
      throw new HttpRequestException(exc);
    } catch (Throwable throwable) {
      ReferenceCountUtil.safeRelease(content);
      throw throwable;
    }
  }

  private byte[] partHeaders(final RequestBodyPart part) {
    return String.format(
            "\r\n--%s\r\n%s: %s\r\n%s: %s\r\n\r\n",
            boundary,
            HttpHeaderNames.CONTENT_DISPOSITION,
            part.contentDisposition(),
            HttpHeaderNames.CONTENT_TYPE,
            part.contentType())
        .getBytes(StandardCharsets.UTF_8);
  }

  private byte[] multipartEnd() {
    return String.format("\r\n--%s--\r\n", boundary).getBytes(StandardCharsets.UTF_8);
  }

  @Override
  public long contentLength() {
    return Stream.of(parts).mapToLong(part -> partHeaders(part).length + part.contentLength()).sum()
        + multipartEnd().length;
  }

  @Override
  public CharSequence contentType() {
    return String.format("multipart/form-data; boundary=%s", boundary);
  }

  @Override
  public boolean isCompleted() {
    return completed.get();
  }

  /** The type Multipart request body builder. */
  public static class MultipartRequestBodyBuilder {
    private RequestBodyPart[] parts;

    /**
     * Parts multipart request body builder.
     *
     * @param parts the parts
     * @return the multipart request body builder
     */
    public MultipartRequestBodyBuilder parts(RequestBodyPart... parts) {
      this.parts = parts;
      return this;
    }
  }
}
