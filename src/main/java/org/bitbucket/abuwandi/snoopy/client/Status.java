package org.bitbucket.abuwandi.snoopy.client;

/** The enum Status. */
public enum Status {
  /** Pending status. */
  PENDING("Pending"),
  /** Idle status. */
  IDLE("Idle"),
  /** Active status. */
  ACTIVE("Active"),
  /** Complete status. */
  COMPLETE("Complete"),
  /** Retry status. */
  RETRY("Retry"),
  /** Failed status. */
  FAILED("Failed");

  private final String value;

  Status(final String value) {
    this.value = value;
  }

  /**
   * Value string.
   *
   * @return the string
   */
  public String value() {
    return value;
  }
}
