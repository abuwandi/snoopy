package org.bitbucket.abuwandi.snoopy.client.http;

import org.bitbucket.abuwandi.snoopy.client.ClientException;

/** The type Http request exception. */
public class HttpRequestException extends ClientException {

  /**
   * Instantiates a new Http request exception.
   *
   * @param message the message
   */
  public HttpRequestException(String message) {
    super(message);
  }

  /**
   * Instantiates a new Http request exception.
   *
   * @param cause the cause
   */
  public HttpRequestException(Throwable cause) {
    super(cause);
  }

  /**
   * Instantiates a new Http request exception.
   *
   * @param message the message
   * @param cause the cause
   */
  public HttpRequestException(String message, Throwable cause) {
    super(message, cause);
  }
}
