package org.bitbucket.abuwandi.snoopy.client.http;

import io.netty.buffer.ByteBuf;
import io.netty.handler.codec.http.DefaultHttpContent;
import io.netty.handler.codec.http.HttpContent;
import io.netty.util.ReferenceCountUtil;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import org.bitbucket.abuwandi.snoopy.util.BufferSupplier;
import org.bitbucket.abuwandi.snoopy.util.ByteUnit;
import org.bitbucket.abuwandi.snoopy.util.IOUtils;

/** The type Input stream request body. */
public abstract class InputStreamRequestBody implements RequestBodyPart {

  /** The Chunk size. */
  protected final int chunkSize;
  /** The Input stream. */
  protected final AtomicReference<InputStream> inputStream;
  /** The End of stream reached. */
  protected final AtomicBoolean endOfStreamReached;

  /** The Total bytes read. */
  protected final AtomicLong totalBytesRead;

  protected final AtomicBoolean completed;

  /**
   * Instantiates a new Input stream request body.
   *
   * @param chunkSize the chunk size
   */
  InputStreamRequestBody(final int chunkSize) {
    this.chunkSize = Math.max(chunkSize, (int) ByteUnit.KIB.toBytes(8));
    this.inputStream = new AtomicReference<>(null);
    this.endOfStreamReached = new AtomicBoolean(false);
    this.totalBytesRead = new AtomicLong(0);
    this.completed = new AtomicBoolean(false);
  }

  /** The Pause. */
  protected final AtomicBoolean pause = new AtomicBoolean(false);

  @Override
  public boolean pause() {
    return pause.compareAndSet(false, true);
  }

  @Override
  public boolean resume() {
    return canResume() && pause.compareAndSet(true, false);
  }

  @Override
  public boolean isPaused() {
    return pause.get();
  }

  @Override
  public boolean canResume() {
    return !isCompleted();
  }

  @Override
  public boolean complete() {
    return this.completed.compareAndSet(false, true);
  }

  @Override
  public boolean isCompleted() {
    return completed.get();
  }

  /**
   * Input stream input stream.
   *
   * @return the input stream
   * @throws IOException the io exception
   */
  protected abstract InputStream inputStream() throws IOException;

  /**
   * Gets buffer initial capacity.
   *
   * @return the buffer initial capacity
   */
  protected int getBufferInitialCapacity() {
    return chunkSize;
  }

  /**
   * Pre write.
   *
   * @param buffer the buffer
   */
  protected abstract void preWrite(ByteBuf buffer);

  /**
   * Post write.
   *
   * @param buffer the buffer
   */
  protected abstract void postWrite(ByteBuf buffer);

  @Override
  public HttpContent next(BufferSupplier<ByteBuf> supplier) {
    if (endOfStreamReached.get()) {
      return null;
    }
    ByteBuf buffer = null;
    try {
      if (Objects.isNull(inputStream.get())) {
        inputStream.set(inputStream());
      }
      final int initialCapacity = getBufferInitialCapacity();
      buffer = supplier.allocate(initialCapacity);
      preWrite(buffer);
      int bytesRead = buffer.writeBytes(inputStream.get(), chunkSize);
      if (bytesRead < 0) {
        endOfStreamReached.compareAndSet(false, true);
        IOUtils.close(inputStream.get());
        ReferenceCountUtil.safeRelease(buffer);
        return null;
      }
      totalBytesRead.addAndGet(bytesRead);
      int availableWritableBytes = chunkSize - bytesRead;
      while (availableWritableBytes > 0) {
        bytesRead = buffer.writeBytes(inputStream.get(), availableWritableBytes);
        if (bytesRead < 0) {
          endOfStreamReached.compareAndSet(false, true);
          IOUtils.close(inputStream.get());
          postWrite(buffer);
          return new DefaultHttpContent(buffer);
        }
        totalBytesRead.addAndGet(bytesRead);
        availableWritableBytes -= bytesRead;
      }
      postWrite(buffer);
      return new DefaultHttpContent(buffer);
    } catch (Exception exc) {
      endOfStreamReached.compareAndSet(false, true);
      IOUtils.close(inputStream.get());
      ReferenceCountUtil.safeRelease(buffer);
      throw new HttpRequestException(exc);
    } catch (Throwable throwable) {
      endOfStreamReached.compareAndSet(false, true);
      IOUtils.close(inputStream.get());
      ReferenceCountUtil.safeRelease(buffer);
      throw throwable;
    }
  }
}
