package org.bitbucket.abuwandi.snoopy.client.handlers;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.disposables.Disposable;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import lombok.Builder;
import lombok.NonNull;
import org.bitbucket.abuwandi.snoopy.client.ResponseRegistrar;
import org.bitbucket.abuwandi.snoopy.client.SnoopyResponse;
import org.bitbucket.abuwandi.snoopy.client.SnoopySchedulers;

/**
 * The type Snoopy inbound traffic handler.
 *
 * @param <T> the type parameter
 */
public class SnoopyInboundTrafficHandler<T> extends ChannelInboundHandlerAdapter
    implements ResponseRegistrar<T> {

  @Override
  public void channelRead(ChannelHandlerContext ctx, Object msg) {
    if (msg instanceof ByteBuf) {
      bytesReceived.addAndGet(((ByteBuf) msg).readableBytes());
    }
    ctx.fireChannelRead(msg);
  }

  @Override
  public void channelRegistered(ChannelHandlerContext ctx) {
    ctx.fireChannelRegistered();
  }

  @Override
  public void channelActive(ChannelHandlerContext ctx) {
    ctx.fireChannelActive();
  }

  @Override
  public void channelReadComplete(ChannelHandlerContext ctx) {
    ctx.fireChannelReadComplete();
  }

  @Override
  public void channelWritabilityChanged(ChannelHandlerContext ctx) {
    ctx.fireChannelWritabilityChanged();
  }

  @Override
  public void channelInactive(ChannelHandlerContext ctx) {
    deregisterAll();
    ctx.fireChannelInactive();
  }

  @Override
  public void channelUnregistered(ChannelHandlerContext ctx) {
    deregisterAll();
    ctx.fireChannelUnregistered();
  }

  @Override
  public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {
    deregisterAll();
    super.handlerRemoved(ctx);
  }

  @Override
  public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
    deregisterAll();
    super.exceptionCaught(ctx, cause);
  }

  public boolean deregister(@NonNull final SnoopyResponse<T> response) {
    return atomicResponse.compareAndSet(response, null);
  }

  public void deregisterAll() {
    atomicResponse.set(null);
    atomicDisposable.updateAndGet(
        disposable -> {
          if (Objects.nonNull(disposable) && !disposable.isDisposed()) {
            disposable.dispose();
          }
          return null;
        });
    bytesReceived.set(0);
  }

  public boolean register(@NonNull final SnoopyResponse<T> response) {
    return atomicResponse.compareAndSet(null, response)
        && atomicDisposable.get() != atomicDisposable.updateAndGet(this::updateTrafficDisposable);
  }

  private Disposable updateTrafficDisposable(final Disposable current) {
    if (Objects.isNull(current)) {
      return Flowable.timer(checkInterval, checkIntervalUnit, SnoopySchedulers.timer())
          .map(delay -> bytesReceived.getAndSet(0))
          .map(
              bytesReceived -> {
                final SnoopyResponse<T> resp = atomicResponse.get();
                if (Objects.nonNull(resp)) {
                  resp.setThroughput(bytesReceived);
                }
                return bytesReceived;
              })
          .repeatUntil(() -> Objects.isNull(atomicResponse.get()))
          .subscribe(value -> {}, throwable -> {});
    }
    return current;
  }

  private final AtomicLong bytesReceived;
  private final AtomicReference<SnoopyResponse<T>> atomicResponse;
  private final AtomicReference<Disposable> atomicDisposable;
  private final long checkInterval;
  private final TimeUnit checkIntervalUnit;

  /**
   * Instantiates a new Snoopy inbound traffic handler.
   *
   * @param checkInterval the check interval
   * @param checkIntervalUnit the check interval unit
   */
  @Builder
  protected SnoopyInboundTrafficHandler(
      final long checkInterval, final TimeUnit checkIntervalUnit) {
    this.bytesReceived = new AtomicLong(0);
    this.atomicResponse = new AtomicReference<>(null);
    this.atomicDisposable = new AtomicReference<>(null);
    this.checkInterval = checkInterval;
    this.checkIntervalUnit = checkIntervalUnit;
  }
}
