package org.bitbucket.abuwandi.snoopy.client;

import org.bitbucket.abuwandi.snoopy.exceptions.SnoopyException;

/** The type Client exception. */
public class ClientException extends SnoopyException {

  /**
   * Instantiates a new Client exception.
   *
   * @param message the message
   */
  public ClientException(String message) {
    super(message);
  }

  /**
   * Instantiates a new Client exception.
   *
   * @param cause the cause
   */
  public ClientException(Throwable cause) {
    super(cause);
  }

  /**
   * Instantiates a new Client exception.
   *
   * @param message the message
   * @param cause the cause
   */
  public ClientException(String message, Throwable cause) {
    super(message, cause);
  }
}
