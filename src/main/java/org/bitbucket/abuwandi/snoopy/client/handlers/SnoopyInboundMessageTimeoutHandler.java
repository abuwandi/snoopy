package org.bitbucket.abuwandi.snoopy.client.handlers;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.disposables.Disposable;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import lombok.Builder;
import lombok.NonNull;
import org.bitbucket.abuwandi.snoopy.client.ReadTimeoutException;
import org.bitbucket.abuwandi.snoopy.client.ResponseRegistrar;
import org.bitbucket.abuwandi.snoopy.client.SnoopyResponse;
import org.bitbucket.abuwandi.snoopy.client.SnoopySchedulers;
import org.bitbucket.abuwandi.snoopy.client.Status;

/**
 * The type Snoopy inbound message timeout handler.
 *
 * @param <T> the type parameter
 */
public class SnoopyInboundMessageTimeoutHandler<T> extends ChannelInboundHandlerAdapter
    implements ResponseRegistrar<T> {

  @Override
  public void channelRead(ChannelHandlerContext ctx, Object msg) {
    lastTimeReadNanos.set(System.nanoTime());
    ctx.fireChannelRead(msg);
  }

  @Override
  public void channelRegistered(ChannelHandlerContext ctx) {
    ctx.fireChannelRegistered();
  }

  @Override
  public void channelActive(ChannelHandlerContext ctx) {
    ctx.fireChannelActive();
  }

  @Override
  public void channelReadComplete(ChannelHandlerContext ctx) {
    ctx.fireChannelReadComplete();
  }

  @Override
  public void channelWritabilityChanged(ChannelHandlerContext ctx) {
    ctx.fireChannelWritabilityChanged();
  }

  @Override
  public void channelInactive(ChannelHandlerContext ctx) {
    deregisterAll();
    ctx.fireChannelInactive();
  }

  @Override
  public void channelUnregistered(ChannelHandlerContext ctx) {
    deregisterAll();
    ctx.fireChannelUnregistered();
  }

  @Override
  public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {
    deregisterAll();
    super.handlerRemoved(ctx);
  }

  @Override
  public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
    deregisterAll();
    super.exceptionCaught(ctx, cause);
  }

  public boolean deregister(@NonNull final SnoopyResponse<T> response) {
    return atomicResponse.compareAndSet(response, null);
  }

  public void deregisterAll() {
    atomicResponse.set(null);
  }

  public boolean register(@NonNull final SnoopyResponse<T> response) {
    return atomicResponse.compareAndSet(null, response);
  }

  /** Watch. */
  public void watch() {
    atomicDisposable.updateAndGet(this::updateReadTimeOutDisposable);
  }

  private Disposable updateReadTimeOutDisposable(final Disposable current) {
    if (Objects.nonNull(current) && !current.isDisposed()) {
      current.dispose();
    }
    return Observable.intervalRange(
            1, Long.MAX_VALUE, 0, 1, TimeUnit.SECONDS, SnoopySchedulers.timer())
        .takeWhile(time -> Objects.nonNull(atomicResponse.get()))
        .doOnSubscribe(disposable -> lastTimeReadNanos.set(System.nanoTime()))
        .subscribe(
            counter -> {
              final long lastRead = lastTimeReadNanos.get();
              final SnoopyResponse<T> resp = atomicResponse.get();
              if (Objects.nonNull(resp)) {
                final long timeSinceLastRead =
                    TimeUnit.NANOSECONDS.toSeconds(System.nanoTime() - lastRead);
                if (timeSinceLastRead >= idleTimeUnit.toSeconds(idleTime)) {
                  resp.setStatus(Status.IDLE);
                }
                if (timeSinceLastRead >= readTimeoutUnit.toSeconds(readTimeout)) {
                  resp.onError(new ReadTimeoutException("Channel read timeout exceeded"));
                }
              }
            },
            throwable -> {});
  }

  private final AtomicLong lastTimeReadNanos;
  private final AtomicReference<SnoopyResponse<T>> atomicResponse;
  private final AtomicReference<Disposable> atomicDisposable;
  private final long readTimeout;
  private final TimeUnit readTimeoutUnit;
  private final long idleTime;
  private final TimeUnit idleTimeUnit;

  /**
   * Instantiates a new Snoopy inbound message timeout handler.
   *
   * @param readTimeout the read timeout
   * @param readTimeoutUnit the read timeout unit
   * @param idleTime the idle time
   * @param idleTimeUnit the idle time unit
   */
  @Builder
  protected SnoopyInboundMessageTimeoutHandler(
      final long readTimeout,
      final TimeUnit readTimeoutUnit,
      final long idleTime,
      final TimeUnit idleTimeUnit) {
    this.readTimeout = readTimeout;
    this.readTimeoutUnit = readTimeoutUnit;
    this.idleTime = idleTime;
    this.idleTimeUnit = idleTimeUnit;
    this.atomicResponse = new AtomicReference<>(null);
    this.atomicDisposable = new AtomicReference<>(null);
    this.lastTimeReadNanos = new AtomicLong(0);
  }
}
