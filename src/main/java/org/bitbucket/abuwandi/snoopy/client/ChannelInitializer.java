package org.bitbucket.abuwandi.snoopy.client;

import io.netty.channel.Channel;
import java.net.URI;

/** The interface Channel initializer. */
public interface ChannelInitializer {

  /**
   * Initialize.
   *
   * @param uri the uri
   * @param channel the channel
   */
  void initialize(URI uri, Channel channel);

  /**
   * Install handlers.
   *
   * @param channel the channel
   */
  void installHandlers(Channel channel);

  /**
   * Uninstall handlers.
   *
   * @param channel the channel
   */
  void uninstallHandlers(Channel channel);

  /** Shutdown. */
  void shutdown();
}
