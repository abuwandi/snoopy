package org.bitbucket.abuwandi.snoopy.model;

import java.util.Arrays;
import java.util.Optional;
import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;

/** The enum Scheme. */
public enum Scheme {
  /** The Http. */
  HTTP("http", "Hypertext transport protocol", 80, false),
  /** The Https. */
  HTTPS("https", "Secure hypertext transport protocol", 443, true),
  /** The File. */
  FILE("file", "Addressing files on local or network file systems", -1, false),
  /** The Ftp. */
  FTP("ftp", "File transfer protocol", 21, false),
  /** The Ftps. */
  FTPS("ftps", "Secure file transfer protocol", 21, true);

  private final String value;
  private final String description;
  private final int port;
  private final boolean secure;

  Scheme(
      @NonNull final String value,
      @NonNull final String description,
      final int port,
      final boolean secure) {
    this.value = value;
    this.description = description;
    this.port = port;
    this.secure = secure;
  }

  /**
   * Value string.
   *
   * @return the string
   */
  public String value() {
    return value;
  }

  /**
   * Port int.
   *
   * @return the int
   */
  public int port() {
    return port;
  }

  /**
   * Description string.
   *
   * @return the string
   */
  public String description() {
    return description;
  }

  /**
   * Is secure boolean.
   *
   * @return the boolean
   */
  public boolean isSecure() {
    return secure;
  }

  /**
   * For value scheme.
   *
   * @param value the value
   * @return the scheme
   */
  public static Scheme forValue(final String value) {
    return Optional.ofNullable(value)
        .filter(StringUtils::isNoneBlank)
        .map(
            target ->
                Arrays.stream(Scheme.values())
                    .filter(scheme -> scheme.value().equalsIgnoreCase(value))
                    .findFirst()
                    .orElseThrow(() -> new IllegalArgumentException("Invalid argument value")))
        .orElseThrow(() -> new IllegalArgumentException("Invalid argument value"));
  }
}
