package org.bitbucket.abuwandi.snoopy.model;

import java.time.Duration;

/** The interface Token. */
public interface Token {

  /**
   * Duration duration.
   *
   * @return the duration
   */
  Duration duration();

  /**
   * Content string.
   *
   * @return the string
   */
  String content();

  /**
   * Is valid boolean.
   *
   * @return the boolean
   */
  boolean isValid();
}
