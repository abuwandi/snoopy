package org.bitbucket.abuwandi.snoopy.model;

import java.util.Arrays;
import java.util.Optional;
import org.apache.commons.lang3.StringUtils;

/** The enum Protocol. */
public enum Protocol {
  /** Http 2 protocol. */
  HTTP2("h2"),
  /** Http 11 protocol. */
  HTTP11("http/1.1"),
  /** Http 1 protocol. */
  HTTP1("http/1.0"),
  /** Spdy 31 protocol. */
  SPDY31("spdy/3.1"),
  /** Spdy 3 protocol. */
  SPDY3("spdy/3"),
  /** Spdy 2 protocol. */
  SPDY2("spdy/2"),
  /** Spdy 1 protocol. */
  SPDY1("spdy/1"),
  /** Ftp protocol. */
  FTP("ftp");

  private final String value;

  Protocol(final String value) {
    this.value = value;
  }

  /**
   * Value string.
   *
   * @return the string
   */
  public String value() {
    return value;
  }

  /**
   * For value protocol.
   *
   * @param value the value
   * @return the protocol
   */
  public static Protocol forValue(final String value) {
    return Optional.ofNullable(value)
        .filter(StringUtils::isNoneBlank)
        .map(
            target ->
                Arrays.stream(Protocol.values())
                    .filter(protocol -> protocol.value().equalsIgnoreCase(value))
                    .findFirst()
                    .orElseThrow(() -> new IllegalArgumentException("Invalid protocol value")))
        .orElseThrow(() -> new IllegalArgumentException("Invalid protocol value"));
  }
}
